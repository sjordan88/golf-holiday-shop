<a href="https://s-pyadyshev.github.io/golf-holiday-shop/build/index.html">Homepage</a>
<br>
<a href="https://s-pyadyshev.github.io/golf-holiday-shop/build/details.html">Details</a>
<br>
<a href="https://s-pyadyshev.github.io/golf-holiday-shop/build/landing.html">Landing</a>
<br>
html-boilerplate

Gulp, Pug, Less, Animate.css, UIkit 3.1.4


Project build

$ cd html-boilerplate

$ npm i

$ npm i gulp -g

$ gulp build


UIkit custom icon generation

$ cd uikit-develop

$ yarn install

Add new icons to uikit-develop\src\images\icons

$ yarn compile
