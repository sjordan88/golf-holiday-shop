"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*! UIkit 3.1.4 | http://www.getuikit.com | (c) 2014 - 2018 YOOtheme | MIT License */

!function (t, e) {
  "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define("uikit", e) : (t = t || self).UIkit = e();
}(undefined, function () {
  "use strict";
  function l(n, i) {
    return function (t) {
      var e = arguments.length;return e ? 1 < e ? n.apply(i, arguments) : n.call(i, t) : n.call(i);
    };
  }var e = Object.prototype,
      n = e.hasOwnProperty;function c(t, e) {
    return n.call(t, e);
  }var i = {},
      r = /([a-z\d])([A-Z])/g;function d(t) {
    return t in i || (i[t] = t.replace(r, "$1-$2").toLowerCase()), i[t];
  }var o = /-(\w)/g;function f(t) {
    return t.replace(o, s);
  }function s(t, e) {
    return e ? e.toUpperCase() : "";
  }function p(t) {
    return t.length ? s(0, t.charAt(0)) + t.slice(1) : "";
  }var t = String.prototype,
      a = t.startsWith || function (t) {
    return 0 === this.lastIndexOf(t, 0);
  };function w(t, e) {
    return a.call(t, e);
  }var u = t.endsWith || function (t) {
    return this.substr(-t.length) === t;
  };function h(t, e) {
    return u.call(t, e);
  }function m(t, e) {
    return ~this.indexOf(t, e);
  }var v = Array.prototype,
      g = t.includes || m,
      b = v.includes || m;function x(t, e) {
    return t && (B(t) ? g : b).call(t, e);
  }var y = v.findIndex || function (t) {
    for (var e = arguments, n = 0; n < this.length; n++) {
      if (t.call(e[1], this[n], n, this)) return n;
    }return -1;
  };function k(t, e) {
    return y.call(t, e);
  }var $ = Array.isArray;function I(t) {
    return "function" == typeof t;
  }function T(t) {
    return null !== t && "object" == (typeof t === "undefined" ? "undefined" : _typeof(t));
  }function A(t) {
    return T(t) && Object.getPrototypeOf(t) === e;
  }function S(t) {
    return T(t) && t === t.window;
  }function E(t) {
    return T(t) && 9 === t.nodeType;
  }function C(t) {
    return T(t) && !!t.jquery;
  }function _(t) {
    return t instanceof Node || T(t) && 1 <= t.nodeType;
  }var N = e.toString;function M(t) {
    return N.call(t).match(/^\[object (NodeList|HTMLCollection)\]$/);
  }function O(t) {
    return "boolean" == typeof t;
  }function B(t) {
    return "string" == typeof t;
  }function z(t) {
    return "number" == typeof t;
  }function D(t) {
    return z(t) || B(t) && !isNaN(t - parseFloat(t));
  }function H(t) {
    return !($(t) ? t.length : T(t) && Object.keys(t).length);
  }function P(t) {
    return void 0 === t;
  }function L(t) {
    return O(t) ? t : "true" === t || "1" === t || "" === t || "false" !== t && "0" !== t && t;
  }function j(t) {
    var e = Number(t);return !isNaN(e) && e;
  }function F(t) {
    return parseFloat(t) || 0;
  }function W(t) {
    return _(t) || S(t) || E(t) ? t : M(t) || C(t) ? t[0] : $(t) ? W(t[0]) : null;
  }function V(t) {
    return _(t) ? [t] : M(t) ? v.slice.call(t) : $(t) ? t.map(W).filter(Boolean) : C(t) ? t.toArray() : [];
  }function q(t) {
    return $(t) ? t : B(t) ? t.split(/,(?![^(]*\))/).map(function (t) {
      return D(t) ? j(t) : L(t.trim());
    }) : [t];
  }function R(t) {
    return t ? h(t, "ms") ? F(t) : 1e3 * F(t) : 0;
  }function Y(t, n) {
    return t === n || T(t) && T(n) && Object.keys(t).length === Object.keys(n).length && G(t, function (t, e) {
      return t === n[e];
    });
  }function U(t, e, n) {
    return t.replace(new RegExp(e + "|" + n, "mg"), function (t) {
      return t === e ? n : e;
    });
  }var X = Object.assign || function (t) {
    for (var e = [], n = arguments.length - 1; 0 < n--;) {
      e[n] = arguments[n + 1];
    }t = Object(t);for (var i = 0; i < e.length; i++) {
      var r = e[i];if (null !== r) for (var o in r) {
        c(r, o) && (t[o] = r[o]);
      }
    }return t;
  };function G(t, e) {
    for (var n in t) {
      if (!1 === e(t[n], n)) return !1;
    }return !0;
  }function J(t, r) {
    return t.sort(function (t, e) {
      var n = t[r];void 0 === n && (n = 0);var i = e[r];return void 0 === i && (i = 0), i < n ? 1 : n < i ? -1 : 0;
    });
  }function K(t, n) {
    var i = new Set();return t.filter(function (t) {
      var e = t[n];return !i.has(e) && (i.add(e) || !0);
    });
  }function Z(t, e, n) {
    return void 0 === e && (e = 0), void 0 === n && (n = 1), Math.min(Math.max(j(t) || 0, e), n);
  }function Q() {}function tt(t, e) {
    return t.left < e.right && t.right > e.left && t.top < e.bottom && t.bottom > e.top;
  }function et(t, e) {
    return t.x <= e.right && t.x >= e.left && t.y <= e.bottom && t.y >= e.top;
  }var nt = { ratio: function ratio(t, e, n) {
      var i,
          r = "width" === e ? "height" : "width";return (i = {})[r] = t[e] ? Math.round(n * t[r] / t[e]) : t[r], i[e] = n, i;
    }, contain: function contain(n, i) {
      var r = this;return G(n = X({}, n), function (t, e) {
        return n = n[e] > i[e] ? r.ratio(n, e, i[e]) : n;
      }), n;
    }, cover: function cover(n, i) {
      var r = this;return G(n = this.contain(n, i), function (t, e) {
        return n = n[e] < i[e] ? r.ratio(n, e, i[e]) : n;
      }), n;
    } };function it(t, e, n) {
    if (T(e)) for (var i in e) {
      it(t, i, e[i]);
    } else {
      if (P(n)) return (t = W(t)) && t.getAttribute(e);V(t).forEach(function (t) {
        I(n) && (n = n.call(t, it(t, e))), null === n ? ot(t, e) : t.setAttribute(e, n);
      });
    }
  }function rt(t, e) {
    return V(t).some(function (t) {
      return t.hasAttribute(e);
    });
  }function ot(t, e) {
    t = V(t), e.split(" ").forEach(function (e) {
      return t.forEach(function (t) {
        return t.hasAttribute(e) && t.removeAttribute(e);
      });
    });
  }function st(t, e) {
    for (var n = 0, i = [e, "data-" + e]; n < i.length; n++) {
      if (rt(t, i[n])) return it(t, i[n]);
    }
  }function at(t, e) {
    return W(t) || ht(t, ct(t, e));
  }function ut(t, e) {
    var n = V(t);return n.length && n || lt(t, ct(t, e));
  }function ct(t, e) {
    return void 0 === e && (e = document), mt(t) || E(e) ? e : e.ownerDocument;
  }function ht(t, e) {
    return W(dt(t, e, "querySelector"));
  }function lt(t, e) {
    return V(dt(t, e, "querySelectorAll"));
  }function dt(t, s, e) {
    if (void 0 === s && (s = document), !t || !B(t)) return null;var a;mt(t = t.replace(pt, "$1 *")) && (a = [], t = function (t) {
      return t.match(vt).map(function (t) {
        return t.replace(/,$/, "").trim();
      });
    }(t).map(function (t, e) {
      var n = s;if ("!" === t[0]) {
        var i = t.substr(1).trim().split(" ");n = yt(s.parentNode, i[0]), t = i.slice(1).join(" ").trim();
      }if ("-" === t[0]) {
        var r = t.substr(1).trim().split(" "),
            o = (n || s).previousElementSibling;n = bt(o, t.substr(1)) ? o : null, t = r.slice(1).join(" ");
      }return n ? (n.id || (n.id = "uk-" + Date.now() + e, a.push(function () {
        return ot(n, "id");
      })), "#" + It(n.id) + " " + t) : null;
    }).filter(Boolean).join(","), s = document);try {
      return s[e](t);
    } catch (t) {
      return null;
    } finally {
      a && a.forEach(function (t) {
        return t();
      });
    }
  }var ft = /(^|[^\\],)\s*[!>+~-]/,
      pt = /([!>+~-])(?=\s+[!>+~-]|\s*$)/g;function mt(t) {
    return B(t) && t.match(ft);
  }var vt = /.*?[^\\](?:,|$)/g;var gt = Element.prototype,
      wt = gt.matches || gt.webkitMatchesSelector || gt.msMatchesSelector;function bt(t, e) {
    return V(t).some(function (t) {
      return wt.call(t, e);
    });
  }var xt = gt.closest || function (t) {
    var e = this;do {
      if (bt(e, t)) return e;e = e.parentNode;
    } while (e && 1 === e.nodeType);
  };function yt(t, e) {
    return w(e, ">") && (e = e.slice(1)), _(t) ? t.parentNode && xt.call(t, e) : V(t).map(function (t) {
      return yt(t, e);
    }).filter(Boolean);
  }function kt(t, e) {
    for (var n = [], i = W(t).parentNode; i && 1 === i.nodeType;) {
      bt(i, e) && n.push(i), i = i.parentNode;
    }return n;
  }var $t = window.CSS && CSS.escape || function (t) {
    return t.replace(/([^\x7f-\uFFFF\w-])/g, function (t) {
      return "\\" + t;
    });
  };function It(t) {
    return B(t) ? $t.call(null, t) : "";
  }var Tt = { area: !0, base: !0, br: !0, col: !0, embed: !0, hr: !0, img: !0, input: !0, keygen: !0, link: !0, menuitem: !0, meta: !0, param: !0, source: !0, track: !0, wbr: !0 };function At(t) {
    return V(t).some(function (t) {
      return Tt[t.tagName.toLowerCase()];
    });
  }function St(t) {
    return V(t).some(function (t) {
      return t.offsetWidth || t.offsetHeight || t.getClientRects().length;
    });
  }var Et = "input,select,textarea,button";function Ct(t) {
    return V(t).some(function (t) {
      return bt(t, Et);
    });
  }function _t(t, e) {
    return V(t).filter(function (t) {
      return bt(t, e);
    });
  }function Nt(t, e) {
    return B(e) ? bt(t, e) || yt(t, e) : t === e || (E(e) ? e.documentElement : W(e)).contains(W(t));
  }function Mt() {
    for (var t = [], e = arguments.length; e--;) {
      t[e] = arguments[e];
    }var n = Ht(t),
        i = n[0],
        r = n[1],
        o = n[2],
        s = n[3],
        a = n[4];return i = jt(i), o && (s = function (t, i, r) {
      var o = this;return function (n) {
        t.forEach(function (t) {
          var e = ">" === i[0] ? lt(i, t).reverse().filter(function (t) {
            return Nt(n.target, t);
          })[0] : yt(n.target, i);e && (n.delegate = t, n.current = e, r.call(o, n));
        });
      };
    }(i, o, s)), 1 < s.length && (s = function (e) {
      return function (t) {
        return $(t.detail) ? e.apply(void 0, [t].concat(t.detail)) : e(t);
      };
    }(s)), r.split(" ").forEach(function (e) {
      return i.forEach(function (t) {
        return t.addEventListener(e, s, a);
      });
    }), function () {
      return Ot(i, r, s, a);
    };
  }function Ot(t, e, n, i) {
    void 0 === i && (i = !1), t = jt(t), e.split(" ").forEach(function (e) {
      return t.forEach(function (t) {
        return t.removeEventListener(e, n, i);
      });
    });
  }function Bt() {
    for (var t = [], e = arguments.length; e--;) {
      t[e] = arguments[e];
    }var n = Ht(t),
        i = n[0],
        r = n[1],
        o = n[2],
        s = n[3],
        a = n[4],
        u = n[5],
        c = Mt(i, r, o, function (t) {
      var e = !u || u(t);e && (c(), s(t, e));
    }, a);return c;
  }function zt(t, n, i) {
    return jt(t).reduce(function (t, e) {
      return t && e.dispatchEvent(Dt(n, !0, !0, i));
    }, !0);
  }function Dt(t, e, n, i) {
    if (void 0 === e && (e = !0), void 0 === n && (n = !1), B(t)) {
      var r = document.createEvent("CustomEvent");r.initCustomEvent(t, e, n, i), t = r;
    }return t;
  }function Ht(t) {
    return I(t[2]) && t.splice(2, 0, !1), t;
  }function Pt(t) {
    return t && "addEventListener" in t;
  }function Lt(t) {
    return Pt(t) ? t : W(t);
  }function jt(t) {
    return $(t) ? t.map(Lt).filter(Boolean) : B(t) ? lt(t) : Pt(t) ? [t] : V(t);
  }function Ft(t) {
    return "touch" === t.pointerType || t.touches;
  }function Wt(t, e) {
    void 0 === e && (e = "client");var n = t.touches,
        i = t.changedTouches,
        r = n && n[0] || i && i[0] || t;return { x: r[e + "X"], y: r[e + "Y"] };
  }function Vt() {
    var n = this;this.promise = new qt(function (t, e) {
      n.reject = e, n.resolve = t;
    });
  }var qt = "Promise" in window ? window.Promise : Ut,
      Rt = 2,
      Yt = "setImmediate" in window ? setImmediate : setTimeout;function Ut(t) {
    this.state = Rt, this.value = void 0, this.deferred = [];var e = this;try {
      t(function (t) {
        e.resolve(t);
      }, function (t) {
        e.reject(t);
      });
    } catch (t) {
      e.reject(t);
    }
  }Ut.reject = function (n) {
    return new Ut(function (t, e) {
      e(n);
    });
  }, Ut.resolve = function (n) {
    return new Ut(function (t, e) {
      t(n);
    });
  }, Ut.all = function (s) {
    return new Ut(function (n, t) {
      var i = [],
          r = 0;function e(e) {
        return function (t) {
          i[e] = t, (r += 1) === s.length && n(i);
        };
      }0 === s.length && n(i);for (var o = 0; o < s.length; o += 1) {
        Ut.resolve(s[o]).then(e(o), t);
      }
    });
  }, Ut.race = function (i) {
    return new Ut(function (t, e) {
      for (var n = 0; n < i.length; n += 1) {
        Ut.resolve(i[n]).then(t, e);
      }
    });
  };var Xt = Ut.prototype;function Gt(s, a) {
    return new qt(function (t, e) {
      var n = X({ data: null, method: "GET", headers: {}, xhr: new XMLHttpRequest(), beforeSend: Q, responseType: "" }, a);n.beforeSend(n);var i = n.xhr;for (var r in n) {
        if (r in i) try {
          i[r] = n[r];
        } catch (t) {}
      }for (var o in i.open(n.method.toUpperCase(), s), n.headers) {
        i.setRequestHeader(o, n.headers[o]);
      }Mt(i, "load", function () {
        0 === i.status || 200 <= i.status && i.status < 300 || 304 === i.status ? t(i) : e(X(Error(i.statusText), { xhr: i, status: i.status }));
      }), Mt(i, "error", function () {
        return e(X(Error("Network Error"), { xhr: i }));
      }), Mt(i, "timeout", function () {
        return e(X(Error("Network Timeout"), { xhr: i }));
      }), i.send(n.data);
    });
  }function Jt(i, r, o) {
    return new qt(function (t, e) {
      var n = new Image();n.onerror = e, n.onload = function () {
        return t(n);
      }, o && (n.sizes = o), r && (n.srcset = r), n.src = i;
    });
  }Xt.resolve = function (t) {
    var e = this;if (e.state === Rt) {
      if (t === e) throw new TypeError("Promise settled with itself.");var n = !1;try {
        var i = t && t.then;if (null !== t && T(t) && I(i)) return void i.call(t, function (t) {
          n || e.resolve(t), n = !0;
        }, function (t) {
          n || e.reject(t), n = !0;
        });
      } catch (t) {
        return void (n || e.reject(t));
      }e.state = 0, e.value = t, e.notify();
    }
  }, Xt.reject = function (t) {
    var e = this;if (e.state === Rt) {
      if (t === e) throw new TypeError("Promise settled with itself.");e.state = 1, e.value = t, e.notify();
    }
  }, Xt.notify = function () {
    var o = this;Yt(function () {
      if (o.state !== Rt) for (; o.deferred.length;) {
        var t = o.deferred.shift(),
            e = t[0],
            n = t[1],
            i = t[2],
            r = t[3];try {
          0 === o.state ? I(e) ? i(e.call(void 0, o.value)) : i(o.value) : 1 === o.state && (I(n) ? i(n.call(void 0, o.value)) : r(o.value));
        } catch (t) {
          r(t);
        }
      }
    });
  }, Xt.then = function (n, i) {
    var r = this;return new Ut(function (t, e) {
      r.deferred.push([n, i, t, e]), r.notify();
    });
  }, Xt.catch = function (t) {
    return this.then(void 0, t);
  };var Kt = /msie|trident/i.test(window.navigator.userAgent),
      Zt = "rtl" === it(document.documentElement, "dir"),
      Qt = "ontouchstart" in window,
      te = window.PointerEvent,
      ee = Qt || window.DocumentTouch && document instanceof DocumentTouch || navigator.maxTouchPoints,
      ne = te ? "pointerdown" : Qt ? "touchstart" : "mousedown",
      ie = te ? "pointermove" : Qt ? "touchmove" : "mousemove",
      re = te ? "pointerup" : Qt ? "touchend" : "mouseup",
      oe = te ? "pointerenter" : Qt ? "" : "mouseenter",
      se = te ? "pointerleave" : Qt ? "" : "mouseleave",
      ae = te ? "pointercancel" : "touchcancel";function ue(t) {
    if ("loading" === document.readyState) var e = Mt(document, "DOMContentLoaded", function () {
      e(), t();
    });else t();
  }function ce(t, e) {
    return e ? V(t).indexOf(W(e)) : V((t = W(t)) && t.parentNode.children).indexOf(t);
  }function he(t, e, n, i) {
    void 0 === n && (n = 0), void 0 === i && (i = !1);var r = (e = V(e)).length;return t = D(t) ? j(t) : "next" === t ? n + 1 : "previous" === t ? n - 1 : ce(e, t), i ? Z(t, 0, r - 1) : (t %= r) < 0 ? t + r : t;
  }function le(t) {
    return (t = Te(t)).innerHTML = "", t;
  }function de(t, e) {
    return t = Te(t), P(e) ? t.innerHTML : fe(t.hasChildNodes() ? le(t) : t, e);
  }function fe(e, t) {
    return e = Te(e), ve(t, function (t) {
      return e.appendChild(t);
    });
  }function pe(e, t) {
    return e = Te(e), ve(t, function (t) {
      return e.parentNode.insertBefore(t, e);
    });
  }function me(e, t) {
    return e = Te(e), ve(t, function (t) {
      return e.nextSibling ? pe(e.nextSibling, t) : fe(e.parentNode, t);
    });
  }function ve(t, e) {
    return (t = B(t) ? $e(t) : t) ? "length" in t ? V(t).map(e) : e(t) : null;
  }function ge(t) {
    V(t).map(function (t) {
      return t.parentNode && t.parentNode.removeChild(t);
    });
  }function we(t, e) {
    for (e = W(pe(t, e)); e.firstChild;) {
      e = e.firstChild;
    }return fe(e, t), e;
  }function be(t, e) {
    return V(V(t).map(function (t) {
      return t.hasChildNodes ? we(V(t.childNodes), e) : fe(t, e);
    }));
  }function xe(t) {
    V(t).map(function (t) {
      return t.parentNode;
    }).filter(function (t, e, n) {
      return n.indexOf(t) === e;
    }).forEach(function (t) {
      pe(t, t.childNodes), ge(t);
    });
  }var ye = /^\s*<(\w+|!)[^>]*>/,
      ke = /^<(\w+)\s*\/?>(?:<\/\1>)?$/;function $e(t) {
    var e = ke.exec(t);if (e) return document.createElement(e[1]);var n = document.createElement("div");return ye.test(t) ? n.insertAdjacentHTML("beforeend", t.trim()) : n.textContent = t, 1 < n.childNodes.length ? V(n.childNodes) : n.firstChild;
  }function Ie(t, e) {
    if (t && 1 === t.nodeType) for (e(t), t = t.firstElementChild; t;) {
      Ie(t, e), t = t.nextElementSibling;
    }
  }function Te(t, e) {
    return B(t) ? Se(t) ? W($e(t)) : ht(t, e) : W(t);
  }function Ae(t, e) {
    return B(t) ? Se(t) ? V($e(t)) : lt(t, e) : V(t);
  }function Se(t) {
    return "<" === t[0] || t.match(/^\s*</);
  }function Ee(t) {
    for (var e = [], n = arguments.length - 1; 0 < n--;) {
      e[n] = arguments[n + 1];
    }Be(t, e, "add");
  }function Ce(t) {
    for (var e = [], n = arguments.length - 1; 0 < n--;) {
      e[n] = arguments[n + 1];
    }Be(t, e, "remove");
  }function _e(t, e) {
    it(t, "class", function (t) {
      return (t || "").replace(new RegExp("\\b" + e + "\\b", "g"), "");
    });
  }function Ne(t) {
    for (var e = [], n = arguments.length - 1; 0 < n--;) {
      e[n] = arguments[n + 1];
    }e[0] && Ce(t, e[0]), e[1] && Ee(t, e[1]);
  }function Me(t, e) {
    return e && V(t).some(function (t) {
      return t.classList.contains(e.split(" ")[0]);
    });
  }function Oe(t) {
    for (var i = [], e = arguments.length - 1; 0 < e--;) {
      i[e] = arguments[e + 1];
    }if (i.length) {
      var r = B((i = ze(i))[i.length - 1]) ? [] : i.pop();i = i.filter(Boolean), V(t).forEach(function (t) {
        for (var e = t.classList, n = 0; n < i.length; n++) {
          De.Force ? e.toggle.apply(e, [i[n]].concat(r)) : e[(P(r) ? !e.contains(i[n]) : r) ? "add" : "remove"](i[n]);
        }
      });
    }
  }function Be(t, n, i) {
    (n = ze(n).filter(Boolean)).length && V(t).forEach(function (t) {
      var e = t.classList;De.Multiple ? e[i].apply(e, n) : n.forEach(function (t) {
        return e[i](t);
      });
    });
  }function ze(t) {
    return t.reduce(function (t, e) {
      return t.concat.call(t, B(e) && x(e, " ") ? e.trim().split(" ") : e);
    }, []);
  }var De = { get Multiple() {
      return this.get("_multiple");
    }, get Force() {
      return this.get("_force");
    }, get: function get(t) {
      if (!c(this, t)) {
        var e = document.createElement("_").classList;e.add("a", "b"), e.toggle("c", !1), this._multiple = e.contains("b"), this._force = !e.contains("c");
      }return this[t];
    } },
      He = { "animation-iteration-count": !0, "column-count": !0, "fill-opacity": !0, "flex-grow": !0, "flex-shrink": !0, "font-weight": !0, "line-height": !0, opacity: !0, order: !0, orphans: !0, "stroke-dasharray": !0, "stroke-dashoffset": !0, widows: !0, "z-index": !0, zoom: !0 };function Pe(t, e, r) {
    return V(t).map(function (n) {
      if (B(e)) {
        if (e = qe(e), P(r)) return je(n, e);r || z(r) ? n.style[e] = D(r) && !He[e] ? r + "px" : r : n.style.removeProperty(e);
      } else {
        if ($(e)) {
          var i = Le(n);return e.reduce(function (t, e) {
            return t[e] = i[qe(e)], t;
          }, {});
        }T(e) && G(e, function (t, e) {
          return Pe(n, e, t);
        });
      }return n;
    })[0];
  }function Le(t, e) {
    return (t = W(t)).ownerDocument.defaultView.getComputedStyle(t, e);
  }function je(t, e, n) {
    return Le(t, n)[e];
  }var Fe = {};function We(t) {
    var e = document.documentElement;if (!Kt) return Le(e).getPropertyValue("--uk-" + t);if (!(t in Fe)) {
      var n = fe(e, document.createElement("div"));Ee(n, "uk-" + t), Fe[t] = je(n, "content", ":before").replace(/^["'](.*)["']$/, "$1"), ge(n);
    }return Fe[t];
  }var Ve = {};function qe(t) {
    var e = Ve[t];return e || (e = Ve[t] = function (t) {
      t = d(t);var e = document.documentElement.style;if (t in e) return t;var n,
          i = Re.length;for (; i--;) {
        if ((n = "-" + Re[i] + "-" + t) in e) return n;
      }
    }(t) || t), e;
  }var Re = ["webkit", "moz", "ms"];function Ye(t, s, a, u) {
    return void 0 === a && (a = 400), void 0 === u && (u = "linear"), qt.all(V(t).map(function (o) {
      return new qt(function (n, i) {
        for (var t in s) {
          var e = Pe(o, t);"" === e && Pe(o, t, e);
        }var r = setTimeout(function () {
          return zt(o, "transitionend");
        }, a);Bt(o, "transitionend transitioncanceled", function (t) {
          var e = t.type;clearTimeout(r), Ce(o, "uk-transition"), Pe(o, { "transition-property": "", "transition-duration": "", "transition-timing-function": "" }), "transitioncanceled" === e ? i() : n();
        }, !1, function (t) {
          var e = t.target;return o === e;
        }), Ee(o, "uk-transition"), Pe(o, X({ "transition-property": Object.keys(s).map(qe).join(","), "transition-duration": a + "ms", "transition-timing-function": u }, s));
      });
    }));
  }var Ue = { start: Ye, stop: function stop(t) {
      return zt(t, "transitionend"), qt.resolve();
    }, cancel: function cancel(t) {
      zt(t, "transitioncanceled");
    }, inProgress: function inProgress(t) {
      return Me(t, "uk-transition");
    } },
      Xe = "uk-animation-",
      Ge = "uk-cancel-animation";function Je(t, e, n, a, u) {
    var c = arguments;return void 0 === n && (n = 200), qt.all(V(t).map(function (s) {
      return new qt(function (i, r) {
        if (Me(s, Ge)) requestAnimationFrame(function () {
          return qt.resolve().then(function () {
            return Je.apply(void 0, c).then(i, r);
          });
        });else {
          var t = e + " " + Xe + (u ? "leave" : "enter");w(e, Xe) && (a && (t += " uk-transform-origin-" + a), u && (t += " " + Xe + "reverse")), o(), Bt(s, "animationend animationcancel", function (t) {
            var e = t.type,
                n = !1;"animationcancel" === e ? (r(), o()) : (i(), qt.resolve().then(function () {
              n = !0, o();
            })), requestAnimationFrame(function () {
              n || (Ee(s, Ge), requestAnimationFrame(function () {
                return Ce(s, Ge);
              }));
            });
          }, !1, function (t) {
            var e = t.target;return s === e;
          }), Pe(s, "animationDuration", n + "ms"), Ee(s, t);
        }function o() {
          Pe(s, "animationDuration", ""), _e(s, Xe + "\\S*");
        }
      });
    }));
  }var Ke = new RegExp(Xe + "(enter|leave)"),
      Ze = { in: function _in(t, e, n, i) {
      return Je(t, e, n, i, !1);
    }, out: function out(t, e, n, i) {
      return Je(t, e, n, i, !0);
    }, inProgress: function inProgress(t) {
      return Ke.test(it(t, "class"));
    }, cancel: function cancel(t) {
      zt(t, "animationcancel");
    } },
      Qe = { width: ["x", "left", "right"], height: ["y", "top", "bottom"] };function tn(t, e, h, l, d, n, i, r) {
    h = hn(h), l = hn(l);var f = { element: h, target: l };if (!t || !e) return f;var p = nn(t),
        m = nn(e),
        v = m;if (cn(v, h, p, -1), cn(v, l, m, 1), d = ln(d, p.width, p.height), n = ln(n, m.width, m.height), d.x += n.x, d.y += n.y, v.left += d.x, v.top += d.y, i) {
      var o = [nn(bn(t))];r && o.unshift(nn(r)), G(Qe, function (t, s) {
        var a = t[0],
            u = t[1],
            c = t[2];!0 !== i && !x(i, a) || o.some(function (i) {
          var t = h[a] === u ? -p[s] : h[a] === c ? p[s] : 0,
              e = l[a] === u ? m[s] : l[a] === c ? -m[s] : 0;if (v[u] < i[u] || v[u] + p[s] > i[c]) {
            var n = p[s] / 2,
                r = "center" === l[a] ? -m[s] / 2 : 0;return "center" === h[a] && (o(n, r) || o(-n, -r)) || o(t, e);
          }function o(e, t) {
            var n = v[u] + e + t - 2 * d[a];if (n >= i[u] && n + p[s] <= i[c]) return v[u] = n, ["element", "target"].forEach(function (t) {
              f[t][a] = e ? f[t][a] === Qe[s][1] ? Qe[s][2] : Qe[s][1] : f[t][a];
            }), !0;
          }
        });
      });
    }return en(t, v), f;
  }function en(n, i) {
    if (n = W(n), !i) return nn(n);var r = en(n),
        o = Pe(n, "position");["left", "top"].forEach(function (t) {
      if (t in i) {
        var e = Pe(n, t);Pe(n, t, i[t] - r[t] + F("absolute" === o && "auto" === e ? rn(n)[t] : e));
      }
    });
  }function nn(t) {
    var e,
        n,
        i = bn(t = W(t)),
        r = i.pageYOffset,
        o = i.pageXOffset;if (S(t)) {
      var s = t.innerHeight,
          a = t.innerWidth;return { top: r, left: o, height: s, width: a, bottom: r + s, right: o + a };
    }St(t) || "none" !== Pe(t, "display") || (e = it(t, "style"), n = it(t, "hidden"), it(t, { style: (e || "") + ";display:block !important;", hidden: null }));var u = t.getBoundingClientRect();return P(e) || it(t, { style: e, hidden: n }), { height: u.height, width: u.width, top: u.top + r, left: u.left + o, bottom: u.bottom + r, right: u.right + o };
  }function rn(i) {
    var r = (i = W(i)).offsetParent || function (t) {
      return xn(t).documentElement;
    }(i),
        o = en(r),
        t = ["top", "left"].reduce(function (t, e) {
      var n = p(e);return t[e] -= o[e] + F(Pe(i, "margin" + n)) + F(Pe(r, "border" + n + "Width")), t;
    }, en(i));return { top: t.top, left: t.left };
  }var on = an("height"),
      sn = an("width");function an(i) {
    var r = p(i);return function (t, e) {
      if (t = W(t), P(e)) {
        if (S(t)) return t["inner" + r];if (E(t)) {
          var n = t.documentElement;return Math.max(n["offset" + r], n["scroll" + r]);
        }return (e = "auto" === (e = Pe(t, i)) ? t["offset" + r] : F(e) || 0) - un(i, t);
      }Pe(t, i, e || 0 === e ? +e + un(i, t) + "px" : "");
    };
  }function un(t, n, e) {
    return void 0 === e && (e = "border-box"), Pe(n, "boxSizing") === e ? Qe[t].slice(1).map(p).reduce(function (t, e) {
      return t + F(Pe(n, "padding" + e)) + F(Pe(n, "border" + e + "Width"));
    }, 0) : 0;
  }function cn(o, s, a, u) {
    G(Qe, function (t, e) {
      var n = t[0],
          i = t[1],
          r = t[2];s[n] === r ? o[i] += a[e] * u : "center" === s[n] && (o[i] += a[e] * u / 2);
    });
  }function hn(t) {
    var e = /left|center|right/,
        n = /top|center|bottom/;return 1 === (t = (t || "").split(" ")).length && (t = e.test(t[0]) ? t.concat(["center"]) : n.test(t[0]) ? ["center"].concat(t) : ["center", "center"]), { x: e.test(t[0]) ? t[0] : "center", y: n.test(t[1]) ? t[1] : "center" };
  }function ln(t, e, n) {
    var i = (t || "").split(" "),
        r = i[0],
        o = i[1];return { x: r ? F(r) * (h(r, "%") ? e / 100 : 1) : 0, y: o ? F(o) * (h(o, "%") ? n / 100 : 1) : 0 };
  }function dn(t) {
    switch (t) {case "left":
        return "right";case "right":
        return "left";case "top":
        return "bottom";case "bottom":
        return "top";default:
        return t;}
  }function fn(t, e, n) {
    if (void 0 === e && (e = 0), void 0 === n && (n = 0), !St(t)) return !1;var i = bn(t = W(t)),
        r = t.getBoundingClientRect(),
        o = { top: -e, left: -n, bottom: e + on(i), right: n + sn(i) };return tt(r, o) || et({ x: r.left, y: r.top }, o);
  }function pn(t, e) {
    if (void 0 === e && (e = 0), !St(t)) return 0;var n = bn(t = W(t)),
        i = xn(t),
        r = t.offsetHeight + e,
        o = vn(t)[0],
        s = on(n),
        a = s + Math.min(0, o - s),
        u = Math.max(0, s - (on(i) + e - (o + r)));return Z((a + n.pageYOffset - o) / ((a + (r - (u < s ? u : 0))) / 100) / 100);
  }function mn(t, e) {
    if (S(t = W(t)) || E(t)) {
      var n = bn(t);(0, n.scrollTo)(n.pageXOffset, e);
    } else t.scrollTop = e;
  }function vn(t) {
    var e = [0, 0];do {
      if (e[0] += t.offsetTop, e[1] += t.offsetLeft, "fixed" === Pe(t, "position")) {
        var n = bn(t);return e[0] += n.pageYOffset, e[1] += n.pageXOffset, e;
      }
    } while (t = t.offsetParent);return e;
  }function gn(t, e, n) {
    return void 0 === e && (e = "width"), void 0 === n && (n = window), D(t) ? +t : h(t, "vh") ? wn(on(bn(n)), t) : h(t, "vw") ? wn(sn(bn(n)), t) : h(t, "%") ? wn(nn(n)[e], t) : F(t);
  }function wn(t, e) {
    return t * F(e) / 100;
  }function bn(t) {
    return S(t) ? t : xn(t).defaultView;
  }function xn(t) {
    return W(t).ownerDocument;
  }var yn = { reads: [], writes: [], read: function read(t) {
      return this.reads.push(t), kn(), t;
    }, write: function write(t) {
      return this.writes.push(t), kn(), t;
    }, clear: function clear(t) {
      return In(this.reads, t) || In(this.writes, t);
    }, flush: function flush() {
      $n(this.reads), $n(this.writes.splice(0, this.writes.length)), this.scheduled = !1, (this.reads.length || this.writes.length) && kn();
    } };function kn() {
    yn.scheduled || (yn.scheduled = !0, requestAnimationFrame(yn.flush.bind(yn)));
  }function $n(t) {
    for (var e; e = t.shift();) {
      e();
    }
  }function In(t, e) {
    var n = t.indexOf(e);return !!~n && !!t.splice(n, 1);
  }function Tn() {}function An(t, e) {
    return (e.y - t.y) / (e.x - t.x);
  }Tn.prototype = { positions: [], position: null, init: function init() {
      var i = this;this.positions = [], this.position = null;var r = !1;this.unbind = Mt(document, "mousemove", function (n) {
        r || (setTimeout(function () {
          var t = Date.now(),
              e = i.positions.length;e && 100 < t - i.positions[e - 1].time && i.positions.splice(0, e), i.positions.push({ time: t, x: n.pageX, y: n.pageY }), 5 < i.positions.length && i.positions.shift(), r = !1;
        }, 5), r = !0);
      });
    }, cancel: function cancel() {
      this.unbind && this.unbind();
    }, movesTo: function movesTo(t) {
      if (this.positions.length < 2) return !1;var e = en(t),
          n = this.positions[this.positions.length - 1],
          i = this.positions[0];if (e.left <= n.x && n.x <= e.right && e.top <= n.y && n.y <= e.bottom) return !1;var r = [[{ x: e.left, y: e.top }, { x: e.right, y: e.bottom }], [{ x: e.right, y: e.top }, { x: e.left, y: e.bottom }]];return e.right <= n.x || (e.left >= n.x ? (r[0].reverse(), r[1].reverse()) : e.bottom <= n.y ? r[0].reverse() : e.top >= n.y && r[1].reverse()), !!r.reduce(function (t, e) {
        return t + (An(i, e[0]) < An(n, e[0]) && An(i, e[1]) > An(n, e[1]));
      }, 0);
    } };var Sn = {};function En(t, e, n) {
    return Sn.computed(I(t) ? t.call(n, n) : t, I(e) ? e.call(n, n) : e);
  }function Cn(t, e) {
    return t = t && !$(t) ? [t] : t, e ? t ? t.concat(e) : $(e) ? e : [e] : t;
  }function _n(e, n, i) {
    var r = {};if (I(n) && (n = n.options), n.extends && (e = _n(e, n.extends, i)), n.mixins) for (var t = 0, o = n.mixins.length; t < o; t++) {
      e = _n(e, n.mixins[t], i);
    }for (var s in e) {
      u(s);
    }for (var a in n) {
      c(e, a) || u(a);
    }function u(t) {
      r[t] = (Sn[t] || function (t, e) {
        return P(e) ? t : e;
      })(e[t], n[t], i);
    }return r;
  }function Nn(t, e) {
    var n;void 0 === e && (e = []);try {
      return t ? w(t, "{") ? JSON.parse(t) : e.length && !x(t, ":") ? ((n = {})[e[0]] = t, n) : t.split(";").reduce(function (t, e) {
        var n = e.split(/:(.*)/),
            i = n[0],
            r = n[1];return i && !P(r) && (t[i.trim()] = r.trim()), t;
      }, {}) : {};
    } catch (t) {
      return {};
    }
  }Sn.events = Sn.created = Sn.beforeConnect = Sn.connected = Sn.beforeDisconnect = Sn.disconnected = Sn.destroy = Cn, Sn.args = function (t, e) {
    return Cn(e || t);
  }, Sn.update = function (t, e) {
    return J(Cn(t, I(e) ? { read: e } : e), "order");
  }, Sn.props = function (t, e) {
    return $(e) && (e = e.reduce(function (t, e) {
      return t[e] = String, t;
    }, {})), Sn.methods(t, e);
  }, Sn.computed = Sn.methods = function (t, e) {
    return e ? t ? X({}, t, e) : e : t;
  }, Sn.data = function (e, n, t) {
    return t ? En(e, n, t) : n ? e ? function (t) {
      return En(e, n, t);
    } : n : e;
  };function Mn(t) {
    this.id = ++On, this.el = W(t);
  }var On = 0;function Bn(t, e) {
    try {
      t.contentWindow.postMessage(JSON.stringify(X({ event: "command" }, e)), "*");
    } catch (t) {}
  }Mn.prototype.isVideo = function () {
    return this.isYoutube() || this.isVimeo() || this.isHTML5();
  }, Mn.prototype.isHTML5 = function () {
    return "VIDEO" === this.el.tagName;
  }, Mn.prototype.isIFrame = function () {
    return "IFRAME" === this.el.tagName;
  }, Mn.prototype.isYoutube = function () {
    return this.isIFrame() && !!this.el.src.match(/\/\/.*?youtube(-nocookie)?\.[a-z]+\/(watch\?v=[^&\s]+|embed)|youtu\.be\/.*/);
  }, Mn.prototype.isVimeo = function () {
    return this.isIFrame() && !!this.el.src.match(/vimeo\.com\/video\/.*/);
  }, Mn.prototype.enableApi = function () {
    var e = this;if (this.ready) return this.ready;var n,
        i = this.isYoutube(),
        r = this.isVimeo();return i || r ? this.ready = new qt(function (t) {
      Bt(e.el, "load", function () {
        if (i) {
          var t = function t() {
            return Bn(e.el, { event: "listening", id: e.id });
          };n = setInterval(t, 100), t();
        }
      }), function (i) {
        return new qt(function (n) {
          Bt(window, "message", function (t, e) {
            return n(e);
          }, !1, function (t) {
            var e = t.data;if (e && B(e)) {
              try {
                e = JSON.parse(e);
              } catch (t) {
                return;
              }return e && i(e);
            }
          });
        });
      }(function (t) {
        return i && t.id === e.id && "onReady" === t.event || r && Number(t.player_id) === e.id;
      }).then(function () {
        t(), n && clearInterval(n);
      }), it(e.el, "src", e.el.src + (x(e.el.src, "?") ? "&" : "?") + (i ? "enablejsapi=1" : "api=1&player_id=" + e.id));
    }) : qt.resolve();
  }, Mn.prototype.play = function () {
    var t = this;if (this.isVideo()) if (this.isIFrame()) this.enableApi().then(function () {
      return Bn(t.el, { func: "playVideo", method: "play" });
    });else if (this.isHTML5()) try {
      var e = this.el.play();e && e.catch(Q);
    } catch (t) {}
  }, Mn.prototype.pause = function () {
    var t = this;this.isVideo() && (this.isIFrame() ? this.enableApi().then(function () {
      return Bn(t.el, { func: "pauseVideo", method: "pause" });
    }) : this.isHTML5() && this.el.pause());
  }, Mn.prototype.mute = function () {
    var t = this;this.isVideo() && (this.isIFrame() ? this.enableApi().then(function () {
      return Bn(t.el, { func: "mute", method: "setVolume", value: 0 });
    }) : this.isHTML5() && (this.el.muted = !0, it(this.el, "muted", "")));
  };var zn = "IntersectionObserver" in window ? window.IntersectionObserver : function () {
    function t(e, t) {
      var n = this;void 0 === t && (t = {});var i = t.rootMargin;void 0 === i && (i = "0 0"), this.targets = [];var r,
          o = (i || "0 0").split(" ").map(F),
          s = o[0],
          a = o[1];this.offsetTop = s, this.offsetLeft = a, this.apply = function () {
        r || (r = requestAnimationFrame(function () {
          return setTimeout(function () {
            var t = n.takeRecords();t.length && e(t, n), r = !1;
          });
        }));
      }, this.off = Mt(window, "scroll resize load", this.apply, { passive: !0, capture: !0 });
    }return t.prototype.takeRecords = function () {
      var n = this;return this.targets.filter(function (t) {
        var e = fn(t.target, n.offsetTop, n.offsetLeft);if (null === t.isIntersecting || e ^ t.isIntersecting) return t.isIntersecting = e, !0;
      });
    }, t.prototype.observe = function (t) {
      this.targets.push({ target: t, isIntersecting: null }), this.apply();
    }, t.prototype.disconnect = function () {
      this.targets = [], this.off();
    }, t;
  }();function Dn(t) {
    return !(!w(t, "uk-") && !w(t, "data-uk-")) && f(t.replace("data-uk-", "").replace("uk-", ""));
  }function Hn(t) {
    this._init(t);
  }var Pn, Ln, jn, Fn, Wn, Vn, qn, Rn, Yn;function Un(t, e) {
    if (t) for (var n in t) {
      t[n]._connected && t[n]._callUpdate(e);
    }
  }function Xn(t, e) {
    var n = {},
        i = t.args;void 0 === i && (i = []);var r = t.props;void 0 === r && (r = {});var o = t.el;if (!r) return n;for (var s in r) {
      var a = d(s),
          u = st(o, a);if (!P(u)) {
        if (u = r[s] === Boolean && "" === u || Zn(r[s], u), "target" === a && (!u || w(u, "_"))) continue;n[s] = u;
      }
    }var c = Nn(st(o, e), i);for (var h in c) {
      var l = f(h);void 0 !== r[l] && (n[l] = Zn(r[l], c[h]));
    }return n;
  }function Gn(i, r, o) {
    Object.defineProperty(i, r, { enumerable: !0, get: function get() {
        var t = i._computeds,
            e = i.$props,
            n = i.$el;return c(t, r) || (t[r] = (o.get || o).call(i, e, n)), t[r];
      }, set: function set(t) {
        var e = i._computeds;e[r] = o.set ? o.set.call(i, t) : t, P(e[r]) && delete e[r];
      } });
  }function Jn(e, n, i) {
    A(n) || (n = { name: i, handler: n });var t = n.name,
        r = n.el,
        o = n.handler,
        s = n.capture,
        a = n.passive,
        u = n.delegate,
        c = n.filter,
        h = n.self;r = I(r) ? r.call(e) : r || e.$el, $(r) ? r.forEach(function (t) {
      return Jn(e, X({}, n, { el: t }), i);
    }) : !r || c && !c.call(e) || (o = function (e) {
      return function (t) {
        return $(t.detail) ? e.apply(void 0, [t].concat(t.detail)) : e(t);
      };
    }(B(o) ? e[o] : l(o, e)), h && (o = function (e) {
      return function (t) {
        if (t.target === t.currentTarget || t.target === t.current) return e.call(null, t);
      };
    }(o)), e._events.push(Mt(r, t, u ? B(u) ? u : u.call(e) : null, o, O(a) ? { passive: a, capture: s } : s)));
  }function Kn(t, e) {
    return t.every(function (t) {
      return !t || !c(t, e);
    });
  }function Zn(t, e) {
    return t === Boolean ? L(e) : t === Number ? j(e) : "list" === t ? q(e) : t ? t(e) : e;
  }Hn.util = Object.freeze({ ajax: Gt, getImage: Jt, transition: Ye, Transition: Ue, animate: Je, Animation: Ze, attr: it, hasAttr: rt, removeAttr: ot, data: st, addClass: Ee, removeClass: Ce, removeClasses: _e, replaceClass: Ne, hasClass: Me, toggleClass: Oe, positionAt: tn, offset: en, position: rn, height: on, width: sn, boxModelAdjust: un, flipPosition: dn, isInView: fn, scrolledOver: pn, scrollTop: mn, offsetPosition: vn, toPx: gn, ready: ue, index: ce, getIndex: he, empty: le, html: de, prepend: function prepend(e, t) {
      return (e = Te(e)).hasChildNodes() ? ve(t, function (t) {
        return e.insertBefore(t, e.firstChild);
      }) : fe(e, t);
    }, append: fe, before: pe, after: me, remove: ge, wrapAll: we, wrapInner: be, unwrap: xe, fragment: $e, apply: Ie, $: Te, $$: Ae, isIE: Kt, isRtl: Zt, hasTouch: ee, pointerDown: ne, pointerMove: ie, pointerUp: re, pointerEnter: oe, pointerLeave: se, pointerCancel: ae, on: Mt, off: Ot, once: Bt, trigger: zt, createEvent: Dt, toEventTargets: jt, isTouch: Ft, getEventPos: Wt, fastdom: yn, isVoidElement: At, isVisible: St, selInput: Et, isInput: Ct, filter: _t, within: Nt, bind: l, hasOwn: c, hyphenate: d, camelize: f, ucfirst: p, startsWith: w, endsWith: h, includes: x, findIndex: k, isArray: $, isFunction: I, isObject: T, isPlainObject: A, isWindow: S, isDocument: E, isJQuery: C, isNode: _, isNodeCollection: M, isBoolean: O, isString: B, isNumber: z, isNumeric: D, isEmpty: H, isUndefined: P, toBoolean: L, toNumber: j, toFloat: F, toNode: W, toNodes: V, toList: q, toMs: R, isEqual: Y, swap: U, assign: X, each: G, sortBy: J, uniqueBy: K, clamp: Z, noop: Q, intersectRect: tt, pointInRect: et, Dimensions: nt, MouseTracker: Tn, mergeOptions: _n, parseOptions: Nn, Player: Mn, Promise: qt, Deferred: Vt, IntersectionObserver: zn, query: at, queryAll: ut, find: ht, findAll: lt, matches: bt, closest: yt, parents: kt, escape: It, css: Pe, getStyles: Le, getStyle: je, getCssVar: We, propName: qe }), Hn.data = "__uikit__", Hn.prefix = "uk-", Hn.options = {}, jn = (Pn = Hn).data, Pn.use = function (t) {
    if (!t.installed) return t.call(null, this), t.installed = !0, this;
  }, Pn.mixin = function (t, e) {
    (e = (B(e) ? Pn.component(e) : e) || this).options = _n(e.options, t);
  }, Pn.extend = function (t) {
    function e(t) {
      this._init(t);
    }return t = t || {}, ((e.prototype = Object.create(this.prototype)).constructor = e).options = _n(this.options, t), e.super = this, e.extend = this.extend, e;
  }, Pn.update = function (t, e) {
    (function t(e, n) {
      e && e !== document.body && e.parentNode && (t(e.parentNode, n), n(e.parentNode));
    })(t = t ? W(t) : document.body, function (t) {
      return Un(t[jn], e);
    }), Ie(t, function (t) {
      return Un(t[jn], e);
    });
  }, Object.defineProperty(Pn, "container", { get: function get() {
      return Ln || document.body;
    }, set: function set(t) {
      Ln = Te(t);
    } }), (Fn = Hn).prototype._callHook = function (t) {
    var e = this,
        n = this.$options[t];n && n.forEach(function (t) {
      return t.call(e);
    });
  }, Fn.prototype._callConnected = function () {
    this._connected || (this._data = {}, this._computeds = {}, this._initProps(), this._callHook("beforeConnect"), this._connected = !0, this._initEvents(), this._initObserver(), this._callHook("connected"), this._callUpdate());
  }, Fn.prototype._callDisconnected = function () {
    this._connected && (this._callHook("beforeDisconnect"), this._observer && (this._observer.disconnect(), this._observer = null), this._unbindEvents(), this._callHook("disconnected"), this._connected = !1);
  }, Fn.prototype._callUpdate = function (t) {
    var o = this;void 0 === t && (t = "update");var s = t.type || t;x(["update", "resize"], s) && this._callWatches();var e = this.$options.update,
        n = this._frames,
        a = n.reads,
        u = n.writes;e && e.forEach(function (t, e) {
      var n = t.read,
          i = t.write,
          r = t.events;"update" !== s && !x(r, s) || (n && !x(yn.reads, a[e]) && (a[e] = yn.read(function () {
        var t = o._connected && n.call(o, o._data, s);!1 === t && i ? yn.clear(u[e]) : A(t) && X(o._data, t);
      })), i && !x(yn.writes, u[e]) && (u[e] = yn.write(function () {
        return o._connected && i.call(o, o._data, s);
      })));
    });
  }, Vn = 0, (Wn = Hn).prototype._init = function (t) {
    (t = t || {}).data = function (t, e) {
      var n = t.data,
          i = (t.el, e.args),
          r = e.props;if (void 0 === r && (r = {}), n = $(n) ? H(i) ? void 0 : n.slice(0, i.length).reduce(function (t, e, n) {
        return A(e) ? X(t, e) : t[i[n]] = e, t;
      }, {}) : n) for (var o in n) {
        P(n[o]) ? delete n[o] : n[o] = r[o] ? Zn(r[o], n[o]) : n[o];
      }return n;
    }(t, this.constructor.options), this.$options = _n(this.constructor.options, t, this), this.$el = null, this.$props = {}, this._frames = { reads: {}, writes: {} }, this._events = [], this._uid = Vn++, this._initData(), this._initMethods(), this._initComputeds(), this._callHook("created"), t.el && this.$mount(t.el);
  }, Wn.prototype._initData = function () {
    var t = this.$options.data;for (var e in void 0 === t && (t = {}), t) {
      this.$props[e] = this[e] = t[e];
    }
  }, Wn.prototype._initMethods = function () {
    var t = this.$options.methods;if (t) for (var e in t) {
      this[e] = l(t[e], this);
    }
  }, Wn.prototype._initComputeds = function () {
    var t = this.$options.computed;if (this._computeds = {}, t) for (var e in t) {
      Gn(this, e, t[e]);
    }
  }, Wn.prototype._callWatches = function () {
    var t = this.$options.computed,
        e = this._computeds;for (var n in e) {
      var i = e[n];delete e[n], t[n].watch && !Y(i, this[n]) && t[n].watch.call(this, this[n], i);
    }
  }, Wn.prototype._initProps = function (t) {
    var e;for (e in t = t || Xn(this.$options, this.$name)) {
      P(t[e]) || (this.$props[e] = t[e]);
    }var n = [this.$options.computed, this.$options.methods];for (e in this.$props) {
      e in t && Kn(n, e) && (this[e] = this.$props[e]);
    }
  }, Wn.prototype._initEvents = function () {
    var n = this,
        t = this.$options.events;t && t.forEach(function (t) {
      if (c(t, "handler")) Jn(n, t);else for (var e in t) {
        Jn(n, t[e], e);
      }
    });
  }, Wn.prototype._unbindEvents = function () {
    this._events.forEach(function (t) {
      return t();
    }), this._events = [];
  }, Wn.prototype._initObserver = function () {
    var n = this,
        t = this.$options,
        i = t.attrs,
        e = t.props,
        r = t.el;if (!this._observer && e && !1 !== i) {
      i = $(i) ? i : Object.keys(e), this._observer = new MutationObserver(function () {
        var e = Xn(n.$options, n.$name);i.some(function (t) {
          return !P(e[t]) && e[t] !== n.$props[t];
        }) && n.$reset();
      });var o = i.map(function (t) {
        return d(t);
      }).concat(this.$name);this._observer.observe(r, { attributes: !0, attributeFilter: o.concat(o.map(function (t) {
          return "data-" + t;
        })) });
    }
  }, Rn = (qn = Hn).data, Yn = {}, qn.component = function (s, t) {
    if (!t) return A(Yn[s]) && (Yn[s] = qn.extend(Yn[s])), Yn[s];qn[s] = function (t, n) {
      for (var e = arguments.length, i = Array(e); e--;) {
        i[e] = arguments[e];
      }var r = qn.component(s);return A(t) ? new r({ data: t }) : r.options.functional ? new r({ data: [].concat(i) }) : t && t.nodeType ? o(t) : Ae(t).map(o)[0];function o(t) {
        var e = qn.getComponent(t, s);if (e) {
          if (!n) return e;e.$destroy();
        }return new r({ el: t, data: n });
      }
    };var e = A(t) ? X({}, t) : t.options;if (e.name = s, e.install && e.install(qn, e, s), qn._initialized && !e.functional) {
      var n = d(s);yn.read(function () {
        return qn[s]("[uk-" + n + "],[data-uk-" + n + "]");
      });
    }return Yn[s] = A(t) ? e : t;
  }, qn.getComponents = function (t) {
    return t && t[Rn] || {};
  }, qn.getComponent = function (t, e) {
    return qn.getComponents(t)[e];
  }, qn.connect = function (t) {
    if (t[Rn]) for (var e in t[Rn]) {
      t[Rn][e]._callConnected();
    }for (var n = 0; n < t.attributes.length; n++) {
      var i = Dn(t.attributes[n].name);i && i in Yn && qn[i](t);
    }
  }, qn.disconnect = function (t) {
    for (var e in t[Rn]) {
      t[Rn][e]._callDisconnected();
    }
  }, function (i) {
    var r = i.data;i.prototype.$mount = function (t) {
      var e = this.$options.name;t[r] || (t[r] = {}), t[r][e] || ((t[r][e] = this).$el = this.$options.el = this.$options.el || t, Nt(t, document) && this._callConnected());
    }, i.prototype.$emit = function (t) {
      this._callUpdate(t);
    }, i.prototype.$reset = function () {
      this._callDisconnected(), this._callConnected();
    }, i.prototype.$destroy = function (t) {
      void 0 === t && (t = !1);var e = this.$options,
          n = e.el,
          i = e.name;n && this._callDisconnected(), this._callHook("destroy"), n && n[r] && (delete n[r][i], H(n[r]) || delete n[r], t && ge(this.$el));
    }, i.prototype.$create = function (t, e, n) {
      return i[t](e, n);
    }, i.prototype.$update = i.update, i.prototype.$getComponent = i.getComponent;var e = {};Object.defineProperties(i.prototype, { $container: Object.getOwnPropertyDescriptor(i, "container"), $name: { get: function get() {
          var t = this.$options.name;return e[t] || (e[t] = i.prefix + d(t)), e[t];
        } } });
  }(Hn);var Qn = { connected: function connected() {
      Me(this.$el, this.$name) || Ee(this.$el, this.$name);
    } },
      ti = { props: { cls: Boolean, animation: "list", duration: Number, origin: String, transition: String, queued: Boolean }, data: { cls: !1, animation: [!1], duration: 200, origin: !1, transition: "linear", queued: !1, initProps: { overflow: "", height: "", paddingTop: "", paddingBottom: "", marginTop: "", marginBottom: "" }, hideProps: { overflow: "hidden", height: 0, paddingTop: 0, paddingBottom: 0, marginTop: 0, marginBottom: 0 } }, computed: { hasAnimation: function hasAnimation(t) {
        return !!t.animation[0];
      }, hasTransition: function hasTransition(t) {
        var e = t.animation;return this.hasAnimation && !0 === e[0];
      } }, methods: { toggleElement: function toggleElement(c, h, l) {
        var d = this;return new qt(function (t) {
          c = V(c);function e(t) {
            return qt.all(t.map(function (t) {
              return d._toggleElement(t, h, l);
            }));
          }var n,
              i = c.filter(function (t) {
            return d.isToggled(t);
          }),
              r = c.filter(function (t) {
            return !x(i, t);
          });if (d.queued && P(l) && P(h) && d.hasAnimation && !(c.length < 2)) {
            var o = document.body,
                s = o.scrollTop,
                a = i[0],
                u = Ze.inProgress(a) && Me(a, "uk-animation-leave") || Ue.inProgress(a) && "0px" === a.style.height;n = e(i), u || (n = n.then(function () {
              var t = e(r);return o.scrollTop = s, t;
            }));
          } else n = e(r.concat(i));n.then(t, Q);
        });
      }, toggleNow: function toggleNow(e, n) {
        var i = this;return new qt(function (t) {
          return qt.all(V(e).map(function (t) {
            return i._toggleElement(t, n, !1);
          })).then(t, Q);
        });
      }, isToggled: function isToggled(t) {
        var e = V(t || this.$el);return this.cls ? Me(e, this.cls.split(" ")[0]) : !rt(e, "hidden");
      }, updateAria: function updateAria(t) {
        !1 === this.cls && it(t, "aria-hidden", !this.isToggled(t));
      }, _toggleElement: function _toggleElement(t, e, n) {
        var i = this;if (e = O(e) ? e : Ze.inProgress(t) ? Me(t, "uk-animation-leave") : Ue.inProgress(t) ? "0px" === t.style.height : !this.isToggled(t), !zt(t, "before" + (e ? "show" : "hide"), [this])) return qt.reject();var r = (I(n) ? n : !1 !== n && this.hasAnimation ? this.hasTransition ? function (t) {
          var s = t.isToggled,
              a = t.duration,
              u = t.initProps,
              c = t.hideProps,
              h = t.transition,
              l = t._toggle;return function (t, e) {
            var n = Ue.inProgress(t),
                i = t.hasChildNodes ? F(Pe(t.firstElementChild, "marginTop")) + F(Pe(t.lastElementChild, "marginBottom")) : 0,
                r = St(t) ? on(t) + (n ? 0 : i) : 0;Ue.cancel(t), s(t) || l(t, !0), on(t, ""), yn.flush();var o = on(t) + (n ? 0 : i);return on(t, r), (e ? Ue.start(t, X({}, u, { overflow: "hidden", height: o }), Math.round(a * (1 - r / o)), h) : Ue.start(t, c, Math.round(a * (r / o)), h).then(function () {
              return l(t, !1);
            })).then(function () {
              return Pe(t, u);
            });
          };
        }(this) : function (t) {
          var n = t.animation,
              i = t.duration,
              r = t.origin,
              o = t._toggle;return function (t, e) {
            return Ze.cancel(t), e ? (o(t, !0), Ze.in(t, n[0], i, r)) : Ze.out(t, n[1] || n[0], i, r).then(function () {
              return o(t, !1);
            });
          };
        }(this) : this._toggle)(t, e);zt(t, e ? "show" : "hide", [this]);function o() {
          zt(t, e ? "shown" : "hidden", [i]), i.$update(t);
        }return r ? r.then(o) : qt.resolve(o());
      }, _toggle: function _toggle(t, e) {
        var n;t && (e = Boolean(e), this.cls ? (n = x(this.cls, " ") || e !== Me(t, this.cls)) && Oe(t, this.cls, x(this.cls, " ") ? void 0 : e) : (n = e === rt(t, "hidden")) && it(t, "hidden", e ? null : ""), Ae("[autofocus]", t).some(function (t) {
          return St(t) ? t.focus() || !0 : t.blur();
        }), this.updateAria(t), n && this.$update(t));
      } } };var ei = { mixins: [Qn, ti], props: { targets: String, active: null, collapsible: Boolean, multiple: Boolean, toggle: String, content: String, transition: String }, data: { targets: "> *", active: !1, animation: [!0], collapsible: !0, multiple: !1, clsOpen: "uk-open", toggle: "> .uk-accordion-title", content: "> .uk-accordion-content", transition: "ease" }, computed: { items: function items(t, e) {
        return Ae(t.targets, e);
      } }, events: [{ name: "click", delegate: function delegate() {
        return this.targets + " " + this.$props.toggle;
      }, handler: function handler(t) {
        t.preventDefault(), this.toggle(ce(Ae(this.targets + " " + this.$props.toggle, this.$el), t.current));
      } }], connected: function connected() {
      if (!1 !== this.active) {
        var t = this.items[Number(this.active)];t && !Me(t, this.clsOpen) && this.toggle(t, !1);
      }
    }, update: function update() {
      var e = this;this.items.forEach(function (t) {
        return e._toggle(Te(e.content, t), Me(t, e.clsOpen));
      });var t = !this.collapsible && !Me(this.items, this.clsOpen) && this.items[0];t && this.toggle(t, !1);
    }, methods: { toggle: function toggle(r, o) {
        var s = this,
            t = he(r, this.items),
            a = _t(this.items, "." + this.clsOpen);(r = this.items[t]) && [r].concat(!this.multiple && !x(a, r) && a || []).forEach(function (t) {
          var e = t === r,
              n = e && !Me(t, s.clsOpen);if (n || !e || s.collapsible || !(a.length < 2)) {
            Oe(t, s.clsOpen, n);var i = t._wrapper ? t._wrapper.firstElementChild : Te(s.content, t);t._wrapper || (t._wrapper = we(i, "<div>"), it(t._wrapper, "hidden", n ? "" : null)), s._toggle(i, !0), s.toggleElement(t._wrapper, n, o).then(function () {
              Me(t, s.clsOpen) === n && (n || s._toggle(i, !1), t._wrapper = null, xe(i));
            });
          }
        });
      } } },
      ni = { mixins: [Qn, ti], args: "animation", props: { close: String }, data: { animation: [!0], selClose: ".uk-alert-close", duration: 150, hideProps: X({ opacity: 0 }, ti.data.hideProps) }, events: [{ name: "click", delegate: function delegate() {
        return this.selClose;
      }, handler: function handler(t) {
        t.preventDefault(), this.close();
      } }], methods: { close: function close() {
        var t = this;this.toggleElement(this.$el).then(function () {
          return t.$destroy(!0);
        });
      } } };function ii(r) {
    ue(function () {
      var n;r.update(), Mt(window, "load resize", function () {
        return r.update(null, "resize");
      }), Mt(document, "loadedmetadata load", function (t) {
        var e = t.target;return r.update(e, "resize");
      }, !0), Mt(window, "scroll", function (t) {
        if (!n) {
          n = !0, yn.write(function () {
            return n = !1;
          });var e = t.target;r.update(1 !== e.nodeType ? document.body : e, t.type);
        }
      }, { passive: !0, capture: !0 });var e,
          i = 0;Mt(document, "animationstart", function (t) {
        var e = t.target;(Pe(e, "animationName") || "").match(/^uk-.*(left|right)/) && (i++, Pe(document.body, "overflowX", "hidden"), setTimeout(function () {
          --i || Pe(document.body, "overflowX", "");
        }, R(Pe(e, "animationDuration")) + 100));
      }, !0), Mt(document, ne, function (t) {
        if (e && e(), Ft(t)) {
          var r = Wt(t),
              o = "tagName" in t.target ? t.target : t.target.parentNode;e = Bt(document, re, function (t) {
            var e = Wt(t),
                n = e.x,
                i = e.y;(o && n && 100 < Math.abs(r.x - n) || i && 100 < Math.abs(r.y - i)) && setTimeout(function () {
              zt(o, "swipe"), zt(o, "swipe" + function (t, e, n, i) {
                return Math.abs(t - n) >= Math.abs(e - i) ? 0 < t - n ? "Left" : "Right" : 0 < e - i ? "Up" : "Down";
              }(r.x, r.y, n, i));
            });
          });
        }
      }, { passive: !0 });
    });
  }var ri,
      oi,
      si = { args: "autoplay", props: { automute: Boolean, autoplay: Boolean }, data: { automute: !1, autoplay: !0 }, computed: { inView: function inView(t) {
        return "inview" === t.autoplay;
      } }, connected: function connected() {
      this.inView && !rt(this.$el, "preload") && (this.$el.preload = "none"), this.player = new Mn(this.$el), this.automute && this.player.mute();
    }, update: { read: function read() {
        return !!this.player && { visible: St(this.$el) && "hidden" !== Pe(this.$el, "visibility"), inView: this.inView && fn(this.$el) };
      }, write: function write(t) {
        var e = t.visible,
            n = t.inView;!e || this.inView && !n ? this.player.pause() : (!0 === this.autoplay || this.inView && n) && this.player.play();
      }, events: ["resize", "scroll"] } },
      ai = { mixins: [Qn, si], props: { width: Number, height: Number }, data: { automute: !0 }, update: { read: function read() {
        var t = this.$el;if (!St(t)) return !1;var e = t.parentNode;return { height: e.offsetHeight, width: e.offsetWidth };
      }, write: function write(t) {
        var e = t.height,
            n = t.width,
            i = this.$el,
            r = this.width || i.naturalWidth || i.videoWidth || i.clientWidth,
            o = this.height || i.naturalHeight || i.videoHeight || i.clientHeight;r && o && Pe(i, nt.cover({ width: r, height: o }, { width: n + (n % 2 ? 1 : 0), height: e + (e % 2 ? 1 : 0) }));
      }, events: ["resize"] } },
      ui = { mixins: [{ props: { pos: String, offset: null, flip: Boolean, clsPos: String }, data: { pos: "bottom-" + (Zt ? "right" : "left"), flip: !0, offset: !1, clsPos: "" }, computed: { pos: function pos(t) {
          var e = t.pos;return (e + (x(e, "-") ? "" : "-center")).split("-");
        }, dir: function dir() {
          return this.pos[0];
        }, align: function align() {
          return this.pos[1];
        } }, methods: { positionAt: function positionAt(t, e, n) {
          var i;_e(t, this.clsPos + "-(top|bottom|left|right)(-[a-z]+)?"), Pe(t, { top: "", left: "" });var r = this.offset,
              o = this.getAxis();D(r) || (r = (i = Te(r)) ? en(i)["x" === o ? "left" : "top"] - en(e)["x" === o ? "right" : "bottom"] : 0);var s = tn(t, e, "x" === o ? dn(this.dir) + " " + this.align : this.align + " " + dn(this.dir), "x" === o ? this.dir + " " + this.align : this.align + " " + this.dir, "x" === o ? "" + ("left" === this.dir ? -r : r) : " " + ("top" === this.dir ? -r : r), null, this.flip, n).target,
              a = s.x,
              u = s.y;this.dir = "x" === o ? a : u, this.align = "x" === o ? u : a, Oe(t, this.clsPos + "-" + this.dir + "-" + this.align, !1 === this.offset);
        }, getAxis: function getAxis() {
          return "top" === this.dir || "bottom" === this.dir ? "y" : "x";
        } } }, ti], args: "pos", props: { mode: "list", toggle: Boolean, boundary: Boolean, boundaryAlign: Boolean, delayShow: Number, delayHide: Number, clsDrop: String }, data: { mode: ["click", "hover"], toggle: "- *", boundary: window, boundaryAlign: !1, delayShow: 0, delayHide: 800, clsDrop: !1, hoverIdle: 200, animation: ["uk-animation-fade"], cls: "uk-open" }, computed: { boundary: function boundary(t, e) {
        return at(t.boundary, e);
      }, clsDrop: function clsDrop(t) {
        return t.clsDrop || "uk-" + this.$options.name;
      }, clsPos: function clsPos() {
        return this.clsDrop;
      } }, created: function created() {
      this.tracker = new Tn();
    }, connected: function connected() {
      Ee(this.$el, this.clsDrop);var t = this.$props.toggle;this.toggle = t && this.$create("toggle", at(t, this.$el), { target: this.$el, mode: this.mode }), this.toggle || zt(this.$el, "updatearia");
    }, events: [{ name: "click", delegate: function delegate() {
        return "." + this.clsDrop + "-close";
      }, handler: function handler(t) {
        t.preventDefault(), this.hide(!1);
      } }, { name: "click", delegate: function delegate() {
        return 'a[href^="#"]';
      }, handler: function handler(t) {
        var e = t.target.hash;e || t.preventDefault(), e && Nt(e, this.$el) || this.hide(!1);
      } }, { name: "beforescroll", handler: function handler() {
        this.hide(!1);
      } }, { name: "toggle", self: !0, handler: function handler(t, e) {
        t.preventDefault(), this.isToggled() ? this.hide(!1) : this.show(e, !1);
      } }, { name: oe, filter: function filter() {
        return x(this.mode, "hover");
      }, handler: function handler(t) {
        Ft(t) || (ri && ri !== this && ri.toggle && x(ri.toggle.mode, "hover") && !Nt(t.target, ri.toggle.$el) && !et({ x: t.pageX, y: t.pageY }, en(ri.$el)) && ri.hide(!1), t.preventDefault(), this.show(this.toggle));
      } }, { name: "toggleshow", handler: function handler(t, e) {
        e && !x(e.target, this.$el) || (t.preventDefault(), this.show(e || this.toggle));
      } }, { name: "togglehide " + se, handler: function handler(t, e) {
        Ft(t) || e && !x(e.target, this.$el) || (t.preventDefault(), this.toggle && x(this.toggle.mode, "hover") && this.hide());
      } }, { name: "beforeshow", self: !0, handler: function handler() {
        this.clearTimers(), Ze.cancel(this.$el), this.position();
      } }, { name: "show", self: !0, handler: function handler() {
        this.tracker.init(), zt(this.$el, "updatearia"), function () {
          if (oi) return;oi = !0, Mt(document, re, function (t) {
            var e,
                n = t.target,
                i = t.defaultPrevented;if (!i) for (; ri && ri !== e && !Nt(n, ri.$el) && (!ri.toggle || !Nt(n, ri.toggle.$el));) {
              (e = ri).hide(!1);
            }
          });
        }();
      } }, { name: "beforehide", self: !0, handler: function handler() {
        this.clearTimers();
      } }, { name: "hide", handler: function handler(t) {
        var e = t.target;this.$el === e ? (ri = this.isActive() ? null : ri, zt(this.$el, "updatearia"), this.tracker.cancel()) : ri = null === ri && Nt(e, this.$el) && this.isToggled() ? this : ri;
      } }, { name: "updatearia", self: !0, handler: function handler(t, e) {
        t.preventDefault(), this.updateAria(this.$el), (e || this.toggle) && (it((e || this.toggle).$el, "aria-expanded", this.isToggled() ? "true" : "false"), Oe(this.toggle.$el, this.cls, this.isToggled()));
      } }], update: { write: function write() {
        this.isToggled() && !Ze.inProgress(this.$el) && this.position();
      }, events: ["resize"] }, methods: { show: function show(e, n) {
        var i = this;void 0 === n && (n = !0);function r() {
          return !i.isToggled() && i.toggleElement(i.$el, !0);
        }function t() {
          if (i.toggle = e || i.toggle, i.clearTimers(), !i.isActive()) if (n && ri && ri !== i && ri.isDelaying) i.showTimer = setTimeout(i.show, 10);else {
            if (i.isParentOf(ri)) {
              if (!ri.hideTimer) return;ri.hide(!1);
            } else if (ri && i.isChildOf(ri)) ri.clearTimers();else if (ri && !i.isChildOf(ri) && !i.isParentOf(ri)) for (var t; ri && ri !== t && !i.isChildOf(ri);) {
              (t = ri).hide(!1);
            }n && i.delayShow ? i.showTimer = setTimeout(r, i.delayShow) : r(), ri = i;
          }
        }e && this.toggle && e.$el !== this.toggle.$el ? (Bt(this.$el, "hide", t), this.hide(!1)) : t();
      }, hide: function hide(t) {
        var e = this;void 0 === t && (t = !0);function n() {
          return e.toggleNow(e.$el, !1);
        }this.clearTimers(), this.isDelaying = this.tracker.movesTo(this.$el), t && this.isDelaying ? this.hideTimer = setTimeout(this.hide, this.hoverIdle) : t && this.delayHide ? this.hideTimer = setTimeout(n, this.delayHide) : n();
      }, clearTimers: function clearTimers() {
        clearTimeout(this.showTimer), clearTimeout(this.hideTimer), this.showTimer = null, this.hideTimer = null, this.isDelaying = !1;
      }, isActive: function isActive() {
        return ri === this;
      }, isChildOf: function isChildOf(t) {
        return t && t !== this && Nt(this.$el, t.$el);
      }, isParentOf: function isParentOf(t) {
        return t && t !== this && Nt(t.$el, this.$el);
      }, position: function position() {
        _e(this.$el, this.clsDrop + "-(stack|boundary)"), Pe(this.$el, { top: "", left: "", display: "block" }), Oe(this.$el, this.clsDrop + "-boundary", this.boundaryAlign);var t = en(this.boundary),
            e = this.boundaryAlign ? t : en(this.toggle.$el);if ("justify" === this.align) {
          var n = "y" === this.getAxis() ? "width" : "height";Pe(this.$el, n, e[n]);
        } else this.$el.offsetWidth > Math.max(t.right - e.left, e.right - t.left) && Ee(this.$el, this.clsDrop + "-stack");this.positionAt(this.$el, this.boundaryAlign ? this.boundary : this.toggle.$el, this.boundary), Pe(this.$el, "display", "");
      } } };var ci = { extends: ui },
      hi = { mixins: [Qn], args: "target", props: { target: Boolean }, data: { target: !1 }, computed: { input: function input(t, e) {
        return Te(Et, e);
      }, state: function state() {
        return this.input.nextElementSibling;
      }, target: function target(t, e) {
        var n = t.target;return n && (!0 === n && this.input.parentNode === e && this.input.nextElementSibling || at(n, e));
      } }, update: function update() {
      var t = this.target,
          e = this.input;if (t) {
        var n,
            i = Ct(t) ? "value" : "textContent",
            r = t[i],
            o = e.files && e.files[0] ? e.files[0].name : bt(e, "select") && (n = Ae("option", e).filter(function (t) {
          return t.selected;
        })[0]) ? n.textContent : e.value;r !== o && (t[i] = o);
      }
    }, events: { change: function change() {
        this.$emit();
      } } },
      li = { update: { read: function read(t) {
        var e = fn(this.$el);if (!e || t.isInView === e) return !1;t.isInView = e;
      }, write: function write() {
        this.$el.src = this.$el.src;
      }, events: ["scroll", "resize"] } },
      di = { props: { margin: String, firstColumn: Boolean }, data: { margin: "uk-margin-small-top", firstColumn: "uk-first-column" }, update: { read: function read(t) {
        var e = this.$el.children;if (!e.length || !St(this.$el)) return t.rows = [[]];t.rows = fi(e), t.stacks = !t.rows.some(function (t) {
          return 1 < t.length;
        });
      }, write: function write(t) {
        var i = this;t.rows.forEach(function (t, n) {
          return t.forEach(function (t, e) {
            Oe(t, i.margin, 0 !== n), Oe(t, i.firstColumn, 0 === e);
          });
        });
      }, events: ["resize"] } };function fi(t) {
    for (var e = [[]], n = 0; n < t.length; n++) {
      var i = t[n],
          r = pi(i);if (r.height) for (var o = e.length - 1; 0 <= o; o--) {
        var s = e[o];if (!s[0]) {
          s.push(i);break;
        }var a = void 0;if (a = s[0].offsetParent === i.offsetParent ? pi(s[0]) : (r = pi(i, !0), pi(s[0], !0)), r.top >= a.bottom - 1) {
          e.push([i]);break;
        }if (r.bottom > a.top) {
          if (r.left < a.left && !Zt) {
            s.unshift(i);break;
          }s.push(i);break;
        }if (0 === o) {
          e.unshift([i]);break;
        }
      }
    }return e;
  }function pi(t, e) {
    var n;void 0 === e && (e = !1);var i = t.offsetTop,
        r = t.offsetLeft,
        o = t.offsetHeight;return e && (i = (n = vn(t))[0], r = n[1]), { top: i, left: r, height: o, bottom: i + o };
  }var mi = { extends: di, mixins: [Qn], name: "grid", props: { masonry: Boolean, parallax: Number }, data: { margin: "uk-grid-margin", clsStack: "uk-grid-stack", masonry: !1, parallax: 0 }, computed: { length: function length(t, e) {
        return e.children.length;
      }, parallax: function parallax(t) {
        var e = t.parallax;return e && this.length ? Math.abs(e) : "";
      } }, connected: function connected() {
      this.masonry && Ee(this.$el, "uk-flex-top uk-flex-wrap-top");
    }, update: [{ read: function read(t) {
        var r = t.rows;(this.masonry || this.parallax) && (r = r.map(function (t) {
          return J(t, "offsetLeft");
        }), Zt && r.map(function (t) {
          return t.reverse();
        }));var e = r.some(function (t) {
          return t.some(Ue.inProgress);
        }),
            n = !1,
            i = "";if (this.masonry && this.length) {
          var o = 0;n = r.reduce(function (n, t, i) {
            return n[i] = t.map(function (t, e) {
              return 0 === i ? 0 : F(n[i - 1][e]) + (o - F(r[i - 1][e] && r[i - 1][e].offsetHeight));
            }), o = t.reduce(function (t, e) {
              return Math.max(t, e.offsetHeight);
            }, 0), n;
          }, []), i = function (t) {
            return Math.max.apply(Math, t.reduce(function (n, t) {
              return t.forEach(function (t, e) {
                return n[e] = (n[e] || 0) + t.offsetHeight;
              }), n;
            }, []));
          }(r) + function (t, e) {
            var n = V(t.children),
                i = n.filter(function (t) {
              return Me(t, e);
            })[0];return F(i ? Pe(i, "marginTop") : Pe(n[0], "paddingLeft"));
          }(this.$el, this.margin) * (r.length - 1);
        }return { rows: r, translates: n, height: !e && i };
      }, write: function write(t) {
        var e = t.stacks,
            n = t.height;Oe(this.$el, this.clsStack, e), Pe(this.$el, "paddingBottom", this.parallax), !1 !== n && Pe(this.$el, "height", n);
      }, events: ["resize"] }, { read: function read(t) {
        var e = t.height;return { scrolled: !!this.parallax && pn(this.$el, e ? e - on(this.$el) : 0) * this.parallax };
      }, write: function write(t) {
        var e = t.rows,
            i = t.scrolled,
            r = t.translates;!1 === i && !r || e.forEach(function (t, n) {
          return t.forEach(function (t, e) {
            return Pe(t, "transform", i || r ? "translateY(" + ((r && -r[n][e]) + (i ? e % 2 ? i : i / 8 : 0)) + "px)" : "");
          });
        });
      }, events: ["scroll", "resize"] }] };var vi = Kt ? { data: { selMinHeight: !1, forceHeight: !1 }, computed: { elements: function elements(t, e) {
        var n = t.selMinHeight;return n ? Ae(n, e) : [e];
      } }, update: [{ read: function read() {
        Pe(this.elements, "height", "");
      }, order: -5, events: ["resize"] }, { write: function write() {
        var n = this;this.elements.forEach(function (t) {
          var e = F(Pe(t, "minHeight"));e && (n.forceHeight || Math.round(e + un("height", t, "content-box")) >= t.offsetHeight) && Pe(t, "height", e);
        });
      }, order: 5, events: ["resize"] }] } : {},
      gi = { mixins: [vi], args: "target", props: { target: String, row: Boolean }, data: { target: "> *", row: !0, forceHeight: !0 }, computed: { elements: function elements(t, e) {
        return Ae(t.target, e);
      } }, update: { read: function read() {
        return { rows: (this.row ? fi(this.elements) : [this.elements]).map(wi) };
      }, write: function write(t) {
        t.rows.forEach(function (t) {
          var n = t.heights;return t.elements.forEach(function (t, e) {
            return Pe(t, "minHeight", n[e]);
          });
        });
      }, events: ["resize"] } };function wi(t) {
    var e;if (t.length < 2) return { heights: [""], elements: t };var n = bi(t),
        i = n.heights,
        r = n.max,
        o = t.some(function (t) {
      return t.style.minHeight;
    }),
        s = t.some(function (t, e) {
      return !t.style.minHeight && i[e] < r;
    });return o && s && (Pe(t, "minHeight", ""), e = bi(t), i = e.heights, r = e.max), { heights: i = t.map(function (t, e) {
        return i[e] === r && F(t.style.minHeight).toFixed(2) !== r.toFixed(2) ? "" : r;
      }), elements: t };
  }function bi(t) {
    var e = t.map(function (t) {
      return en(t).height - un("height", t, "content-box");
    });return { heights: e, max: Math.max.apply(null, e) };
  }var xi = { mixins: [vi], props: { expand: Boolean, offsetTop: Boolean, offsetBottom: Boolean, minHeight: Number }, data: { expand: !1, offsetTop: !1, offsetBottom: !1, minHeight: 0 }, update: { read: function read() {
        var t = "",
            e = un("height", this.$el, "content-box");if (this.expand) t = on(window) - (yi(document.documentElement) - yi(this.$el)) - e || "";else {
          if (t = "calc(100vh", this.offsetTop) {
            var n = en(this.$el).top;t += n < on(window) / 2 ? " - " + n + "px" : "";
          }!0 === this.offsetBottom ? t += " - " + yi(this.$el.nextElementSibling) + "px" : D(this.offsetBottom) ? t += " - " + this.offsetBottom + "vh" : this.offsetBottom && h(this.offsetBottom, "px") ? t += " - " + F(this.offsetBottom) + "px" : B(this.offsetBottom) && (t += " - " + yi(at(this.offsetBottom, this.$el)) + "px"), t += (e ? " - " + e + "px" : "") + ")";
        }return { minHeight: t };
      }, write: function write(t) {
        var e = t.minHeight;Pe(this.$el, { minHeight: e }), this.minHeight && F(Pe(this.$el, "minHeight")) < this.minHeight && Pe(this.$el, "minHeight", this.minHeight);
      }, events: ["resize"] } };function yi(t) {
    return t && t.offsetHeight || 0;
  }var ki = { args: "src", props: { id: Boolean, icon: String, src: String, style: String, width: Number, height: Number, ratio: Number, class: String, strokeAnimation: Boolean, attributes: "list" }, data: { ratio: 1, include: ["style", "class"], class: "", strokeAnimation: !1 }, connected: function connected() {
      var t,
          e = this;if (this.class += " uk-svg", !this.icon && x(this.src, "#")) {
        var n = this.src.split("#");1 < n.length && (t = n, this.src = t[0], this.icon = t[1]);
      }this.svg = this.getSvg().then(function (t) {
        return e.applyAttributes(t), e.svgEl = function (t, e) {
          {
            if (At(e) || "CANVAS" === e.tagName) {
              it(e, "hidden", !0);var n = e.nextElementSibling;return Si(t, n) ? n : me(e, t);
            }var i = e.lastElementChild;return Si(t, i) ? i : fe(e, t);
          }
        }(t, e.$el);
      }, Q);
    }, disconnected: function disconnected() {
      var e = this;At(this.$el) && it(this.$el, "hidden", null), this.svg && this.svg.then(function (t) {
        return (!e._connected || t !== e.svgEl) && ge(t);
      }, Q), this.svg = this.svgEl = null;
    }, update: { read: function read() {
        return !!(this.strokeAnimation && this.svgEl && St(this.svgEl));
      }, write: function write() {
        !function (t) {
          var e = Ai(t);e && t.style.setProperty("--uk-animation-stroke", e);
        }(this.svgEl);
      }, type: ["resize"] }, methods: { getSvg: function getSvg() {
        var e = this;return function (n) {
          if ($i[n]) return $i[n];return $i[n] = new qt(function (e, t) {
            n ? w(n, "data:") ? e(decodeURIComponent(n.split(",")[1])) : Gt(n).then(function (t) {
              return e(t.response);
            }, function () {
              return t("SVG not found.");
            }) : t();
          });
        }(this.src).then(function (t) {
          return function (t, e) {
            e && x(t, "<symbol") && (t = function (t, e) {
              if (!Ti[t]) {
                var n;for (Ti[t] = {}; n = Ii.exec(t);) {
                  Ti[t][n[3]] = '<svg xmlns="http://www.w3.org/2000/svg"' + n[1] + "svg>";
                }Ii.lastIndex = 0;
              }return Ti[t][e];
            }(t, e) || t);return (t = Te(t.substr(t.indexOf("<svg")))) && t.hasChildNodes() && t;
          }(t, e.icon) || qt.reject("SVG not found.");
        });
      }, applyAttributes: function applyAttributes(n) {
        var i = this;for (var t in this.$options.props) {
          this[t] && x(this.include, t) && it(n, t, this[t]);
        }for (var e in this.attributes) {
          var r = this.attributes[e].split(":", 2),
              o = r[0],
              s = r[1];it(n, o, s);
        }this.id || ot(n, "id");var a = ["width", "height"],
            u = [this.width, this.height];u.some(function (t) {
          return t;
        }) || (u = a.map(function (t) {
          return it(n, t);
        }));var c = it(n, "viewBox");c && !u.some(function (t) {
          return t;
        }) && (u = c.split(" ").slice(2)), u.forEach(function (t, e) {
          (t = (0 | t) * i.ratio) && it(n, a[e], t), t && !u[1 ^ e] && ot(n, a[1 ^ e]);
        }), it(n, "data-svg", this.icon || this.src);
      } } },
      $i = {};var Ii = /<symbol(.*?id=(['"])(.*?)\2[^]*?<\/)symbol>/g,
      Ti = {};function Ai(t) {
    return Math.ceil(Math.max.apply(Math, Ae("[stroke]", t).map(function (t) {
      return t.getTotalLength && t.getTotalLength() || 0;
    }).concat([0])));
  }function Si(t, e) {
    return it(t, "data-svg") === it(e, "data-svg");
  }var Ei = {},
      Ci = { spinner: '<svg width="30" height="30" viewBox="0 0 30 30" xmlns="http://www.w3.org/2000/svg"><circle fill="none" stroke="#000" cx="15" cy="15" r="14"/></svg>', totop: '<svg width="18" height="10" viewBox="0 0 18 10" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#000" stroke-width="1.2" points="1 9 9 1 17 9 "/></svg>', marker: '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><rect x="9" y="4" width="1" height="11"/><rect x="4" y="9" width="11" height="1"/></svg>', "close-icon": '<svg width="14" height="14" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg"><line fill="none" stroke="#000" stroke-width="1.1" x1="1" y1="1" x2="13" y2="13"/><line fill="none" stroke="#000" stroke-width="1.1" x1="13" y1="1" x2="1" y2="13"/></svg>', "close-large": '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><line fill="none" stroke="#000" stroke-width="1.4" x1="1" y1="1" x2="19" y2="19"/><line fill="none" stroke="#000" stroke-width="1.4" x1="19" y1="1" x2="1" y2="19"/></svg>', "navbar-toggle-icon": '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><rect y="9" width="20" height="2"/><rect y="3" width="20" height="2"/><rect y="15" width="20" height="2"/></svg>', "overlay-icon": '<svg width="40" height="40" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg"><rect x="19" y="0" width="1" height="40"/><rect x="0" y="19" width="40" height="1"/></svg>', "pagination-next": '<svg width="7" height="12" viewBox="0 0 7 12" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#000" stroke-width="1.2" points="1 1 6 6 1 11"/></svg>', "pagination-previous": '<svg width="7" height="12" viewBox="0 0 7 12" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#000" stroke-width="1.2" points="6 1 1 6 6 11"/></svg>', "search-icon": '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><circle fill="none" stroke="#000" stroke-width="1.1" cx="9" cy="9" r="7"/><path fill="none" stroke="#000" stroke-width="1.1" d="M14,14 L18,18 L14,14 Z"/></svg>', "search-large": '<svg width="40" height="40" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg"><circle fill="none" stroke="#000" stroke-width="1.8" cx="17.5" cy="17.5" r="16.5"/><line fill="none" stroke="#000" stroke-width="1.8" x1="38" y1="39" x2="29" y2="30"/></svg>', "search-navbar": '<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><circle fill="none" stroke="#000" stroke-width="1.1" cx="10.5" cy="10.5" r="9.5"/><line fill="none" stroke="#000" stroke-width="1.1" x1="23" y1="23" x2="17" y2="17"/></svg>', "slidenav-next": '<svg width="14px" height="24px" viewBox="0 0 14 24" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#000" stroke-width="1.4" points="1.225,23 12.775,12 1.225,1 "/></svg>', "slidenav-next-large": '<svg width="25px" height="40px" viewBox="0 0 25 40" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#000" stroke-width="2" points="4.002,38.547 22.527,20.024 4,1.5 "/></svg>', "slidenav-previous": '<svg width="14px" height="24px" viewBox="0 0 14 24" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#000" stroke-width="1.4" points="12.775,1 1.225,12 12.775,23 "/></svg>', "slidenav-previous-large": '<svg width="25px" height="40px" viewBox="0 0 25 40" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#000" stroke-width="2" points="20.527,1.5 2,20.024 20.525,38.547 "/></svg>' },
      _i = { install: function install(r) {
      r.icon.add = function (t, e) {
        var n,
            i = B(t) ? ((n = {})[t] = e, n) : t;G(i, function (t, e) {
          Ci[e] = t, delete Ei[e];
        }), r._initialized && Ie(document.body, function (t) {
          return G(r.getComponents(t), function (t) {
            t.$options.isIcon && t.icon in i && t.$reset();
          });
        });
      };
    }, mixins: [Qn, ki], args: "icon", props: ["icon"], data: { include: [] }, isIcon: !0, connected: function connected() {
      Ee(this.$el, "uk-icon");
    }, methods: { getSvg: function getSvg() {
        var t = function (t) {
          if (!Ci[t]) return null;Ei[t] || (Ei[t] = Te(Ci[t].trim()));return Ei[t].cloneNode(!0);
        }(function (t) {
          return Zt ? U(U(t, "left", "right"), "previous", "next") : t;
        }(this.icon));return t ? qt.resolve(t) : qt.reject("Icon not found.");
      } } },
      Ni = { extends: _i, data: function data(t) {
      return { icon: d(t.constructor.options.name) };
    } },
      Mi = { extends: Ni, connected: function connected() {
      Ee(this.$el, "uk-slidenav");
    }, computed: { icon: function icon(t, e) {
        var n = t.icon;return Me(e, "uk-slidenav-large") ? n + "-large" : n;
      } } },
      Oi = { extends: Ni, computed: { icon: function icon(t, e) {
        var n = t.icon;return Me(e, "uk-search-icon") && kt(e, ".uk-search-large").length ? "search-large" : kt(e, ".uk-search-navbar").length ? "search-navbar" : n;
      } } },
      Bi = { extends: Ni, computed: { icon: function icon() {
        return "close-" + (Me(this.$el, "uk-close-large") ? "large" : "icon");
      } } },
      zi = { extends: Ni, connected: function connected() {
      var e = this;this.svg.then(function (t) {
        return 1 !== e.ratio && Pe(Te("circle", t), "strokeWidth", 1 / e.ratio);
      }, Q);
    } };var Di = { args: "dataSrc", props: { dataSrc: String, dataSrcset: Boolean, sizes: String, width: Number, height: Number, offsetTop: String, offsetLeft: String, target: String }, data: { dataSrc: "", dataSrcset: !1, sizes: !1, width: !1, height: !1, offsetTop: "50vh", offsetLeft: 0, target: !1 }, computed: { cacheKey: function cacheKey(t) {
        var e = t.dataSrc;return this.$name + "." + e;
      }, width: function width(t) {
        var e = t.width,
            n = t.dataWidth;return e || n;
      }, height: function height(t) {
        var e = t.height,
            n = t.dataHeight;return e || n;
      }, sizes: function sizes(t) {
        var e = t.sizes,
            n = t.dataSizes;return e || n;
      }, isImg: function isImg(t, e) {
        return Vi(e);
      }, target: { get: function get(t) {
          var e = t.target;return [this.$el].concat(ut(e, this.$el));
        }, watch: function watch() {
          this.observe();
        } }, offsetTop: function offsetTop(t) {
        return gn(t.offsetTop, "height");
      }, offsetLeft: function offsetLeft(t) {
        return gn(t.offsetLeft, "width");
      } }, connected: function connected() {
      Ri[this.cacheKey] ? Hi(this.$el, Ri[this.cacheKey] || this.dataSrc, this.dataSrcset, this.sizes) : this.isImg && this.width && this.height && Hi(this.$el, function (t, e, n) {
        var i;n && (i = nt.ratio({ width: t, height: e }, "width", gn(Li(n))), t = i.width, e = i.height);return 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="' + t + '" height="' + e + '"></svg>';
      }(this.width, this.height, this.sizes)), this.observer = new zn(this.load, { rootMargin: this.offsetTop + "px " + this.offsetLeft + "px" }), requestAnimationFrame(this.observe);
    }, disconnected: function disconnected() {
      this.observer.disconnect();
    }, update: { read: function read(t) {
        var e = this,
            n = t.image;if (n || "complete" !== document.readyState || this.load(this.observer.takeRecords()), this.isImg) return !1;n && n.then(function (t) {
          return t && "" !== t.currentSrc && Hi(e.$el, qi(t));
        });
      }, write: function write(t) {
        if (this.dataSrcset && 1 !== window.devicePixelRatio) {
          var e = Pe(this.$el, "backgroundSize");!e.match(/^(auto\s?)+$/) && F(e) !== t.bgSize || (t.bgSize = function (t, e) {
            var n = gn(Li(e)),
                i = (t.match(Wi) || []).map(F).sort(function (t, e) {
              return t - e;
            });return i.filter(function (t) {
              return n <= t;
            })[0] || i.pop() || "";
          }(this.dataSrcset, this.sizes), Pe(this.$el, "backgroundSize", t.bgSize + "px"));
        }
      }, events: ["resize"] }, methods: { load: function load(t) {
        var e = this;t.some(function (t) {
          return t.isIntersecting;
        }) && (this._data.image = Jt(this.dataSrc, this.dataSrcset, this.sizes).then(function (t) {
          return Hi(e.$el, qi(t), t.srcset, t.sizes), Ri[e.cacheKey] = qi(t), t;
        }, Q), this.observer.disconnect());
      }, observe: function observe() {
        var e = this;!this._data.image && this._connected && this.target.forEach(function (t) {
          return e.observer.observe(t);
        });
      } } };function Hi(t, e, n, i) {
    if (Vi(t)) i && (t.sizes = i), n && (t.srcset = n), e && (t.src = e);else if (e) {
      !x(t.style.backgroundImage, e) && (Pe(t, "backgroundImage", "url(" + It(e) + ")"), zt(t, Dt("load", !1)));
    }
  }var Pi = /\s*(.*?)\s*(\w+|calc\(.*?\))\s*(?:,|$)/g;function Li(t) {
    var e, n;for (Pi.lastIndex = 0; e = Pi.exec(t);) {
      if (!e[1] || window.matchMedia(e[1]).matches) {
        e = w(n = e[2], "calc") ? n.substring(5, n.length - 1).replace(ji, function (t) {
          return gn(t);
        }).replace(/ /g, "").match(Fi).reduce(function (t, e) {
          return t + +e;
        }, 0) : n;break;
      }
    }return e || "100vw";
  }var ji = /\d+(?:\w+|%)/g,
      Fi = /[+-]?(\d+)/g;var Wi = /\s+\d+w\s*(?:,|$)/g;function Vi(t) {
    return "IMG" === t.tagName;
  }function qi(t) {
    return t.currentSrc || t.src;
  }var Ri,
      Yi = "__test__";try {
    (Ri = window.sessionStorage || {})[Yi] = 1, delete Ri[Yi];
  } catch (t) {
    Ri = {};
  }var Ui = { props: { media: Boolean }, data: { media: !1 }, computed: { matchMedia: function matchMedia() {
        var t = function (t) {
          if (B(t)) if ("@" === t[0]) {
            var e = "breakpoint-" + t.substr(1);t = F(We(e));
          } else if (isNaN(t)) return t;return !(!t || isNaN(t)) && "(min-width: " + t + "px)";
        }(this.media);return !t || window.matchMedia(t).matches;
      } } };var Xi,
      Gi,
      Ji = { mixins: [Qn, Ui], props: { fill: String }, data: { fill: "", clsWrapper: "uk-leader-fill", clsHide: "uk-leader-hide", attrFill: "data-fill" }, computed: { fill: function fill(t) {
        return t.fill || We("leader-fill-content");
      } }, connected: function connected() {
      var t;t = be(this.$el, '<span class="' + this.clsWrapper + '">'), this.wrapper = t[0];
    }, disconnected: function disconnected() {
      xe(this.wrapper.childNodes);
    }, update: { read: function read(t) {
        var e = t.changed,
            n = t.width,
            i = n;return { width: n = Math.floor(this.$el.offsetWidth / 2), fill: this.fill, changed: e || i !== n, hide: !this.matchMedia };
      }, write: function write(t) {
        Oe(this.wrapper, this.clsHide, t.hide), t.changed && (t.changed = !1, it(this.wrapper, this.attrFill, new Array(t.width).join(t.fill)));
      }, events: ["resize"] } },
      Ki = { props: { container: Boolean }, data: { container: !0 }, computed: { container: function container(t) {
        var e = t.container;return !0 === e && this.$container || e && Te(e);
      } } },
      Zi = { mixins: [Qn, Ki, ti], props: { selPanel: String, selClose: String, escClose: Boolean, bgClose: Boolean, stack: Boolean }, data: { cls: "uk-open", escClose: !0, bgClose: !0, overlay: !0, stack: !1 }, computed: { panel: function panel(t, e) {
        return Te(t.selPanel, e);
      }, transitionElement: function transitionElement() {
        return this.panel;
      }, bgClose: function bgClose(t) {
        return t.bgClose && this.panel;
      } }, beforeDisconnect: function beforeDisconnect() {
      this.isToggled() && this.toggleNow(this.$el, !1);
    }, events: [{ name: "click", delegate: function delegate() {
        return this.selClose;
      }, handler: function handler(t) {
        t.preventDefault(), this.hide();
      } }, { name: "toggle", self: !0, handler: function handler(t) {
        t.defaultPrevented || (t.preventDefault(), this.toggle());
      } }, { name: "beforeshow", self: !0, handler: function handler(t) {
        var n = Xi && Xi !== this && Xi;Xi = this, n ? this.stack ? this.prev = n : ((Xi = n).isToggled() ? n.hide().then(this.show) : Bt(n.$el, "beforeshow hidden", this.show, !1, function (t) {
          var e = t.target;return "hidden" === t.type && e === n.$el;
        }), t.preventDefault()) : function () {
          if (Gi) return;Gi = [Mt(document, re, function (t) {
            var e = t.target,
                n = t.defaultPrevented;!Xi || !Xi.bgClose || n || Xi.overlay && !Nt(e, Xi.$el) || Nt(e, Xi.panel) || Xi.hide();
          }), Mt(document, "keydown", function (t) {
            27 === t.keyCode && Xi && Xi.escClose && (t.preventDefault(), Xi.hide());
          })];
        }();
      } }, { name: "show", self: !0, handler: function handler() {
        Me(document.documentElement, this.clsPage) || (this.scrollbarWidth = sn(window) - sn(document), Pe(document.body, "overflowY", this.scrollbarWidth && this.overlay ? "scroll" : "")), Ee(document.documentElement, this.clsPage);
      } }, { name: "hide", self: !0, handler: function handler() {
        Xi && (Xi !== this || this.prev) || (Gi && Gi.forEach(function (t) {
          return t();
        }), Gi = null);
      } }, { name: "hidden", self: !0, handler: function handler() {
        var t,
            e = this.prev;if (Xi = Xi && Xi !== this && Xi || e) for (; e;) {
          if (e.clsPage === this.clsPage) {
            t = !0;break;
          }e = e.prev;
        } else Pe(document.body, "overflowY", "");t || Ce(document.documentElement, this.clsPage);
      } }], methods: { toggle: function toggle() {
        return this.isToggled() ? this.hide() : this.show();
      }, show: function show() {
        var e = this;return this.isToggled() ? qt.resolve() : this.container && this.$el.parentNode !== this.container ? (fe(this.container, this.$el), new qt(function (t) {
          return requestAnimationFrame(function () {
            return e.show().then(t);
          });
        })) : this.toggleElement(this.$el, !0, Qi(this));
      }, hide: function hide() {
        return this.isToggled() ? this.toggleElement(this.$el, !1, Qi(this)) : qt.resolve();
      }, getActive: function getActive() {
        return Xi;
      } } };function Qi(t) {
    var r = t.transitionElement,
        o = t._toggle;return function (n, i) {
      return new qt(function (t, e) {
        return Bt(n, "show hide", function () {
          n._reject && n._reject(), n._reject = e, o(n, i), R(Pe(r, "transitionDuration")) ? Bt(r, "transitionend", t, !1, function (t) {
            return t.target === r;
          }) : t();
        });
      });
    };
  }var tr = { install: function install(a) {
      a.modal.dialog = function (t, e) {
        var i = a.modal(' <div class="uk-modal"> <div class="uk-modal-dialog">' + t + "</div> </div> ", e);return i.show(), Mt(i.$el, "hidden", function (t) {
          var e = t.target,
              n = t.currentTarget;e === n && qt.resolve(function () {
            return i.$destroy(!0);
          });
        }), i;
      }, a.modal.alert = function (e, n) {
        return n = X({ bgClose: !1, escClose: !1, labels: a.modal.labels }, n), new qt(function (t) {
          return Mt(a.modal.dialog(' <div class="uk-modal-body">' + (B(e) ? e : de(e)) + '</div> <div class="uk-modal-footer uk-text-right"> <button class="uk-button uk-button-primary uk-modal-close" autofocus>' + n.labels.ok + "</button> </div> ", n).$el, "hide", t);
        });
      }, a.modal.confirm = function (r, o) {
        return o = X({ bgClose: !1, escClose: !0, labels: a.modal.labels }, o), new qt(function (e, t) {
          var n = a.modal.dialog(' <form> <div class="uk-modal-body">' + (B(r) ? r : de(r)) + '</div> <div class="uk-modal-footer uk-text-right"> <button class="uk-button uk-button-default uk-modal-close" type="button">' + o.labels.cancel + '</button> <button class="uk-button uk-button-primary" autofocus>' + o.labels.ok + "</button> </div> </form> ", o),
              i = !1;Mt(n.$el, "submit", "form", function (t) {
            t.preventDefault(), e(), i = !0, n.hide();
          }), Mt(n.$el, "hide", function () {
            i || t();
          });
        });
      }, a.modal.prompt = function (t, o, s) {
        return s = X({ bgClose: !1, escClose: !0, labels: a.modal.labels }, s), new qt(function (e) {
          var n = a.modal.dialog(' <form class="uk-form-stacked"> <div class="uk-modal-body"> <label>' + (B(t) ? t : de(t)) + '</label> <input class="uk-input" autofocus> </div> <div class="uk-modal-footer uk-text-right"> <button class="uk-button uk-button-default uk-modal-close" type="button">' + s.labels.cancel + '</button> <button class="uk-button uk-button-primary">' + s.labels.ok + "</button> </div> </form> ", s),
              i = Te("input", n.$el);i.value = o;var r = !1;Mt(n.$el, "submit", "form", function (t) {
            t.preventDefault(), e(i.value), r = !0, n.hide();
          }), Mt(n.$el, "hide", function () {
            r || e(null);
          });
        });
      }, a.modal.labels = { ok: "Ok", cancel: "Cancel" };
    }, mixins: [Zi], data: { clsPage: "uk-modal-page", selPanel: ".uk-modal-dialog", selClose: ".uk-modal-close, .uk-modal-close-default, .uk-modal-close-outside, .uk-modal-close-full" }, events: [{ name: "show", self: !0, handler: function handler() {
        Me(this.panel, "uk-margin-auto-vertical") ? Ee(this.$el, "uk-flex") : Pe(this.$el, "display", "block"), on(this.$el);
      } }, { name: "hidden", self: !0, handler: function handler() {
        Pe(this.$el, "display", ""), Ce(this.$el, "uk-flex");
      } }] };var er = { extends: ei, data: { targets: "> .uk-parent", toggle: "> a", content: "> ul" } },
      nr = { mixins: [Qn, vi], props: { dropdown: String, mode: "list", align: String, offset: Number, boundary: Boolean, boundaryAlign: Boolean, clsDrop: String, delayShow: Number, delayHide: Number, dropbar: Boolean, dropbarMode: String, dropbarAnchor: Boolean, duration: Number }, data: { dropdown: ".uk-navbar-nav > li", align: Zt ? "right" : "left", clsDrop: "uk-navbar-dropdown", mode: void 0, offset: void 0, delayShow: void 0, delayHide: void 0, boundaryAlign: void 0, flip: "x", boundary: !0, dropbar: !1, dropbarMode: "slide", dropbarAnchor: !1, duration: 200, forceHeight: !0, selMinHeight: ".uk-navbar-nav > li > a, .uk-navbar-item, .uk-navbar-toggle" }, computed: { boundary: function boundary(t, e) {
        var n = t.boundary,
            i = t.boundaryAlign;return !0 === n || i ? e : n;
      }, dropbarAnchor: function dropbarAnchor(t, e) {
        return at(t.dropbarAnchor, e);
      }, pos: function pos(t) {
        return "bottom-" + t.align;
      }, dropdowns: function dropdowns(t, e) {
        return Ae(t.dropdown + " ." + t.clsDrop, e);
      } }, beforeConnect: function beforeConnect() {
      var t = this.$props.dropbar;this.dropbar = t && (at(t, this.$el) || Te("+ .uk-navbar-dropbar", this.$el) || Te("<div></div>")), this.dropbar && (Ee(this.dropbar, "uk-navbar-dropbar"), "slide" === this.dropbarMode && Ee(this.dropbar, "uk-navbar-dropbar-slide"));
    }, disconnected: function disconnected() {
      this.dropbar && ge(this.dropbar);
    }, update: function update() {
      var e = this;this.$create("drop", this.dropdowns.filter(function (t) {
        return !e.getDropdown(t);
      }), X({}, this.$props, { boundary: this.boundary, pos: this.pos, offset: this.dropbar || this.offset }));
    }, events: [{ name: "mouseover", delegate: function delegate() {
        return this.dropdown;
      }, handler: function handler(t) {
        var e = t.current,
            n = this.getActive();n && n.toggle && !Nt(n.toggle.$el, e) && !n.tracker.movesTo(n.$el) && n.hide(!1);
      } }, { name: "mouseleave", el: function el() {
        return this.dropbar;
      }, handler: function handler() {
        var t = this.getActive();t && !this.dropdowns.some(function (t) {
          return bt(t, ":hover");
        }) && t.hide();
      } }, { name: "beforeshow", capture: !0, filter: function filter() {
        return this.dropbar;
      }, handler: function handler() {
        this.dropbar.parentNode || me(this.dropbarAnchor || this.$el, this.dropbar);
      } }, { name: "show", capture: !0, filter: function filter() {
        return this.dropbar;
      }, handler: function handler(t, e) {
        var n = e.$el,
            i = e.dir;this.clsDrop && Ee(n, this.clsDrop + "-dropbar"), "bottom" === i && this.transitionTo(n.offsetHeight + F(Pe(n, "marginTop")) + F(Pe(n, "marginBottom")), n);
      } }, { name: "beforehide", filter: function filter() {
        return this.dropbar;
      }, handler: function handler(t, e) {
        var n = e.$el,
            i = this.getActive();bt(this.dropbar, ":hover") && i && i.$el === n && t.preventDefault();
      } }, { name: "hide", filter: function filter() {
        return this.dropbar;
      }, handler: function handler(t, e) {
        var n = e.$el,
            i = this.getActive();(!i || i && i.$el === n) && this.transitionTo(0);
      } }], methods: { getActive: function getActive() {
        var t = this.dropdowns.map(this.getDropdown).filter(function (t) {
          return t && t.isActive();
        })[0];return t && x(t.mode, "hover") && Nt(t.toggle.$el, this.$el) && t;
      }, transitionTo: function transitionTo(t, e) {
        var n = this,
            i = this.dropbar,
            r = St(i) ? on(i) : 0;return Pe(e = r < t && e, "clip", "rect(0," + e.offsetWidth + "px," + r + "px,0)"), on(i, r), Ue.cancel([e, i]), qt.all([Ue.start(i, { height: t }, this.duration), Ue.start(e, { clip: "rect(0," + e.offsetWidth + "px," + t + "px,0)" }, this.duration)]).catch(Q).then(function () {
          Pe(e, { clip: "" }), n.$update(i);
        });
      }, getDropdown: function getDropdown(t) {
        return this.$getComponent(t, "drop") || this.$getComponent(t, "dropdown");
      } } },
      ir = { mixins: [Zi], args: "mode", props: { mode: String, flip: Boolean, overlay: Boolean }, data: { mode: "slide", flip: !1, overlay: !1, clsPage: "uk-offcanvas-page", clsContainer: "uk-offcanvas-container", selPanel: ".uk-offcanvas-bar", clsFlip: "uk-offcanvas-flip", clsContainerAnimation: "uk-offcanvas-container-animation", clsSidebarAnimation: "uk-offcanvas-bar-animation", clsMode: "uk-offcanvas", clsOverlay: "uk-offcanvas-overlay", selClose: ".uk-offcanvas-close" }, computed: { clsFlip: function clsFlip(t) {
        var e = t.flip,
            n = t.clsFlip;return e ? n : "";
      }, clsOverlay: function clsOverlay(t) {
        var e = t.overlay,
            n = t.clsOverlay;return e ? n : "";
      }, clsMode: function clsMode(t) {
        var e = t.mode;return t.clsMode + "-" + e;
      }, clsSidebarAnimation: function clsSidebarAnimation(t) {
        var e = t.mode,
            n = t.clsSidebarAnimation;return "none" === e || "reveal" === e ? "" : n;
      }, clsContainerAnimation: function clsContainerAnimation(t) {
        var e = t.mode,
            n = t.clsContainerAnimation;return "push" !== e && "reveal" !== e ? "" : n;
      }, transitionElement: function transitionElement(t) {
        return "reveal" === t.mode ? this.panel.parentNode : this.panel;
      } }, events: [{ name: "click", delegate: function delegate() {
        return 'a[href^="#"]';
      }, handler: function handler(t) {
        var e = t.current;e.hash && Te(e.hash, document.body) && this.hide();
      } }, { name: "touchstart", passive: !0, el: function el() {
        return this.panel;
      }, handler: function handler(t) {
        var e = t.targetTouches;1 === e.length && (this.clientY = e[0].clientY);
      } }, { name: "touchmove", self: !0, passive: !1, filter: function filter() {
        return this.overlay;
      }, handler: function handler(t) {
        t.preventDefault();
      } }, { name: "touchmove", passive: !1, el: function el() {
        return this.panel;
      }, handler: function handler(t) {
        if (1 === t.targetTouches.length) {
          var e = event.targetTouches[0].clientY - this.clientY,
              n = this.panel,
              i = n.scrollTop,
              r = n.scrollHeight,
              o = n.clientHeight;(r <= o || 0 === i && 0 < e || r - i <= o && e < 0) && t.preventDefault();
        }
      } }, { name: "show", self: !0, handler: function handler() {
        "reveal" !== this.mode || Me(this.panel.parentNode, this.clsMode) || (we(this.panel, "<div>"), Ee(this.panel.parentNode, this.clsMode)), Pe(document.documentElement, "overflowY", this.overlay ? "hidden" : ""), Ee(document.body, this.clsContainer, this.clsFlip), Pe(this.$el, "display", "block"), Ee(this.$el, this.clsOverlay), Ee(this.panel, this.clsSidebarAnimation, "reveal" !== this.mode ? this.clsMode : ""), on(document.body), Ee(document.body, this.clsContainerAnimation), this.clsContainerAnimation && (rr().content += ",user-scalable=0");
      } }, { name: "hide", self: !0, handler: function handler() {
        Ce(document.body, this.clsContainerAnimation);var t = this.getActive();("none" === this.mode || t && t !== this && t !== this.prev) && zt(this.panel, "transitionend");
      } }, { name: "hidden", self: !0, handler: function handler() {
        this.clsContainerAnimation && function () {
          var t = rr();t.content = t.content.replace(/,user-scalable=0$/, "");
        }(), "reveal" === this.mode && xe(this.panel), Ce(this.panel, this.clsSidebarAnimation, this.clsMode), Ce(this.$el, this.clsOverlay), Pe(this.$el, "display", ""), Ce(document.body, this.clsContainer, this.clsFlip), Pe(document.documentElement, "overflowY", "");
      } }, { name: "swipeLeft swipeRight", handler: function handler(t) {
        this.isToggled() && h(t.type, "Left") ^ this.flip && this.hide();
      } }] };function rr() {
    return Te('meta[name="viewport"]', document.head) || fe(document.head, '<meta name="viewport">');
  }var or = { mixins: [Qn], props: { selContainer: String, selContent: String }, data: { selContainer: ".uk-modal", selContent: ".uk-modal-dialog" }, computed: { container: function container(t, e) {
        return yt(e, t.selContainer);
      }, content: function content(t, e) {
        return yt(e, t.selContent);
      } }, connected: function connected() {
      Pe(this.$el, "minHeight", 150);
    }, update: { read: function read() {
        return !(!this.content || !this.container) && { current: F(Pe(this.$el, "maxHeight")), max: Math.max(150, on(this.container) - (en(this.content).height - on(this.$el))) };
      }, write: function write(t) {
        var e = t.current,
            n = t.max;Pe(this.$el, "maxHeight", n), Math.round(e) !== Math.round(n) && zt(this.$el, "resize");
      }, events: ["resize"] } },
      sr = { props: ["width", "height"], connected: function connected() {
      Ee(this.$el, "uk-responsive-width");
    }, update: { read: function read() {
        return !!(St(this.$el) && this.width && this.height) && { width: sn(this.$el.parentNode), height: this.height };
      }, write: function write(t) {
        on(this.$el, nt.contain({ height: this.height, width: this.width }, t).height);
      }, events: ["resize"] } },
      ar = { props: { duration: Number, offset: Number }, data: { duration: 1e3, offset: 0 }, methods: { scrollTo: function scrollTo(e) {
        var n = this;e = e && Te(e) || document.body;var t = on(document),
            i = on(window),
            r = en(e).top - this.offset;if (t < r + i && (r = t - i), zt(this.$el, "beforescroll", [this, e])) {
          var o = Date.now(),
              s = window.pageYOffset,
              a = function a() {
            var t = s + (r - s) * function (t) {
              return .5 * (1 - Math.cos(Math.PI * t));
            }(Z((Date.now() - o) / n.duration));mn(window, t), t !== r ? requestAnimationFrame(a) : zt(n.$el, "scrolled", [n, e]);
          };a();
        }
      } }, events: { click: function click(t) {
        t.defaultPrevented || (t.preventDefault(), this.scrollTo(It(decodeURIComponent(this.$el.hash)).substr(1)));
      } } };var ur = { args: "cls", props: { cls: String, target: String, hidden: Boolean, offsetTop: Number, offsetLeft: Number, repeat: Boolean, delay: Number }, data: function data() {
      return { cls: !1, target: !1, hidden: !0, offsetTop: 0, offsetLeft: 0, repeat: !1, delay: 0, inViewClass: "uk-scrollspy-inview" };
    }, computed: { elements: function elements(t, e) {
        var n = t.target;return n ? Ae(n, e) : [e];
      } }, update: [{ write: function write() {
        this.hidden && Pe(_t(this.elements, ":not(." + this.inViewClass + ")"), "visibility", "hidden");
      } }, { read: function read(t) {
        var n = this;t.update && this.elements.forEach(function (t) {
          var e = t._ukScrollspyState;e || (e = { cls: st(t, "uk-scrollspy-class") || n.cls }), e.show = fn(t, n.offsetTop, n.offsetLeft), t._ukScrollspyState = e;
        });
      }, write: function write(r) {
        var o = this;if (!r.update) return this.$emit(), r.update = !0;this.elements.forEach(function (t) {
          var n = t._ukScrollspyState,
              e = n.cls;if (!n.show || n.inview || n.queued) {
            if (!n.show && (n.inview || n.queued) && o.repeat) {
              if (n.abort && n.abort(), !n.inview) return;Pe(t, "visibility", o.hidden ? "hidden" : ""), Ce(t, o.inViewClass), Oe(t, e), zt(t, "outview"), o.$update(t), n.inview = !1;
            }
          } else {
            var i = function i() {
              Pe(t, "visibility", ""), Ee(t, o.inViewClass), Oe(t, e), zt(t, "inview"), o.$update(t), n.inview = !0, n.abort && n.abort();
            };o.delay ? (n.queued = !0, r.promise = (r.promise || qt.resolve()).then(function () {
              return !n.inview && new qt(function (t) {
                var e = setTimeout(function () {
                  i(), t();
                }, r.promise || 1 === o.elements.length ? o.delay : 0);n.abort = function () {
                  clearTimeout(e), t(), n.queued = !1;
                };
              });
            })) : i();
          }
        });
      }, events: ["scroll", "resize"] }] },
      cr = { props: { cls: String, closest: String, scroll: Boolean, overflow: Boolean, offset: Number }, data: { cls: "uk-active", closest: !1, scroll: !1, overflow: !0, offset: 0 }, computed: { links: function links(t, e) {
        return Ae('a[href^="#"]', e).filter(function (t) {
          return t.hash;
        });
      }, elements: function elements(t) {
        var e = t.closest;return yt(this.links, e || "*");
      }, targets: function targets() {
        return Ae(this.links.map(function (t) {
          return It(t.hash).substr(1);
        }).join(","));
      } }, update: [{ read: function read() {
        this.scroll && this.$create("scroll", this.links, { offset: this.offset || 0 });
      } }, { read: function read(o) {
        var s = this,
            a = window.pageYOffset + this.offset + 1,
            u = on(document) - on(window) + this.offset;o.active = !1, this.targets.every(function (t, e) {
          var n = en(t).top,
              i = e + 1 === s.targets.length;if (!s.overflow && (0 === e && a < n || i && n + t.offsetTop < a)) return !1;if (!i && en(s.targets[e + 1]).top <= a) return !0;if (u <= a) for (var r = s.targets.length - 1; e < r; r--) {
            if (fn(s.targets[r])) {
              t = s.targets[r];break;
            }
          }return !(o.active = Te(_t(s.links, '[href="#' + t.id + '"]')));
        });
      }, write: function write(t) {
        var e = t.active;this.links.forEach(function (t) {
          return t.blur();
        }), Ce(this.elements, this.cls), e && zt(this.$el, "active", [e, Ee(this.closest ? yt(e, this.closest) : e, this.cls)]);
      }, events: ["scroll", "resize"] }] },
      hr = { mixins: [Qn, Ui], props: { top: null, bottom: Boolean, offset: Number, animation: String, clsActive: String, clsInactive: String, clsFixed: String, clsBelow: String, selTarget: String, widthElement: Boolean, showOnUp: Boolean, targetOffset: Number }, data: { top: 0, bottom: !1, offset: 0, animation: "", clsActive: "uk-active", clsInactive: "", clsFixed: "uk-sticky-fixed", clsBelow: "uk-sticky-below", selTarget: "", widthElement: !1, showOnUp: !1, targetOffset: !1 }, computed: { selTarget: function selTarget(t, e) {
        var n = t.selTarget;return n && Te(n, e) || e;
      }, widthElement: function widthElement(t, e) {
        return at(t.widthElement, e) || this.placeholder;
      }, isActive: { get: function get() {
          return Me(this.selTarget, this.clsActive);
        }, set: function set(t) {
          t && !this.isActive ? (Ne(this.selTarget, this.clsInactive, this.clsActive), zt(this.$el, "active")) : t || Me(this.selTarget, this.clsInactive) || (Ne(this.selTarget, this.clsActive, this.clsInactive), zt(this.$el, "inactive"));
        } } }, connected: function connected() {
      this.placeholder = Te("+ .uk-sticky-placeholder", this.$el) || Te('<div class="uk-sticky-placeholder"></div>'), this.isFixed = !1, this.isActive = !1;
    }, disconnected: function disconnected() {
      this.isFixed && (this.hide(), Ce(this.selTarget, this.clsInactive)), ge(this.placeholder), this.placeholder = null, this.widthElement = null;
    }, events: [{ name: "load hashchange popstate", el: window, handler: function handler() {
        var i = this;if (!1 !== this.targetOffset && location.hash && 0 < window.pageYOffset) {
          var r = Te(location.hash);r && yn.read(function () {
            var t = en(r).top,
                e = en(i.$el).top,
                n = i.$el.offsetHeight;i.isFixed && t <= e + n && e <= t + r.offsetHeight && mn(window, t - n - (D(i.targetOffset) ? i.targetOffset : 0) - i.offset);
          });
        }
      } }], update: [{ read: function read(t, e) {
        var n = t.height;this.isActive && "update" !== e && (this.hide(), n = this.$el.offsetHeight, this.show()), n = this.isActive ? n : this.$el.offsetHeight, this.topOffset = en(this.isFixed ? this.placeholder : this.$el).top, this.bottomOffset = this.topOffset + n;var i = lr("bottom", this);return this.top = Math.max(F(lr("top", this)), this.topOffset) - this.offset, this.bottom = i && i - n, this.inactive = !this.matchMedia, { lastScroll: !1, height: n, margins: Pe(this.$el, ["marginTop", "marginBottom", "marginLeft", "marginRight"]) };
      }, write: function write(t) {
        var e = t.height,
            n = t.margins,
            i = this.placeholder;Pe(i, X({ height: e }, n)), Nt(i, document) || (me(this.$el, i), it(i, "hidden", "")), this.isActive = this.isActive;
      }, events: ["resize"] }, { read: function read(t) {
        var e = t.scroll;return void 0 === e && (e = 0), this.width = (St(this.widthElement) ? this.widthElement : this.$el).offsetWidth, this.scroll = window.pageYOffset, { dir: e <= this.scroll ? "down" : "up", scroll: this.scroll, visible: St(this.$el), top: vn(this.placeholder)[0] };
      }, write: function write(t, e) {
        var n = this,
            i = t.initTimestamp;void 0 === i && (i = 0);var r = t.dir,
            o = t.lastDir,
            s = t.lastScroll,
            a = t.scroll,
            u = t.top,
            c = t.visible,
            h = performance.now();if (!((t.lastScroll = a) < 0 || a === s || !c || this.disabled || this.showOnUp && "scroll" !== e || ((300 < h - i || r !== o) && (t.initScroll = a, t.initTimestamp = h), t.lastDir = r, this.showOnUp && Math.abs(t.initScroll - a) <= 30 && Math.abs(s - a) <= 10))) if (this.inactive || a < this.top || this.showOnUp && (a <= this.top || "down" === r || "up" === r && !this.isFixed && a <= this.bottomOffset)) {
          if (!this.isFixed) return void (Ze.inProgress(this.$el) && a < u && (Ze.cancel(this.$el), this.hide()));this.isFixed = !1, this.animation && a > this.topOffset ? (Ze.cancel(this.$el), Ze.out(this.$el, this.animation).then(function () {
            return n.hide();
          }, Q)) : this.hide();
        } else this.isFixed ? this.update() : this.animation ? (Ze.cancel(this.$el), this.show(), Ze.in(this.$el, this.animation).catch(Q)) : this.show();
      }, events: ["resize", "scroll"] }], methods: { show: function show() {
        this.isFixed = !0, this.update(), it(this.placeholder, "hidden", null);
      }, hide: function hide() {
        this.isActive = !1, Ce(this.$el, this.clsFixed, this.clsBelow), Pe(this.$el, { position: "", top: "", width: "" }), it(this.placeholder, "hidden", "");
      }, update: function update() {
        var t = 0 !== this.top || this.scroll > this.top,
            e = Math.max(0, this.offset);this.bottom && this.scroll > this.bottom - this.offset && (e = this.bottom - this.scroll), Pe(this.$el, { position: "fixed", top: e + "px", width: this.width }), this.isActive = t, Oe(this.$el, this.clsBelow, this.scroll > this.bottomOffset), Ee(this.$el, this.clsFixed);
      } } };function lr(t, e) {
    var n = e.$props,
        i = e.$el,
        r = e[t + "Offset"],
        o = n[t];if (o) {
      if (D(o)) return r + F(o);if (B(o) && o.match(/^-?\d+vh$/)) return on(window) * F(o) / 100;var s = !0 === o ? i.parentNode : at(o, i);return s ? en(s).top + s.offsetHeight : void 0;
    }
  }var dr,
      fr = { mixins: [ti], args: "connect", props: { connect: String, toggle: String, active: Number, swiping: Boolean }, data: { connect: "~.uk-switcher", toggle: "> * > :first-child", active: 0, swiping: !0, cls: "uk-active", clsContainer: "uk-switcher", attrItem: "uk-switcher-item", queued: !0 }, computed: { connects: function connects(t, e) {
        return ut(t.connect, e);
      }, toggles: function toggles(t, e) {
        return Ae(t.toggle, e);
      } }, events: [{ name: "click", delegate: function delegate() {
        return this.toggle + ":not(.uk-disabled)";
      }, handler: function handler(e) {
        e.preventDefault(), this.show(V(this.$el.children).filter(function (t) {
          return Nt(e.current, t);
        })[0]);
      } }, { name: "click", el: function el() {
        return this.connects;
      }, delegate: function delegate() {
        return "[" + this.attrItem + "],[data-" + this.attrItem + "]";
      }, handler: function handler(t) {
        t.preventDefault(), this.show(st(t.current, this.attrItem));
      } }, { name: "swipeRight swipeLeft", filter: function filter() {
        return this.swiping;
      }, el: function el() {
        return this.connects;
      }, handler: function handler(t) {
        var e = t.type;this.show(h(e, "Left") ? "next" : "previous");
      } }], update: function update() {
      var e = this;this.connects.forEach(function (t) {
        return e.updateAria(t.children);
      });var t = this.$el.children;this.show(_t(t, "." + this.cls)[0] || t[this.active] || t[0]);
    }, methods: { index: function index() {
        return !H(this.connects) && ce(_t(this.connects[0].children, "." + this.cls)[0]);
      }, show: function show(t) {
        for (var e, n, i = this, r = this.$el.children, o = r.length, s = this.index(), a = 0 <= s, u = "previous" === t ? -1 : 1, c = he(t, r, s), h = 0; h < o; h++, c = (c + u + o) % o) {
          if (!bt(this.toggles[c], ".uk-disabled *, .uk-disabled, [disabled]")) {
            e = this.toggles[c], n = r[c];break;
          }
        }!n || 0 <= s && Me(n, this.cls) || s === c || (Ce(r, this.cls), Ee(n, this.cls), it(this.toggles, "aria-expanded", !1), it(e, "aria-expanded", !0), this.connects.forEach(function (t) {
          a ? i.toggleElement([t.children[s], t.children[c]]) : i.toggleNow(t.children[c]);
        }));
      } } },
      pr = { mixins: [Qn], extends: fr, props: { media: Boolean }, data: { media: 960, attrItem: "uk-tab-item" }, connected: function connected() {
      var t = Me(this.$el, "uk-tab-left") ? "uk-tab-left" : !!Me(this.$el, "uk-tab-right") && "uk-tab-right";t && this.$create("toggle", this.$el, { cls: t, mode: "media", media: this.media });
    } },
      mr = { mixins: [Ui, ti], args: "target", props: { href: String, target: null, mode: "list" }, data: { href: !1, target: !1, mode: "click", queued: !0 }, computed: { target: function target(t, e) {
        var n = t.href,
            i = t.target;return (i = ut(i || n, e)).length && i || [e];
      } }, connected: function connected() {
      zt(this.target, "updatearia", [this]);
    }, events: [{ name: oe + " " + se, filter: function filter() {
        return x(this.mode, "hover");
      }, handler: function handler(t) {
        Ft(t) || this.toggle("toggle" + (t.type === oe ? "show" : "hide"));
      } }, { name: "click", filter: function filter() {
        return x(this.mode, "click") || ee && x(this.mode, "hover");
      }, handler: function handler(t) {
        var e;(yt(t.target, 'a[href="#"], a[href=""]') || (e = yt(t.target, "a[href]")) && (this.cls || !St(this.target) || e.hash && bt(this.target, e.hash))) && t.preventDefault(), this.toggle();
      } }], update: { read: function read() {
        return !(!x(this.mode, "media") || !this.media) && { match: this.matchMedia };
      }, write: function write(t) {
        var e = t.match,
            n = this.isToggled(this.target);(e ? !n : n) && this.toggle();
      }, events: ["resize"] }, methods: { toggle: function toggle(t) {
        zt(this.target, t || "toggle", [this]) && this.toggleElement(this.target);
      } } };Hn.version = "3.1.4", (dr = Hn).component("accordion", ei), dr.component("alert", ni), dr.component("cover", ai), dr.component("drop", ui), dr.component("dropdown", ci), dr.component("formCustom", hi), dr.component("gif", li), dr.component("grid", mi), dr.component("heightMatch", gi), dr.component("heightViewport", xi), dr.component("icon", _i), dr.component("img", Di), dr.component("leader", Ji), dr.component("margin", di), dr.component("modal", tr), dr.component("nav", er), dr.component("navbar", nr), dr.component("offcanvas", ir), dr.component("overflowAuto", or), dr.component("responsive", sr), dr.component("scroll", ar), dr.component("scrollspy", ur), dr.component("scrollspyNav", cr), dr.component("sticky", hr), dr.component("svg", ki), dr.component("switcher", fr), dr.component("tab", pr), dr.component("toggle", mr), dr.component("video", si), dr.component("close", Bi), dr.component("marker", Ni), dr.component("navbarToggleIcon", Ni), dr.component("overlayIcon", Ni), dr.component("paginationNext", Ni), dr.component("paginationPrevious", Ni), dr.component("searchIcon", Oi), dr.component("slidenavNext", Mi), dr.component("slidenavPrevious", Mi), dr.component("spinner", zi), dr.component("totop", Ni), dr.use(ii);var vr = { slide: { show: function show(t) {
        return [{ transform: wr(-100 * t) }, { transform: wr() }];
      }, percent: function percent(t) {
        return gr(t);
      }, translate: function translate(t, e) {
        return [{ transform: wr(-100 * e * t) }, { transform: wr(100 * e * (1 - t)) }];
      } } };function gr(t) {
    return Math.abs(Pe(t, "transform").split(",")[4] / t.offsetWidth) || 0;
  }function wr(t, e) {
    return void 0 === t && (t = 0), void 0 === e && (e = "%"), "translateX(" + t + (t ? e : "") + ")";
  }function br(t) {
    return "scale3d(" + t + ", " + t + ", 1)";
  }var xr = X({}, vr, { fade: { show: function show() {
        return [{ opacity: 0 }, { opacity: 1 }];
      }, percent: function percent(t) {
        return 1 - Pe(t, "opacity");
      }, translate: function translate(t) {
        return [{ opacity: 1 - t }, { opacity: t }];
      } }, scale: { show: function show() {
        return [{ opacity: 0, transform: br(.8) }, { opacity: 1, transform: br(1) }];
      }, percent: function percent(t) {
        return 1 - Pe(t, "opacity");
      }, translate: function translate(t) {
        return [{ opacity: 1 - t, transform: br(1 - .2 * t) }, { opacity: t, transform: br(.8 + .2 * t) }];
      } } });function yr(t, e, n) {
    zt(t, Dt(e, !1, !1, n));
  }var kr = { mixins: [{ props: { autoplay: Boolean, autoplayInterval: Number, pauseOnHover: Boolean }, data: { autoplay: !1, autoplayInterval: 7e3, pauseOnHover: !0 }, connected: function connected() {
        this.autoplay && this.startAutoplay();
      }, disconnected: function disconnected() {
        this.stopAutoplay();
      }, update: function update() {
        it(this.slides, "tabindex", "-1");
      }, events: [{ name: "visibilitychange", el: document, filter: function filter() {
          return this.autoplay;
        }, handler: function handler() {
          document.hidden ? this.stopAutoplay() : this.startAutoplay();
        } }, { name: "mouseenter", filter: function filter() {
          return this.autoplay && this.pauseOnHover;
        }, handler: function handler() {
          this.isHovering = !0;
        } }, { name: "mouseleave", filter: function filter() {
          return this.autoplay && this.pauseOnHover;
        }, handler: function handler() {
          this.isHovering = !1;
        } }], methods: { startAutoplay: function startAutoplay() {
          var t = this;this.stopAutoplay(), this.interval = setInterval(function () {
            return !Nt(document.activeElement, t.$el) && !t.isHovering && !t.stack.length && t.show("next");
          }, this.autoplayInterval);
        }, stopAutoplay: function stopAutoplay() {
          this.interval && clearInterval(this.interval);
        } } }, { props: { draggable: Boolean }, data: { draggable: !0, threshold: 10 }, created: function created() {
        var i = this;["start", "move", "end"].forEach(function (t) {
          var n = i[t];i[t] = function (t) {
            var e = Wt(t).x * (Zt ? -1 : 1);i.prevPos = e !== i.pos ? i.pos : i.prevPos, i.pos = e, n(t);
          };
        });
      }, events: [{ name: ne, delegate: function delegate() {
          return this.selSlides;
        }, handler: function handler(t) {
          !this.draggable || !Ft(t) && function (t) {
            return !t.children.length && t.childNodes.length;
          }(t.target) || 0 < t.button || this.length < 2 || this.start(t);
        } }, { name: "touchmove", passive: !1, handler: "move", delegate: function delegate() {
          return this.selSlides;
        } }, { name: "dragstart", handler: function handler(t) {
          t.preventDefault();
        } }], methods: { start: function start() {
          var t = this;this.drag = this.pos, this._transitioner ? (this.percent = this._transitioner.percent(), this.drag += this._transitioner.getDistance() * this.percent * this.dir, this._transitioner.cancel(), this._transitioner.translate(this.percent), this.dragging = !0, this.stack = []) : this.prevIndex = this.index;var e = "touchmove" != ie ? Mt(document, ie, this.move, { passive: !1 }) : Q;this.unbindMove = function () {
            e(), t.unbindMove = null;
          }, Mt(window, "scroll", this.unbindMove), Mt(document, re, this.end, !0), Pe(this.list, "userSelect", "none");
        }, move: function move(t) {
          var e = this;if (this.unbindMove) {
            var n = this.pos - this.drag;if (!(0 == n || this.prevPos === this.pos || !this.dragging && Math.abs(n) < this.threshold)) {
              Pe(this.list, "pointerEvents", "none"), t.cancelable && t.preventDefault(), this.dragging = !0, this.dir = n < 0 ? 1 : -1;for (var i = this.slides, r = this.prevIndex, o = Math.abs(n), s = this.getIndex(r + this.dir, r), a = this._getDistance(r, s) || i[r].offsetWidth; s !== r && a < o;) {
                this.drag -= a * this.dir, r = s, o -= a, s = this.getIndex(r + this.dir, r), a = this._getDistance(r, s) || i[r].offsetWidth;
              }this.percent = o / a;var u,
                  c = i[r],
                  h = i[s],
                  l = this.index !== s,
                  d = r === s;[this.index, this.prevIndex].filter(function (t) {
                return !x([s, r], t);
              }).forEach(function (t) {
                zt(i[t], "itemhidden", [e]), d && (u = !0, e.prevIndex = r);
              }), (this.index === r && this.prevIndex !== r || u) && zt(i[this.index], "itemshown", [this]), l && (this.prevIndex = r, this.index = s, d || zt(c, "beforeitemhide", [this]), zt(h, "beforeitemshow", [this])), this._transitioner = this._translate(Math.abs(this.percent), c, !d && h), l && (d || zt(c, "itemhide", [this]), zt(h, "itemshow", [this]));
            }
          }
        }, end: function end() {
          if (Ot(window, "scroll", this.unbindMove), this.unbindMove && this.unbindMove(), Ot(document, re, this.end, !0), this.dragging) if (this.dragging = null, this.index === this.prevIndex) this.percent = 1 - this.percent, this.dir *= -1, this._show(!1, this.index, !0), this._transitioner = null;else {
            var t = (Zt ? this.dir * (Zt ? 1 : -1) : this.dir) < 0 == this.prevPos > this.pos;this.index = t ? this.index : this.prevIndex, t && (this.percent = 1 - this.percent), this.show(0 < this.dir && !t || this.dir < 0 && t ? "next" : "previous", !0);
          }Pe(this.list, { userSelect: "", pointerEvents: "" }), this.drag = this.percent = null;
        } } }, { data: { selNav: !1 }, computed: { nav: function nav(t, e) {
          return Te(t.selNav, e);
        }, selNavItem: function selNavItem(t) {
          var e = t.attrItem;return "[" + e + "],[data-" + e + "]";
        }, navItems: function navItems(t, e) {
          return Ae(this.selNavItem, e);
        } }, update: { write: function write() {
          var n = this;this.nav && this.length !== this.nav.children.length && de(this.nav, this.slides.map(function (t, e) {
            return "<li " + n.attrItem + '="' + e + '"><a href="#"></a></li>';
          }).join("")), Oe(Ae(this.selNavItem, this.$el).concat(this.nav), "uk-hidden", !this.maxIndex), this.updateNav();
        }, events: ["resize"] }, events: [{ name: "click", delegate: function delegate() {
          return this.selNavItem;
        }, handler: function handler(t) {
          t.preventDefault(), this.show(st(t.current, this.attrItem));
        } }, { name: "itemshow", handler: "updateNav" }], methods: { updateNav: function updateNav() {
          var n = this,
              i = this.getValidIndex();this.navItems.forEach(function (t) {
            var e = st(t, n.attrItem);Oe(t, n.clsActive, j(e) === i), Oe(t, "uk-invisible", n.finite && ("previous" === e && 0 === i || "next" === e && i >= n.maxIndex));
          });
        } } }], props: { clsActivated: Boolean, easing: String, index: Number, finite: Boolean, velocity: Number }, data: function data() {
      return { easing: "ease", finite: !1, velocity: 1, index: 0, stack: [], percent: 0, clsActive: "uk-active", clsActivated: !1, Transitioner: !1, transitionOptions: {} };
    }, computed: { duration: function duration(t, e) {
        var n = t.velocity;return $r(e.offsetWidth / n);
      }, length: function length() {
        return this.slides.length;
      }, list: function list(t, e) {
        return Te(t.selList, e);
      }, maxIndex: function maxIndex() {
        return this.length - 1;
      }, selSlides: function selSlides(t) {
        return t.selList + " > *";
      }, slides: function slides() {
        return V(this.list.children);
      } }, events: { itemshown: function itemshown() {
        this.$update(this.list);
      } }, methods: { show: function show(t, e) {
        var n = this;if (void 0 === e && (e = !1), !this.dragging && this.length) {
          var i = this.stack,
              r = e ? 0 : i.length,
              o = function o() {
            i.splice(r, 1), i.length && n.show(i.shift(), !0);
          };if (i[e ? "unshift" : "push"](t), !e && 1 < i.length) 2 === i.length && this._transitioner.forward(Math.min(this.duration, 200));else {
            var s = this.index,
                a = Me(this.slides, this.clsActive) && this.slides[s],
                u = this.getIndex(t, this.index),
                c = this.slides[u];if (a !== c) {
              if (this.dir = function (t, e) {
                return "next" === t ? 1 : "previous" === t ? -1 : t < e ? -1 : 1;
              }(t, s), this.prevIndex = s, this.index = u, a && zt(a, "beforeitemhide", [this]), !zt(c, "beforeitemshow", [this, a])) return this.index = this.prevIndex, void o();var h = this._show(a, c, e).then(function () {
                return a && zt(a, "itemhidden", [n]), zt(c, "itemshown", [n]), new qt(function (t) {
                  yn.write(function () {
                    i.shift(), i.length ? n.show(i.shift(), !0) : n._transitioner = null, t();
                  });
                });
              });return a && zt(a, "itemhide", [this]), zt(c, "itemshow", [this]), h;
            }o();
          }
        }
      }, getIndex: function getIndex(t, e) {
        return void 0 === t && (t = this.index), void 0 === e && (e = this.index), Z(he(t, this.slides, e, this.finite), 0, this.maxIndex);
      }, getValidIndex: function getValidIndex(t, e) {
        return void 0 === t && (t = this.index), void 0 === e && (e = this.prevIndex), this.getIndex(t, e);
      }, _show: function _show(t, e, n) {
        if (this._transitioner = this._getTransitioner(t, e, this.dir, X({ easing: n ? e.offsetWidth < 600 ? "cubic-bezier(0.25, 0.46, 0.45, 0.94)" : "cubic-bezier(0.165, 0.84, 0.44, 1)" : this.easing }, this.transitionOptions)), !n && !t) return this._transitioner.translate(1), qt.resolve();var i = this.stack.length;return this._transitioner[1 < i ? "forward" : "show"](1 < i ? Math.min(this.duration, 75 + 75 / (i - 1)) : this.duration, this.percent);
      }, _getDistance: function _getDistance(t, e) {
        return new this._getTransitioner(t, t !== e && e).getDistance();
      }, _translate: function _translate(t, e, n) {
        void 0 === e && (e = this.prevIndex), void 0 === n && (n = this.index);var i = this._getTransitioner(e !== n && e, n);return i.translate(t), i;
      }, _getTransitioner: function _getTransitioner(t, e, n, i) {
        return void 0 === t && (t = this.prevIndex), void 0 === e && (e = this.index), void 0 === n && (n = this.dir || 1), void 0 === i && (i = this.transitionOptions), new this.Transitioner(z(t) ? this.slides[t] : t, z(e) ? this.slides[e] : e, n * (Zt ? -1 : 1), i);
      } } };function $r(t) {
    return .5 * t + 300;
  }var Ir = { mixins: [kr], props: { animation: String }, data: { animation: "slide", clsActivated: "uk-transition-active", Animations: vr, Transitioner: function Transitioner(o, s, a, t) {
        var e = t.animation,
            u = t.easing,
            n = e.percent,
            i = e.translate,
            r = e.show;void 0 === r && (r = Q);var c = r(a),
            h = new Vt();return { dir: a, show: function show(t, e, n) {
            var i = this;void 0 === e && (e = 0);var r = n ? "linear" : u;return t -= Math.round(t * Z(e, -1, 1)), this.translate(e), yr(s, "itemin", { percent: e, duration: t, timing: r, dir: a }), yr(o, "itemout", { percent: 1 - e, duration: t, timing: r, dir: a }), qt.all([Ue.start(s, c[1], t, r), Ue.start(o, c[0], t, r)]).then(function () {
              i.reset(), h.resolve();
            }, Q), h.promise;
          }, stop: function stop() {
            return Ue.stop([s, o]);
          }, cancel: function cancel() {
            Ue.cancel([s, o]);
          }, reset: function reset() {
            for (var t in c[0]) {
              Pe([s, o], t, "");
            }
          }, forward: function forward(t, e) {
            return void 0 === e && (e = this.percent()), Ue.cancel([s, o]), this.show(t, e, !0);
          }, translate: function translate(t) {
            this.reset();var e = i(t, a);Pe(s, e[1]), Pe(o, e[0]), yr(s, "itemtranslatein", { percent: t, dir: a }), yr(o, "itemtranslateout", { percent: 1 - t, dir: a });
          }, percent: function percent() {
            return n(o || s, s, a);
          }, getDistance: function getDistance() {
            return o && o.offsetWidth;
          } };
      } }, computed: { animation: function animation(t) {
        var e = t.animation,
            n = t.Animations;return X(e in n ? n[e] : n.slide, { name: e });
      }, transitionOptions: function transitionOptions() {
        return { animation: this.animation };
      } }, events: { "itemshow itemhide itemshown itemhidden": function itemshowItemhideItemshownItemhidden(t) {
        var e = t.target;this.$update(e);
      }, itemshow: function itemshow() {
        z(this.prevIndex) && yn.flush();
      }, beforeitemshow: function beforeitemshow(t) {
        Ee(t.target, this.clsActive);
      }, itemshown: function itemshown(t) {
        Ee(t.target, this.clsActivated);
      }, itemhidden: function itemhidden(t) {
        Ce(t.target, this.clsActive, this.clsActivated);
      } } },
      Tr = { mixins: [Ki, Zi, ti, Ir], functional: !0, props: { delayControls: Number, preload: Number, videoAutoplay: Boolean, template: String }, data: function data() {
      return { preload: 1, videoAutoplay: !1, delayControls: 3e3, items: [], cls: "uk-open", clsPage: "uk-lightbox-page", selList: ".uk-lightbox-items", attrItem: "uk-lightbox-item", selClose: ".uk-close-large", pauseOnHover: !1, velocity: 2, Animations: xr, template: '<div class="uk-lightbox uk-overflow-hidden"> <ul class="uk-lightbox-items"></ul> <div class="uk-lightbox-toolbar uk-position-top uk-text-right uk-transition-slide-top uk-transition-opaque"> <button class="uk-lightbox-toolbar-icon uk-close-large" type="button" uk-close></button> </div> <a class="uk-lightbox-button uk-position-center-left uk-position-medium uk-transition-fade" href="#" uk-slidenav-previous uk-lightbox-item="previous"></a> <a class="uk-lightbox-button uk-position-center-right uk-position-medium uk-transition-fade" href="#" uk-slidenav-next uk-lightbox-item="next"></a> <div class="uk-lightbox-toolbar uk-lightbox-caption uk-position-bottom uk-text-center uk-transition-slide-bottom uk-transition-opaque"></div> </div>' };
    }, created: function created() {
      var t = this;this.$mount(fe(this.container, this.template)), this.caption = Te(".uk-lightbox-caption", this.$el), this.items.forEach(function () {
        return fe(t.list, "<li></li>");
      });
    }, events: [{ name: ie + " " + ne + " keydown", handler: "showControls" }, { name: "click", self: !0, delegate: function delegate() {
        return this.selSlides;
      }, handler: function handler(t) {
        t.defaultPrevented || this.hide();
      } }, { name: "shown", self: !0, handler: function handler() {
        this.showControls();
      } }, { name: "hide", self: !0, handler: function handler() {
        this.hideControls(), Ce(this.slides, this.clsActive), Ue.stop(this.slides);
      } }, { name: "hidden", self: !0, handler: function handler() {
        this.$destroy(!0);
      } }, { name: "keyup", el: document, handler: function handler(t) {
        if (this.isToggled(this.$el)) switch (t.keyCode) {case 37:
            this.show("previous");break;case 39:
            this.show("next");}
      } }, { name: "beforeitemshow", handler: function handler(t) {
        this.isToggled() || (this.draggable = !1, t.preventDefault(), this.toggleNow(this.$el, !0), this.animation = xr.scale, Ce(t.target, this.clsActive), this.stack.splice(1, 0, this.index));
      } }, { name: "itemshow", handler: function handler(t) {
        var e = ce(t.target),
            n = this.getItem(e).caption;Pe(this.caption, "display", n ? "" : "none"), de(this.caption, n);for (var i = 0; i <= this.preload; i++) {
          this.loadItem(this.getIndex(e + i)), this.loadItem(this.getIndex(e - i));
        }
      } }, { name: "itemshown", handler: function handler() {
        this.draggable = this.$props.draggable;
      } }, { name: "itemload", handler: function handler(t, r) {
        var o,
            s = this,
            e = r.source,
            n = r.type,
            i = r.alt;if (this.setItem(r, "<span uk-spinner></span>"), e) if ("image" === n || e.match(/\.(jp(e)?g|png|gif|svg|webp)($|\?)/i)) Jt(e).then(function (t) {
          return s.setItem(r, '<img width="' + t.width + '" height="' + t.height + '" src="' + e + '" alt="' + (i || "") + '">');
        }, function () {
          return s.setError(r);
        });else if ("video" === n || e.match(/\.(mp4|webm|ogv)($|\?)/i)) {
          var a = Te("<video controls playsinline" + (r.poster ? ' poster="' + r.poster + '"' : "") + ' uk-video="' + this.videoAutoplay + '"></video>');it(a, "src", e), Bt(a, "error loadedmetadata", function (t) {
            "error" === t ? s.setError(r) : (it(a, { width: a.videoWidth, height: a.videoHeight }), s.setItem(r, a));
          });
        } else if ("iframe" === n || e.match(/\.(html|php)($|\?)/i)) this.setItem(r, '<iframe class="uk-lightbox-iframe" src="' + e + '" frameborder="0" allowfullscreen></iframe>');else if (o = e.match(/\/\/.*?youtube(-nocookie)?\.[a-z]+\/watch\?v=([^&\s]+)/) || e.match(/()youtu\.be\/(.*)/)) {
          var u = o[2],
              c = function c(t, e) {
            return void 0 === t && (t = 640), void 0 === e && (e = 450), s.setItem(r, Ar("https://www.youtube" + (o[1] || "") + ".com/embed/" + u, t, e, s.videoAutoplay));
          };Jt("https://img.youtube.com/vi/" + u + "/maxresdefault.jpg").then(function (t) {
            var e = t.width,
                n = t.height;120 === e && 90 === n ? Jt("https://img.youtube.com/vi/" + u + "/0.jpg").then(function (t) {
              var e = t.width,
                  n = t.height;return c(e, n);
            }, c) : c(e, n);
          }, c);
        } else (o = e.match(/(\/\/.*?)vimeo\.[a-z]+\/([0-9]+).*?/)) && Gt("https://vimeo.com/api/oembed.json?maxwidth=1920&url=" + encodeURI(e), { responseType: "json", withCredentials: !1 }).then(function (t) {
          var e = t.response,
              n = e.height,
              i = e.width;return s.setItem(r, Ar("https://player.vimeo.com/video/" + o[2], i, n, s.videoAutoplay));
        }, function () {
          return s.setError(r);
        });
      } }], methods: { loadItem: function loadItem(t) {
        void 0 === t && (t = this.index);var e = this.getItem(t);e.content || zt(this.$el, "itemload", [e]);
      }, getItem: function getItem(t) {
        return void 0 === t && (t = this.index), this.items[t] || {};
      }, setItem: function setItem(t, e) {
        X(t, { content: e });var n = de(this.slides[this.items.indexOf(t)], e);zt(this.$el, "itemloaded", [this, n]), this.$update(n);
      }, setError: function setError(t) {
        this.setItem(t, '<span uk-icon="icon: bolt; ratio: 2"></span>');
      }, showControls: function showControls() {
        clearTimeout(this.controlsTimer), this.controlsTimer = setTimeout(this.hideControls, this.delayControls), Ee(this.$el, "uk-active", "uk-transition-active");
      }, hideControls: function hideControls() {
        Ce(this.$el, "uk-active", "uk-transition-active");
      } } };function Ar(t, e, n, i) {
    return '<iframe src="' + t + '" width="' + e + '" height="' + n + '" style="max-width: 100%; box-sizing: border-box;" frameborder="0" allowfullscreen uk-video="autoplay: ' + i + '" uk-responsive></iframe>';
  }var Sr,
      Er = { install: function install(t, e) {
      t.lightboxPanel || t.component("lightboxPanel", Tr);X(e.props, t.component("lightboxPanel").options.props);
    }, props: { toggle: String }, data: { toggle: "a" }, computed: { toggles: { get: function get(t, e) {
          return Ae(t.toggle, e);
        }, watch: function watch() {
          this.hide();
        } }, items: function items() {
        return K(this.toggles.map(Cr), "source");
      } }, disconnected: function disconnected() {
      this.hide();
    }, events: [{ name: "click", delegate: function delegate() {
        return this.toggle + ":not(.uk-disabled)";
      }, handler: function handler(t) {
        t.preventDefault();var e = st(t.current, "href");this.show(k(this.items, function (t) {
          return t.source === e;
        }));
      } }], methods: { show: function show(t) {
        var e = this;return this.panel = this.panel || this.$create("lightboxPanel", X({}, this.$props, { items: this.items })), Mt(this.panel.$el, "hidden", function () {
          return e.panel = !1;
        }), this.panel.show(t);
      }, hide: function hide() {
        return this.panel && this.panel.hide();
      } } };function Cr(n) {
    return ["href", "caption", "type", "poster", "alt"].reduce(function (t, e) {
      return t["href" === e ? "source" : e] = st(n, e), t;
    }, {});
  }(Sr = { click: function click(t) {
      yt(t.target, 'a[href="#"],a[href=""]') && t.preventDefault(), this.close();
    } })[oe] = function () {
    this.timer && clearTimeout(this.timer);
  }, Sr[se] = function () {
    this.timeout && (this.timer = setTimeout(this.close, this.timeout));
  };var _r = ["x", "y", "bgx", "bgy", "rotate", "scale", "color", "backgroundColor", "borderColor", "opacity", "blur", "hue", "grayscale", "invert", "saturate", "sepia", "fopacity", "stroke"],
      Nr = { mixins: [Ui], props: _r.reduce(function (t, e) {
      return t[e] = "list", t;
    }, {}), data: _r.reduce(function (t, e) {
      return t[e] = void 0, t;
    }, {}), computed: { props: function props(m, v) {
        var g = this;return _r.reduce(function (t, e) {
          if (P(m[e])) return t;var n,
              i,
              r,
              o = e.match(/color/i),
              s = o || "opacity" === e,
              a = m[e].slice(0);s && Pe(v, e, ""), a.length < 2 && a.unshift(("scale" === e ? 1 : s ? Pe(v, e) : 0) || 0);var u = function (t) {
            return t.reduce(function (t, e) {
              return B(e) && e.replace(/-|\d/g, "").trim() || t;
            }, "");
          }(a);if (o) {
            var c = v.style.color;a = a.map(function (t) {
              return function (t, e) {
                return Pe(Pe(t, "color", e), "color").split(/[(),]/g).slice(1, -1).concat(1).slice(0, 4).map(F);
              }(v, t);
            }), v.style.color = c;
          } else if (w(e, "bg")) {
            var h = "bgy" === e ? "height" : "width";if (a = a.map(function (t) {
              return gn(t, h, g.$el);
            }), Pe(v, "background-position-" + e[2], ""), i = Pe(v, "backgroundPosition").split(" ")["x" === e[2] ? 0 : 1], g.covers) {
              var l = Math.min.apply(Math, a),
                  d = Math.max.apply(Math, a),
                  f = a.indexOf(l) < a.indexOf(d);r = d - l, a = a.map(function (t) {
                return t - (f ? l : d);
              }), n = (f ? -r : 0) + "px";
            } else n = i;
          } else a = a.map(F);if ("stroke" === e) {
            if (!a.some(function (t) {
              return t;
            })) return t;var p = Ai(g.$el);Pe(v, "strokeDasharray", p), "%" === u && (a = a.map(function (t) {
              return t * p / 100;
            })), a = a.reverse(), e = "strokeDashoffset";
          }return t[e] = { steps: a, unit: u, pos: n, bgPos: i, diff: r }, t;
        }, {});
      }, bgProps: function bgProps() {
        var e = this;return ["bgx", "bgy"].filter(function (t) {
          return t in e.props;
        });
      }, covers: function covers(t, e) {
        return function (t) {
          var e = t.style.backgroundSize,
              n = "cover" === Pe(Pe(t, "backgroundSize", ""), "backgroundSize");return t.style.backgroundSize = e, n;
        }(e);
      } }, disconnected: function disconnected() {
      delete this._image;
    }, update: { read: function read(t) {
        var u = this;if (t.active = this.matchMedia, t.active) {
          if (!t.image && this.covers && this.bgProps.length) {
            var e = Pe(this.$el, "backgroundImage").replace(/^none|url\(["']?(.+?)["']?\)$/, "$1");if (e) {
              var n = new Image();n.src = e, (t.image = n).naturalWidth || (n.onload = function () {
                return u.$emit();
              });
            }
          }var i = t.image;if (i && i.naturalWidth) {
            var c = { width: this.$el.offsetWidth, height: this.$el.offsetHeight },
                h = { width: i.naturalWidth, height: i.naturalHeight },
                l = nt.cover(h, c);this.bgProps.forEach(function (t) {
              var e = u.props[t],
                  n = e.diff,
                  i = e.bgPos,
                  r = e.steps,
                  o = "bgy" === t ? "height" : "width",
                  s = l[o] - c[o];if (s < n) c[o] = l[o] + n - s;else if (n < s) {
                var a = c[o] / gn(i, o, u.$el);a && (u.props[t].steps = r.map(function (t) {
                  return t - (s - n) / a;
                }));
              }l = nt.cover(h, c);
            }), t.dim = l;
          }
        }
      }, write: function write(t) {
        var e = t.dim;t.active ? e && Pe(this.$el, { backgroundSize: e.width + "px " + e.height + "px", backgroundRepeat: "no-repeat" }) : Pe(this.$el, { backgroundSize: "", backgroundRepeat: "" });
      }, events: ["resize"] }, methods: { reset: function reset() {
        var n = this;G(this.getCss(0), function (t, e) {
          return Pe(n.$el, e, "");
        });
      }, getCss: function getCss(l) {
        var d = this.props;return Object.keys(d).reduce(function (t, e) {
          var n = d[e],
              i = n.steps,
              r = n.unit,
              o = n.pos,
              s = function (t, e, n) {
            void 0 === n && (n = 2);var i = Mr(t, e),
                r = i[0],
                o = i[1],
                s = i[2];return (z(r) ? r + Math.abs(r - o) * s * (r < o ? 1 : -1) : +o).toFixed(n);
          }(i, l);switch (e) {case "x":case "y":
              r = r || "px", t.transform += " translate" + p(e) + "(" + F(s).toFixed("px" === r ? 0 : 2) + r + ")";break;case "rotate":
              r = r || "deg", t.transform += " rotate(" + (s + r) + ")";break;case "scale":
              t.transform += " scale(" + s + ")";break;case "bgy":case "bgx":
              t["background-position-" + e[2]] = "calc(" + o + " + " + s + "px)";break;case "color":case "backgroundColor":case "borderColor":
              var a = Mr(i, l),
                  u = a[0],
                  c = a[1],
                  h = a[2];t[e] = "rgba(" + u.map(function (t, e) {
                return t += h * (c[e] - t), 3 === e ? F(t) : parseInt(t, 10);
              }).join(",") + ")";break;case "blur":
              r = r || "px", t.filter += " blur(" + (s + r) + ")";break;case "hue":
              r = r || "deg", t.filter += " hue-rotate(" + (s + r) + ")";break;case "fopacity":
              r = r || "%", t.filter += " opacity(" + (s + r) + ")";break;case "grayscale":case "invert":case "saturate":case "sepia":
              r = r || "%", t.filter += " " + e + "(" + (s + r) + ")";break;default:
              t[e] = s;}return t;
        }, { transform: "", filter: "" });
      } } };function Mr(t, e) {
    var n = t.length - 1,
        i = Math.min(Math.floor(n * e), n - 1),
        r = t.slice(i, i + 2);return r.push(1 === e ? 1 : e % (1 / n) * n), r;
  }var Or = { update: { write: function write() {
        if (!this.stack.length && !this.dragging) {
          var t = this.getValidIndex();delete this.index, Ce(this.slides, this.clsActive, this.clsActivated), this.show(t);
        }
      }, events: ["resize"] } };function Br(t, e, n) {
    var i = Hr(t, e);return n ? i - function (t, e) {
      return Pr(e).width / 2 - Pr(t).width / 2;
    }(t, e) : Math.min(i, zr(e));
  }function zr(t) {
    return Math.max(0, Dr(t) - Pr(t).width);
  }function Dr(t) {
    return jr(t).reduce(function (t, e) {
      return Pr(e).width + t;
    }, 0);
  }function Hr(t, e) {
    return (rn(t).left + (Zt ? Pr(t).width - Pr(e).width : 0)) * (Zt ? -1 : 1);
  }function Pr(t) {
    return t.getBoundingClientRect();
  }function Lr(t, e, n) {
    zt(t, Dt(e, !1, !1, n));
  }function jr(t) {
    return V(t.children);
  }var Fr = { mixins: [Qn, kr, Or], props: { center: Boolean, sets: Boolean }, data: { center: !1, sets: !1, attrItem: "uk-slider-item", selList: ".uk-slider-items", selNav: ".uk-slider-nav", clsContainer: "uk-slider-container", Transitioner: function Transitioner(r, i, o, t) {
        var e = t.center,
            s = t.easing,
            a = t.list,
            u = new Vt(),
            n = r ? Br(r, a, e) : Br(i, a, e) + Pr(i).width * o,
            c = i ? Br(i, a, e) : n + Pr(r).width * o * (Zt ? -1 : 1);return { dir: o, show: function show(t, e, n) {
            void 0 === e && (e = 0);var i = n ? "linear" : s;return t -= Math.round(t * Z(e, -1, 1)), this.translate(e), r && this.updateTranslates(), e = r ? e : Z(e, 0, 1), Lr(this.getItemIn(), "itemin", { percent: e, duration: t, timing: i, dir: o }), r && Lr(this.getItemIn(!0), "itemout", { percent: 1 - e, duration: t, timing: i, dir: o }), Ue.start(a, { transform: wr(-c * (Zt ? -1 : 1), "px") }, t, i).then(u.resolve, Q), u.promise;
          }, stop: function stop() {
            return Ue.stop(a);
          }, cancel: function cancel() {
            Ue.cancel(a);
          }, reset: function reset() {
            Pe(a, "transform", "");
          }, forward: function forward(t, e) {
            return void 0 === e && (e = this.percent()), Ue.cancel(a), this.show(t, e, !0);
          }, translate: function translate(t) {
            var e = this.getDistance() * o * (Zt ? -1 : 1);Pe(a, "transform", wr(Z(e - e * t - c, -Dr(a), Pr(a).width) * (Zt ? -1 : 1), "px")), this.updateTranslates(), r && (t = Z(t, -1, 1), Lr(this.getItemIn(), "itemtranslatein", { percent: t, dir: o }), Lr(this.getItemIn(!0), "itemtranslateout", { percent: 1 - t, dir: o }));
          }, percent: function percent() {
            return Math.abs((Pe(a, "transform").split(",")[4] * (Zt ? -1 : 1) + n) / (c - n));
          }, getDistance: function getDistance() {
            return Math.abs(c - n);
          }, getItemIn: function getItemIn(t) {
            void 0 === t && (t = !1);var e = this.getActives(),
                n = J(jr(a), "offsetLeft"),
                i = ce(n, e[0 < o * (t ? -1 : 1) ? e.length - 1 : 0]);return ~i && n[i + (r && !t ? o : 0)];
          }, getActives: function getActives() {
            var n = Br(r || i, a, e);return J(jr(a).filter(function (t) {
              var e = Hr(t, a);return n <= e && e + Pr(t).width <= Pr(a).width + n;
            }), "offsetLeft");
          }, updateTranslates: function updateTranslates() {
            var n = this.getActives();jr(a).forEach(function (t) {
              var e = x(n, t);Lr(t, "itemtranslate" + (e ? "in" : "out"), { percent: e ? 1 : 0, dir: t.offsetLeft <= i.offsetLeft ? 1 : -1 });
            });
          } };
      } }, computed: { avgWidth: function avgWidth() {
        return Dr(this.list) / this.length;
      }, finite: function finite(t) {
        return t.finite || Dr(this.list) < Pr(this.list).width + function (t) {
          return jr(t).reduce(function (t, e) {
            return Math.max(t, Pr(e).width);
          }, 0);
        }(this.list) + this.center;
      }, maxIndex: function maxIndex() {
        if (!this.finite || this.center && !this.sets) return this.length - 1;if (this.center) return this.sets[this.sets.length - 1];Pe(this.slides, "order", "");for (var t = zr(this.list), e = this.length; e--;) {
          if (Hr(this.list.children[e], this.list) < t) return Math.min(e + 1, this.length - 1);
        }return 0;
      }, sets: function sets(t) {
        var o = this,
            e = t.sets,
            s = Pr(this.list).width / (this.center ? 2 : 1),
            a = 0,
            u = s,
            c = 0;return !H(e = e && this.slides.reduce(function (t, e, n) {
          var i = Pr(e).width;if (a < c + i && (!o.center && n > o.maxIndex && (n = o.maxIndex), !x(t, n))) {
            var r = o.slides[n + 1];o.center && r && i < u - Pr(r).width / 2 ? u -= i : (u = s, t.push(n), a = c + s + (o.center ? i / 2 : 0));
          }return c += i, t;
        }, [])) && e;
      }, transitionOptions: function transitionOptions() {
        return { center: this.center, list: this.list };
      } }, connected: function connected() {
      Oe(this.$el, this.clsContainer, !Te("." + this.clsContainer, this.$el));
    }, update: { write: function write() {
        var n = this;Ae("[" + this.attrItem + "],[data-" + this.attrItem + "]", this.$el).forEach(function (t) {
          var e = st(t, n.attrItem);n.maxIndex && Oe(t, "uk-hidden", D(e) && (n.sets && !x(n.sets, F(e)) || e > n.maxIndex));
        });
      }, events: ["resize"] }, events: { beforeitemshow: function beforeitemshow(t) {
        !this.dragging && this.sets && this.stack.length < 2 && !x(this.sets, this.index) && (this.index = this.getValidIndex());var e = Math.abs(this.index - this.prevIndex + (0 < this.dir && this.index < this.prevIndex || this.dir < 0 && this.index > this.prevIndex ? (this.maxIndex + 1) * this.dir : 0));if (!this.dragging && 1 < e) {
          for (var n = 0; n < e; n++) {
            this.stack.splice(1, 0, 0 < this.dir ? "next" : "previous");
          }t.preventDefault();
        } else this.duration = $r(this.avgWidth / this.velocity) * (Pr(this.dir < 0 || !this.slides[this.prevIndex] ? this.slides[this.index] : this.slides[this.prevIndex]).width / this.avgWidth), this.reorder();
      }, itemshow: function itemshow() {
        P(this.prevIndex) || Ee(this._getTransitioner().getItemIn(), this.clsActive);
      }, itemshown: function itemshown() {
        var e = this,
            n = this._getTransitioner(this.index).getActives();this.slides.forEach(function (t) {
          return Oe(t, e.clsActive, x(n, t));
        }), this.sets && !x(this.sets, F(this.index)) || this.slides.forEach(function (t) {
          return Oe(t, e.clsActivated, x(n, t));
        });
      } }, methods: { reorder: function reorder() {
        var n = this;if (Pe(this.slides, "order", ""), !this.finite) {
          var i = 0 < this.dir && this.slides[this.prevIndex] ? this.prevIndex : this.index;if (this.slides.forEach(function (t, e) {
            return Pe(t, "order", 0 < n.dir && e < i ? 1 : n.dir < 0 && e >= n.index ? -1 : "");
          }), this.center) for (var t = this.slides[i], e = Pr(this.list).width / 2 - Pr(t).width / 2, r = 0; 0 < e;) {
            var o = this.getIndex(--r + i, i),
                s = this.slides[o];Pe(s, "order", i < o ? -2 : -1), e -= Pr(s).width;
          }
        }
      }, getValidIndex: function getValidIndex(t, e) {
        if (void 0 === t && (t = this.index), void 0 === e && (e = this.prevIndex), t = this.getIndex(t, e), !this.sets) return t;var n;do {
          if (x(this.sets, t)) return t;n = t, t = this.getIndex(t + this.dir, e);
        } while (t !== n);return t;
      } } },
      Wr = { mixins: [Nr], data: { selItem: "!li" }, computed: { item: function item(t, e) {
        return at(t.selItem, e);
      } }, events: [{ name: "itemshown", self: !0, el: function el() {
        return this.item;
      }, handler: function handler() {
        Pe(this.$el, this.getCss(.5));
      } }, { name: "itemin itemout", self: !0, el: function el() {
        return this.item;
      }, handler: function handler(t) {
        var e = t.type,
            n = t.detail,
            i = n.percent,
            r = n.duration,
            o = n.timing,
            s = n.dir;Ue.cancel(this.$el), Pe(this.$el, this.getCss(qr(e, s, i))), Ue.start(this.$el, this.getCss(Vr(e) ? .5 : 0 < s ? 1 : 0), r, o).catch(Q);
      } }, { name: "transitioncanceled transitionend", self: !0, el: function el() {
        return this.item;
      }, handler: function handler() {
        Ue.cancel(this.$el);
      } }, { name: "itemtranslatein itemtranslateout", self: !0, el: function el() {
        return this.item;
      }, handler: function handler(t) {
        var e = t.type,
            n = t.detail,
            i = n.percent,
            r = n.dir;Ue.cancel(this.$el), Pe(this.$el, this.getCss(qr(e, r, i)));
      } }] };function Vr(t) {
    return h(t, "in");
  }function qr(t, e, n) {
    return n /= 2, Vr(t) ? e < 0 ? 1 - n : n : e < 0 ? n : 1 - n;
  }var Rr,
      Yr,
      Ur,
      Xr,
      Gr = X({}, vr, { fade: { show: function show() {
        return [{ opacity: 0, zIndex: 0 }, { zIndex: -1 }];
      }, percent: function percent(t) {
        return 1 - Pe(t, "opacity");
      }, translate: function translate(t) {
        return [{ opacity: 1 - t, zIndex: 0 }, { zIndex: -1 }];
      } }, scale: { show: function show() {
        return [{ opacity: 0, transform: br(1.5), zIndex: 0 }, { zIndex: -1 }];
      }, percent: function percent(t) {
        return 1 - Pe(t, "opacity");
      }, translate: function translate(t) {
        return [{ opacity: 1 - t, transform: br(1 + .5 * t), zIndex: 0 }, { zIndex: -1 }];
      } }, pull: { show: function show(t) {
        return t < 0 ? [{ transform: wr(30), zIndex: -1 }, { transform: wr(), zIndex: 0 }] : [{ transform: wr(-100), zIndex: 0 }, { transform: wr(), zIndex: -1 }];
      }, percent: function percent(t, e, n) {
        return n < 0 ? 1 - gr(e) : gr(t);
      }, translate: function translate(t, e) {
        return e < 0 ? [{ transform: wr(30 * t), zIndex: -1 }, { transform: wr(-100 * (1 - t)), zIndex: 0 }] : [{ transform: wr(100 * -t), zIndex: 0 }, { transform: wr(30 * (1 - t)), zIndex: -1 }];
      } }, push: { show: function show(t) {
        return t < 0 ? [{ transform: wr(100), zIndex: 0 }, { transform: wr(), zIndex: -1 }] : [{ transform: wr(-30), zIndex: -1 }, { transform: wr(), zIndex: 0 }];
      }, percent: function percent(t, e, n) {
        return 0 < n ? 1 - gr(e) : gr(t);
      }, translate: function translate(t, e) {
        return e < 0 ? [{ transform: wr(100 * t), zIndex: 0 }, { transform: wr(-30 * (1 - t)), zIndex: -1 }] : [{ transform: wr(-30 * t), zIndex: -1 }, { transform: wr(100 * (1 - t)), zIndex: 0 }];
      } } }),
      Jr = { mixins: [Qn, Ir, Or], props: { ratio: String, minHeight: Number, maxHeight: Number }, data: { ratio: "16:9", minHeight: !1, maxHeight: !1, selList: ".uk-slideshow-items", attrItem: "uk-slideshow-item", selNav: ".uk-slideshow-nav", Animations: Gr }, update: { read: function read() {
        var t = this.ratio.split(":").map(Number),
            e = t[0],
            n = t[1];return n = n * this.list.offsetWidth / e || 0, this.minHeight && (n = Math.max(this.minHeight, n)), this.maxHeight && (n = Math.min(this.maxHeight, n)), { height: n - un(this.list, "content-box") };
      }, write: function write(t) {
        var e = t.height;Pe(this.list, "minHeight", e);
      }, events: ["resize"] } };function Kr() {
    Qr(document.body, Ur), yn.flush(), new MutationObserver(function (t) {
      return t.forEach(Zr);
    }).observe(document, { childList: !0, subtree: !0, characterData: !0, attributes: !0 }), Yr._initialized = !0;
  }function Zr(t) {
    var e = t.target;("attributes" !== t.type ? function (t) {
      for (var e = t.addedNodes, n = t.removedNodes, i = 0; i < e.length; i++) {
        Qr(e[i], Ur);
      }for (var r = 0; r < n.length; r++) {
        Qr(n[r], Xr);
      }return !0;
    }(t) : function (t) {
      var e = t.target,
          n = t.attributeName;if ("href" === n) return !0;var i = Dn(n);if (i && i in Yr) {
        if (rt(e, n)) return Yr[i](e), !0;var r = Yr.getComponent(e, i);return r ? (r.$destroy(), !0) : void 0;
      }
    }(t)) && Yr.update(e);
  }function Qr(t, e) {
    if (1 === t.nodeType && !rt(t, "uk-no-boot")) for (e(t), t = t.firstElementChild; t;) {
      var n = t.nextElementSibling;Qr(t, e), t = n;
    }
  }return Number, String, (Rr = { focus: "show", blur: "hide" })[oe + " " + se] = function (t) {
    Ft(t) || (t.type === oe ? this.show() : this.hide());
  }, Rr[ne] = function (t) {
    Ft(t) && (this.isActive() ? this.hide() : this.show());
  }, Hn.component("lightbox", Er), Hn.component("lightboxPanel", Tr), Hn.component("slider", Fr), Hn.component("sliderParallax", Wr), Hn.component("slideshow", Jr), Hn.component("slideshowParallax", Wr), Ur = (Yr = Hn).connect, Xr = Yr.disconnect, "MutationObserver" in window && (document.body ? Kr() : new MutationObserver(function () {
    document.body && (this.disconnect(), Kr());
  }).observe(document, { childList: !0, subtree: !0 })), Hn;
});;
;
/*! UIkit 3.1.4 | http://www.getuikit.com | (c) 2014 - 2018 YOOtheme | MIT License */

!function (a, s) {
  "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = s() : "function" == typeof define && define.amd ? define("uikiticons", s) : (a = a || self).UIkitIcons = s();
}(undefined, function () {
  "use strict";
  function s(a) {
    s.installed || a.icon.add({ "arrow-down": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12.88 21.73"><path d="M12.43 14a1.52 1.52 0 0 0-2.14 0l-2.36 2.45L8 1.52a1.51 1.51 0 1 0-3 0L4.9 16.4 2.59 14a1.52 1.52 0 0 0-2.14 0 1.51 1.51 0 0 0 0 2.14l4.63 4.8a.16.16 0 0 0 0 .07 1.48 1.48 0 0 0 .22.29 1.75 1.75 0 0 0 .48.32 1.51 1.51 0 0 0 .53.1h.05a.78.78 0 0 0 .22 0 .86.86 0 0 0 .17 0 1.44 1.44 0 0 0 .71-.37 1.54 1.54 0 0 0 .27-.36l4.63-4.8a1.51 1.51 0 0 0 .07-2.19z" data-name="Layer 2"/></svg>', "arrow-left": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.73 12.88"><path d="M20.22 5L5.33 4.9l2.4-2.31a1.52 1.52 0 0 0 0-2.14A1.51 1.51 0 0 0 5.6.44L.8 5.07H.73a1.48 1.48 0 0 0-.29.22 1.75 1.75 0 0 0-.32.48 1.51 1.51 0 0 0-.1.53H0a.78.78 0 0 0 0 .22.86.86 0 0 0 0 .17 1.44 1.44 0 0 0 .37.71 1.54 1.54 0 0 0 .36.27l4.8 4.63a1.51 1.51 0 1 0 2.13-2.15L5.28 7.93 20.21 8a1.51 1.51 0 1 0 0-3z" data-name="Layer 2"/></svg>', "arrow-right": '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 21.7 12.9" xml:space="preserve"><path d="M1.7,7.7l14.9,0.1l-2.4,2.3c-0.6,0.6-0.6,1.5,0,2.1c0.6,0.6,1.5,0.6,2.1,0l4.8-4.6h0.1c0.1-0.1,0.2-0.1,0.3-0.2 c0.1-0.1,0.2-0.3,0.3-0.5c0.1-0.2,0.1-0.3,0.1-0.5h0c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c-0.1-0.3-0.2-0.5-0.4-0.7 c-0.1-0.1-0.2-0.2-0.4-0.3l-4.8-4.6c-0.6-0.6-1.6-0.6-2.1,0s-0.6,1.6,0,2.1l2.4,2.2L1.7,4.7C0.8,4.6,0.1,5.2,0,6.1s0.5,1.6,1.3,1.7 C1.4,7.8,1.6,7.8,1.7,7.7L1.7,7.7z"/></svg>', "brand-icon": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44.11 45.67"><g data-name="Layer 2"><path class="cls-1" d="M3 24.56A3 3 0 0 1 1.52 19L34.89.38a3 3 0 1 1 2.89 5.17L4.41 24.19a3 3 0 0 1-1.41.37zM15 35.12a3 3 0 0 1-1.45-5.55l24-13.4a3 3 0 0 1 2.89 5.18l-24 13.39a2.9 2.9 0 0 1-1.44.38zM27.07 45.67a3 3 0 0 1-1.45-5.55l14.08-7.86a3 3 0 0 1 2.89 5.17L28.51 45.3a3 3 0 0 1-1.44.37z" data-name="desktop - grid"/></g></svg>', close: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29.85 29.85"><g data-name="Layer 2"><g data-name="desktop - grid"><rect class="cls-1" x="-4.9" y="11.81" width="39.64" height="6.22" rx="3.11" transform="rotate(45 14.92 14.92)"/><rect class="cls-1" x="-4.9" y="11.81" width="39.64" height="6.22" rx="3.11" transform="rotate(-45 14.92 14.92)"/></g></g></svg>', facebook: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9.86 18.99"><g data-name="Layer 2"><path d="M9.86.14v3H8.07a1.7 1.7 0 0 0-1.33.41 1.94 1.94 0 0 0-.34 1.24V7h3.35l-.45 3.33H6.4V19H2.91v-8.67H0V7h2.91V4.46A4.42 4.42 0 0 1 4.1 1.17 4.3 4.3 0 0 1 7.26 0a19 19 0 0 1 2.6.14z" fill="#36434b" data-name="desktop - grid"/></g></svg>', "form-select-chevron-down": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.69 10.29"><g data-name="Layer 2"><path d="M7.34,10.29A2.13,2.13,0,0,1,5.6,9.38L.18,1.57A1,1,0,0,1,1.82.43L7.24,8.24a.26.26,0,0,0,.21,0L12.87.43a1,1,0,0,1,1.64,1.14L9.09,9.38A2.13,2.13,0,0,1,7.34,10.29Z" fill="#c6c6c6" data-name="desktop - grid"/></g></svg>', instagram: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.51 18.51"><g data-name="Layer 2"><path d="M18.51 2.37v13.76a2.39 2.39 0 0 1-2.38 2.38H2.37a2.25 2.25 0 0 1-1.67-.7 2.27 2.27 0 0 1-.7-1.68V2.37A2.25 2.25 0 0 1 .7.7 2.25 2.25 0 0 1 2.37 0h13.76a2.27 2.27 0 0 1 1.68.7 2.25 2.25 0 0 1 .7 1.67zm-2.1 13.27V7.83h-1.63A4.93 4.93 0 0 1 15 9.41a5.36 5.36 0 0 1-.77 2.8 5.68 5.68 0 0 1-2.1 2 5.79 5.79 0 0 1-2.86.79 5.6 5.6 0 0 1-4.06-1.63 5.29 5.29 0 0 1-1.69-3.96 5.22 5.22 0 0 1 .24-1.58h-1.7v7.81a.73.73 0 0 0 .74.74h12.88a.73.73 0 0 0 .73-.74zm-4.51-3.87A3.43 3.43 0 0 0 13 9.22a3.4 3.4 0 0 0-1.1-2.55 3.67 3.67 0 0 0-2.63-1 3.63 3.63 0 0 0-2.62 1 3.4 3.4 0 0 0-1.09 2.55 3.43 3.43 0 0 0 1.09 2.55 3.63 3.63 0 0 0 2.62 1.05 3.67 3.67 0 0 0 2.63-1.05zm4.51-6.89v-2a.8.8 0 0 0-.24-.58.78.78 0 0 0-.59-.25h-2.1a.78.78 0 0 0-.59.25.8.8 0 0 0-.24.58v2a.82.82 0 0 0 .83.83h2.1a.82.82 0 0 0 .83-.83z" fill="#36434b" data-name="desktop - grid"/></g></svg>', location: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32.49 43.5"><path d="M28.11 39.59a.67.67 0 0 1-.6-.67L27.3 1.57a1.4 1.4 0 1 0-2.78-.1v.43L.42 11.73A.67.67 0 0 0 .43 13l22.84 9a.7.7 0 0 1 .43.64l-.26 16.25a.69.69 0 0 1-.62.66c-2.82.27-4.85 1-4.85 1.9 0 1.13 3.24 2.06 7.25 2.08s7.26-.89 7.27-2c0-.89-1.8-1.62-4.38-1.94z"/></svg>', "logo-mini-2": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 126.08 80.69"><g data-name="Layer 2"><g data-name="desktop - grid"><path d="M86.54 79.13a39.66 39.66 0 0 1-14-2.55 4.27 4.27 0 0 1 3-8 31 31 0 1 0-4.14-56.11A4.27 4.27 0 1 1 67.24 5a39.56 39.56 0 1 1 19.3 74.12zM4.27 50.29a4.27 4.27 0 0 1-2.08-8l48.07-26.84a4.27 4.27 0 1 1 4.16 7.45L6.35 49.75a4.33 4.33 0 0 1-2.08.54zM21.63 65.49a4.27 4.27 0 0 1-2.09-8L54.1 38.2a4.27 4.27 0 0 1 4.16 7.45L23.71 65a4.25 4.25 0 0 1-2.08.49zM39 80.69a4.26 4.26 0 0 1-2.08-8l20.26-11.32a4.27 4.27 0 1 1 4.16 7.46L41.06 80.15a4.22 4.22 0 0 1-2.06.54z"/><path d="M90.19 66.05c.4-.68-3.76-.51-4.21-.43.58.13.73.34.33.52l-.55.11-.84.09.27.13a26 26 0 0 0 4.24-.16l.75-.25m2.55-8.6a4.59 4.59 0 0 0 4.78-2.62c1.32-3.2-2.12-5.24-4.92-3.95 2.67 1.52 1.51 5-1 6.14a3.57 3.57 0 0 0 1.15.42zm3.7-10.16a4 4 0 0 0 4.7-2.9c1-3.38-2.3-6.32-5.31-4.67 2.62 1.63 2.26 5.69-.38 7.15a3.36 3.36 0 0 0 1 .41zm3.13-11.23a3 3 0 0 0 4.11-2.43c.52-3-2.87-6.44-5.65-4.48 2.07 1.38 2.91 4.93.7 6.51a4.06 4.06 0 0 0 .85.39zm1.89-9.91c1.71.78 3.08.18 3.09-1.26 0-2.58-4.65-5.69-5.8-3.39 1.3 1 2.71 2.87 1.91 4.21a4.78 4.78 0 0 0 .81.43zm-10-3.62c2 .61 3.93-.16 4.24-1.61.5-2.38-3.43-4.18-5.78-3.2 2.59.74 4.14 3.62 1.54 4.8zm-7.09-7.92c1.46.12 4.14-.3 3.85-.73s-3.66-.19-4.08-.11c1.52 0 1.63.49-.16.83zm17.23 40.31a6.06 6.06 0 0 1-1.7 1.93c.23 2.74 4 1.22 5.33-1.29 1.51-2.89-1-4.08-3.22-2.49a3.37 3.37 0 0 1-.41 1.84zm2.32-6.87c1.11 2.71 4.08 1.22 4.86-1.44.86-3-1.31-5.78-3.7-3.52a5 5 0 0 1-1.16 4.95zm2.71-10.28c1.14 2.39 3.7 2.48 4-.6.29-3.54-2.85-6.36-4.12-3.4a5.19 5.19 0 0 1 .12 3.99zm0-11.54a6.15 6.15 0 0 0 2.25 4.09c1 .82 1.51.34 1.24-1a12.21 12.21 0 0 0-2-3.66c-.91-.89-1.59-.67-1.44.56zm-5-6.58c1.15.69 2.31 1 1.79.34l-.39-.37s-4.54-3.44-4.28-2.51c.12.55 1.42 1.75 2.89 2.53zm10.66 16.54c-.68-.44-.75 4.74.08 5.7.24.27.3-.29.34-.72a26 26 0 0 0-.16-4.24c-.13-.09-.03-.59-.26-.74zm-.28 8.2c-1.22-.68-3 5.87-1.69 5.78.74-.05 2.26-5.25 1.69-5.78zm-2.7 8.13c-1.52.28-4.19 5.66-2.89 4.66l.37-.38s3.51-4.68 2.49-4.28zm-8.6 9.39c2.34-2.39-2.7-1.74-4.62.77-1.66 2.13 2.01 1.44 4.59-.77zM93.27 16.2c3.28 1 4.78-.21.43-1.48s-3.7.58-.43 1.48zm-3.4 46.43a4.11 4.11 0 0 1-2 1.34c1.75.9 4.79.08 5.69-1.28 1.38-2.09-1.94-3.4-4.48-2.48a1.6 1.6 0 0 1 .79 2.42z" fill-rule="evenodd" opacity=".6"/></g></g></svg>', "logo-mini": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 97.73 62.55"><g data-name="Layer 2"><path class="cls-1" d="M67.08 61.33a30.66 30.66 0 0 1-10.82-2 3.31 3.31 0 1 1 2.33-6.19 24.05 24.05 0 1 0-3.25-43.48 3.3 3.3 0 1 1-3.22-5.77 30.67 30.67 0 1 1 15 57.44zM3.31 39a3.31 3.31 0 0 1-1.61-6.2L39 12a3.3 3.3 0 1 1 3.22 5.77L4.92 38.56a3.34 3.34 0 0 1-1.61.44zM16.76 50.76a3.3 3.3 0 0 1-1.61-6.19l26.78-15a3.31 3.31 0 0 1 3.23 5.78l-26.79 15a3.23 3.23 0 0 1-1.61.41zM30.22 62.55a3.31 3.31 0 0 1-1.62-6.2l15.72-8.78a3.31 3.31 0 0 1 3.23 5.78l-15.72 8.78a3.36 3.36 0 0 1-1.61.42z" data-name="desktop - grid"/></g></svg>', mail: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.79 30.57"><g data-name="Layer 2"><g data-name="desktop - grid"><path class="cls-1" d="M34.48 23.34a.55.55 0 0 1-.38-.15l-7.84-7.35a.56.56 0 0 1 0-.79.54.54 0 0 1 .4-.17.54.54 0 0 1 .34.12l7.85 7.35a.56.56 0 0 1 .17.39.59.59 0 0 1-.15.4.57.57 0 0 1-.39.2zM8.31 23.34a.57.57 0 0 1-.41-.17.57.57 0 0 1 0-.79L15.77 15a.56.56 0 0 1 .39-.15.52.52 0 0 1 .39.17.55.55 0 0 1 0 .79l-7.86 7.38a.54.54 0 0 1-.38.15z"/><path class="cls-1" d="M3.67 30.18A3.28 3.28 0 0 1 .39 26.9V3.67A3.28 3.28 0 0 1 3.67.39h35.45a3.28 3.28 0 0 1 3.28 3.28V26.9a3.28 3.28 0 0 1-3.28 3.28zm0-28.68A2.18 2.18 0 0 0 1.5 3.67V26.9a2.17 2.17 0 0 0 2.17 2.17h35.45a2.17 2.17 0 0 0 2.17-2.17V3.67a2.18 2.18 0 0 0-2.17-2.17z"/><path class="cls-1" d="M21.4 18.59a3.2 3.2 0 0 1-2.12-.76L4 4.45a.46.46 0 0 1-.17-.32.51.51 0 0 1 .12-.36.48.48 0 0 1 .37-.17.47.47 0 0 1 .32.12l15.27 13.37a2.37 2.37 0 0 0 3 0L38.18 3.74a.47.47 0 0 1 .32-.12.5.5 0 0 1 .37.17.47.47 0 0 1 0 .68L23.52 17.82a3.25 3.25 0 0 1-2.12.77z"/></g></g></svg>', menu: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39.64 27.28"><g data-name="Layer 2"><g data-name="desktop - grid"><rect class="cls-1" width="39.64" height="6.22" rx="3.11"/><rect class="cls-1" y="10.15" width="39.64" height="6.22" rx="3.11"/><rect class="cls-1" y="21.06" width="39.64" height="6.22" rx="3.11"/></g></g></svg>', phone: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 38.89 38.89"><path d="M37 9.61l-7.72-7.73A6.42 6.42 0 0 0 24.73 0a6.4 6.4 0 0 0-4.55 1.88l-18.3 18.3a6.44 6.44 0 0 0 0 9.1L9.61 37a6.45 6.45 0 0 0 9.1 0L32.2 23.53a1 1 0 0 0-1.38-1.37L19.63 33.35 7.19 20.9a1 1 0 0 0-1.37 0 1 1 0 0 0 0 1.37l12.45 12.45-.93.92a4.49 4.49 0 0 1-6.36 0l-7.73-7.73a4.49 4.49 0 0 1 0-6.36L19.78 5l14.77 14.79.08.07a1 1 0 0 0 1.22 0L37 18.71a6.44 6.44 0 0 0 0-9.1zm0 4.55a4.47 4.47 0 0 1-1.32 3.18l-.4.4L21.15 3.65l.4-.4a4.49 4.49 0 0 1 6.36 0L35.64 11A4.47 4.47 0 0 1 37 14.16z"/></svg>', search: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.28 31.45"><g data-name="Layer 2"><path class="cls-1" d="M30.24 27.3l-.13-.13L27 30.28l.07.07.83.83c.52.53 1.65.26 2.51-.6s1.12-2 .59-2.51l-.77-.77zM12.11 0a12.11 12.11 0 1 0 6.7 22.2l6.72 6.62 3.11-3.11L22 19.14A12.1 12.1 0 0 0 12.11 0zm.17 20.63a8.52 8.52 0 1 1 8.51-8.52 8.51 8.51 0 0 1-8.51 8.52z" data-name="desktop - grid"/></g></svg>', twitter: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.42 17.39"><g data-name="Layer 2"><path d="M21.42 2.07a9.27 9.27 0 0 1-2.2 2.26v.58a12.25 12.25 0 0 1-.52 3.52 12.63 12.63 0 0 1-1.57 3.38 13.42 13.42 0 0 1-2.5 2.86 11.07 11.07 0 0 1-3.51 2 12.69 12.69 0 0 1-4.39.74 12.13 12.13 0 0 1-6.74-2 8.55 8.55 0 0 0 1.06.06 8.61 8.61 0 0 0 5.46-1.87A4.36 4.36 0 0 1 4 12.73a4.3 4.3 0 0 1-1.54-2.17 6.19 6.19 0 0 0 .82.07 4.56 4.56 0 0 0 1.16-.15A4.31 4.31 0 0 1 1.87 9a4.21 4.21 0 0 1-1-2.79 4.33 4.33 0 0 0 2 .55 4.44 4.44 0 0 1-1.44-1.65A4.19 4.19 0 0 1 .9 3 4.39 4.39 0 0 1 1.49.8 12.41 12.41 0 0 0 5.5 4a12.2 12.2 0 0 0 5 1.35 4.92 4.92 0 0 1-.1-1 4.23 4.23 0 0 1 1.28-3.11A4.26 4.26 0 0 1 14.83 0 4.21 4.21 0 0 1 18 1.39 8.75 8.75 0 0 0 20.82.33a4.32 4.32 0 0 1-1.93 2.42 9.25 9.25 0 0 0 2.53-.68z" fill="#36434b" data-name="desktop - grid"/></g></svg>', video: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.47 31.81"><path d="M26.3 13.61L4.76.46a3.24 3.24 0 0 0-4 .54 2.14 2.14 0 0 0-.54 1.26L0 16.24a2.66 2.66 0 0 0 2.83 2.44 3 3 0 0 0 2.53-1.21 2.12 2.12 0 0 0 .38-1.17l.14-9.19L20 15.76 1.48 27.46a2.17 2.17 0 0 0-.58 3.38 3.12 3.12 0 0 0 2.69.94 3.07 3.07 0 0 0 1.33-.46L26.21 17.9a2.36 2.36 0 0 0 1.12-1.6 2.21 2.21 0 0 0-1.03-2.69z"/></svg>', youtube: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.35 17.39"><g data-name="Layer 2"><path d="M24.34 6.66v4c0 .45-.05 1.1-.12 1.85a16.08 16.08 0 0 1-.3 2 3.24 3.24 0 0 1-.92 1.76 2.85 2.85 0 0 1-1.69.78 88 88 0 0 1-9.11.34 88 88 0 0 1-9.12-.34 2.85 2.85 0 0 1-1.69-.78 3.18 3.18 0 0 1-1-1.68 16.21 16.21 0 0 1-.29-2c-.04-.75-.1-1.37-.1-1.85v-2-2c0-.45 0-1.11.12-1.86a16.68 16.68 0 0 1 .3-2 3.21 3.21 0 0 1 .94-1.67 2.87 2.87 0 0 1 1.7-.87A88 88 0 0 1 12.18 0a88 88 0 0 1 9.11.34 2.92 2.92 0 0 1 1.7.79 3.14 3.14 0 0 1 .94 1.67 16.8 16.8 0 0 1 .29 2q.11 1.13.12 1.86zm-6.95 2A.79.79 0 0 0 17 8l-7-4.39a.87.87 0 0 0-1.33.74v8.7a.82.82 0 0 0 .45.76.94.94 0 0 0 .42.11.78.78 0 0 0 .46-.14l7-4.35a.77.77 0 0 0 .39-.73z" fill="#36434b" data-name="desktop - grid"/></g></svg>' });
  }return "undefined" != typeof window && window.UIkit && window.UIkit.use(s), s;
});;
;
/**
 * Swiper 4.5.0
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 * http://www.idangero.us/swiper/
 *
 * Copyright 2014-2019 Vladimir Kharlampidi
 *
 * Released under the MIT License
 *
 * Released on: February 22, 2019
 */
!function (e, t) {
  "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : (e = e || self).Swiper = t();
}(undefined, function () {
  "use strict";
  var f = "undefined" == typeof document ? { body: {}, addEventListener: function addEventListener() {}, removeEventListener: function removeEventListener() {}, activeElement: { blur: function blur() {}, nodeName: "" }, querySelector: function querySelector() {
      return null;
    }, querySelectorAll: function querySelectorAll() {
      return [];
    }, getElementById: function getElementById() {
      return null;
    }, createEvent: function createEvent() {
      return { initEvent: function initEvent() {} };
    }, createElement: function createElement() {
      return { children: [], childNodes: [], style: {}, setAttribute: function setAttribute() {}, getElementsByTagName: function getElementsByTagName() {
          return [];
        } };
    }, location: { hash: "" } } : document,
      J = "undefined" == typeof window ? { document: f, navigator: { userAgent: "" }, location: {}, history: {}, CustomEvent: function CustomEvent() {
      return this;
    }, addEventListener: function addEventListener() {}, removeEventListener: function removeEventListener() {}, getComputedStyle: function getComputedStyle() {
      return { getPropertyValue: function getPropertyValue() {
          return "";
        } };
    }, Image: function Image() {}, Date: function Date() {}, screen: {}, setTimeout: function setTimeout() {}, clearTimeout: function clearTimeout() {} } : window,
      l = function l(e) {
    for (var t = 0; t < e.length; t += 1) {
      this[t] = e[t];
    }return this.length = e.length, this;
  };function L(e, t) {
    var a = [],
        i = 0;if (e && !t && e instanceof l) return e;if (e) if ("string" == typeof e) {
      var s,
          r,
          n = e.trim();if (0 <= n.indexOf("<") && 0 <= n.indexOf(">")) {
        var o = "div";for (0 === n.indexOf("<li") && (o = "ul"), 0 === n.indexOf("<tr") && (o = "tbody"), 0 !== n.indexOf("<td") && 0 !== n.indexOf("<th") || (o = "tr"), 0 === n.indexOf("<tbody") && (o = "table"), 0 === n.indexOf("<option") && (o = "select"), (r = f.createElement(o)).innerHTML = n, i = 0; i < r.childNodes.length; i += 1) {
          a.push(r.childNodes[i]);
        }
      } else for (s = t || "#" !== e[0] || e.match(/[ .<>:~]/) ? (t || f).querySelectorAll(e.trim()) : [f.getElementById(e.trim().split("#")[1])], i = 0; i < s.length; i += 1) {
        s[i] && a.push(s[i]);
      }
    } else if (e.nodeType || e === J || e === f) a.push(e);else if (0 < e.length && e[0].nodeType) for (i = 0; i < e.length; i += 1) {
      a.push(e[i]);
    }return new l(a);
  }function r(e) {
    for (var t = [], a = 0; a < e.length; a += 1) {
      -1 === t.indexOf(e[a]) && t.push(e[a]);
    }return t;
  }L.fn = l.prototype, L.Class = l, L.Dom7 = l;var t = { addClass: function addClass(e) {
      if (void 0 === e) return this;for (var t = e.split(" "), a = 0; a < t.length; a += 1) {
        for (var i = 0; i < this.length; i += 1) {
          void 0 !== this[i] && void 0 !== this[i].classList && this[i].classList.add(t[a]);
        }
      }return this;
    }, removeClass: function removeClass(e) {
      for (var t = e.split(" "), a = 0; a < t.length; a += 1) {
        for (var i = 0; i < this.length; i += 1) {
          void 0 !== this[i] && void 0 !== this[i].classList && this[i].classList.remove(t[a]);
        }
      }return this;
    }, hasClass: function hasClass(e) {
      return !!this[0] && this[0].classList.contains(e);
    }, toggleClass: function toggleClass(e) {
      for (var t = e.split(" "), a = 0; a < t.length; a += 1) {
        for (var i = 0; i < this.length; i += 1) {
          void 0 !== this[i] && void 0 !== this[i].classList && this[i].classList.toggle(t[a]);
        }
      }return this;
    }, attr: function attr(e, t) {
      var a = arguments;if (1 === arguments.length && "string" == typeof e) return this[0] ? this[0].getAttribute(e) : void 0;for (var i = 0; i < this.length; i += 1) {
        if (2 === a.length) this[i].setAttribute(e, t);else for (var s in e) {
          this[i][s] = e[s], this[i].setAttribute(s, e[s]);
        }
      }return this;
    }, removeAttr: function removeAttr(e) {
      for (var t = 0; t < this.length; t += 1) {
        this[t].removeAttribute(e);
      }return this;
    }, data: function data(e, t) {
      var a;if (void 0 !== t) {
        for (var i = 0; i < this.length; i += 1) {
          (a = this[i]).dom7ElementDataStorage || (a.dom7ElementDataStorage = {}), a.dom7ElementDataStorage[e] = t;
        }return this;
      }if (a = this[0]) {
        if (a.dom7ElementDataStorage && e in a.dom7ElementDataStorage) return a.dom7ElementDataStorage[e];var s = a.getAttribute("data-" + e);return s || void 0;
      }
    }, transform: function transform(e) {
      for (var t = 0; t < this.length; t += 1) {
        var a = this[t].style;a.webkitTransform = e, a.transform = e;
      }return this;
    }, transition: function transition(e) {
      "string" != typeof e && (e += "ms");for (var t = 0; t < this.length; t += 1) {
        var a = this[t].style;a.webkitTransitionDuration = e, a.transitionDuration = e;
      }return this;
    }, on: function on() {
      for (var e, t = [], a = arguments.length; a--;) {
        t[a] = arguments[a];
      }var i = t[0],
          r = t[1],
          n = t[2],
          s = t[3];function o(e) {
        var t = e.target;if (t) {
          var a = e.target.dom7EventData || [];if (a.indexOf(e) < 0 && a.unshift(e), L(t).is(r)) n.apply(t, a);else for (var i = L(t).parents(), s = 0; s < i.length; s += 1) {
            L(i[s]).is(r) && n.apply(i[s], a);
          }
        }
      }function l(e) {
        var t = e && e.target && e.target.dom7EventData || [];t.indexOf(e) < 0 && t.unshift(e), n.apply(this, t);
      }"function" == typeof t[1] && (i = (e = t)[0], n = e[1], s = e[2], r = void 0), s || (s = !1);for (var d, p = i.split(" "), c = 0; c < this.length; c += 1) {
        var u = this[c];if (r) for (d = 0; d < p.length; d += 1) {
          var h = p[d];u.dom7LiveListeners || (u.dom7LiveListeners = {}), u.dom7LiveListeners[h] || (u.dom7LiveListeners[h] = []), u.dom7LiveListeners[h].push({ listener: n, proxyListener: o }), u.addEventListener(h, o, s);
        } else for (d = 0; d < p.length; d += 1) {
          var v = p[d];u.dom7Listeners || (u.dom7Listeners = {}), u.dom7Listeners[v] || (u.dom7Listeners[v] = []), u.dom7Listeners[v].push({ listener: n, proxyListener: l }), u.addEventListener(v, l, s);
        }
      }return this;
    }, off: function off() {
      for (var e, t = [], a = arguments.length; a--;) {
        t[a] = arguments[a];
      }var i = t[0],
          s = t[1],
          r = t[2],
          n = t[3];"function" == typeof t[1] && (i = (e = t)[0], r = e[1], n = e[2], s = void 0), n || (n = !1);for (var o = i.split(" "), l = 0; l < o.length; l += 1) {
        for (var d = o[l], p = 0; p < this.length; p += 1) {
          var c = this[p],
              u = void 0;if (!s && c.dom7Listeners ? u = c.dom7Listeners[d] : s && c.dom7LiveListeners && (u = c.dom7LiveListeners[d]), u && u.length) for (var h = u.length - 1; 0 <= h; h -= 1) {
            var v = u[h];r && v.listener === r ? (c.removeEventListener(d, v.proxyListener, n), u.splice(h, 1)) : r && v.listener && v.listener.dom7proxy && v.listener.dom7proxy === r ? (c.removeEventListener(d, v.proxyListener, n), u.splice(h, 1)) : r || (c.removeEventListener(d, v.proxyListener, n), u.splice(h, 1));
          }
        }
      }return this;
    }, trigger: function trigger() {
      for (var e = [], t = arguments.length; t--;) {
        e[t] = arguments[t];
      }for (var a = e[0].split(" "), i = e[1], s = 0; s < a.length; s += 1) {
        for (var r = a[s], n = 0; n < this.length; n += 1) {
          var o = this[n],
              l = void 0;try {
            l = new J.CustomEvent(r, { detail: i, bubbles: !0, cancelable: !0 });
          } catch (e) {
            (l = f.createEvent("Event")).initEvent(r, !0, !0), l.detail = i;
          }o.dom7EventData = e.filter(function (e, t) {
            return 0 < t;
          }), o.dispatchEvent(l), o.dom7EventData = [], delete o.dom7EventData;
        }
      }return this;
    }, transitionEnd: function transitionEnd(t) {
      var a,
          i = ["webkitTransitionEnd", "transitionend"],
          s = this;function r(e) {
        if (e.target === this) for (t.call(this, e), a = 0; a < i.length; a += 1) {
          s.off(i[a], r);
        }
      }if (t) for (a = 0; a < i.length; a += 1) {
        s.on(i[a], r);
      }return this;
    }, outerWidth: function outerWidth(e) {
      if (0 < this.length) {
        if (e) {
          var t = this.styles();return this[0].offsetWidth + parseFloat(t.getPropertyValue("margin-right")) + parseFloat(t.getPropertyValue("margin-left"));
        }return this[0].offsetWidth;
      }return null;
    }, outerHeight: function outerHeight(e) {
      if (0 < this.length) {
        if (e) {
          var t = this.styles();return this[0].offsetHeight + parseFloat(t.getPropertyValue("margin-top")) + parseFloat(t.getPropertyValue("margin-bottom"));
        }return this[0].offsetHeight;
      }return null;
    }, offset: function offset() {
      if (0 < this.length) {
        var e = this[0],
            t = e.getBoundingClientRect(),
            a = f.body,
            i = e.clientTop || a.clientTop || 0,
            s = e.clientLeft || a.clientLeft || 0,
            r = e === J ? J.scrollY : e.scrollTop,
            n = e === J ? J.scrollX : e.scrollLeft;return { top: t.top + r - i, left: t.left + n - s };
      }return null;
    }, css: function css(e, t) {
      var a;if (1 === arguments.length) {
        if ("string" != typeof e) {
          for (a = 0; a < this.length; a += 1) {
            for (var i in e) {
              this[a].style[i] = e[i];
            }
          }return this;
        }if (this[0]) return J.getComputedStyle(this[0], null).getPropertyValue(e);
      }if (2 === arguments.length && "string" == typeof e) {
        for (a = 0; a < this.length; a += 1) {
          this[a].style[e] = t;
        }return this;
      }return this;
    }, each: function each(e) {
      if (!e) return this;for (var t = 0; t < this.length; t += 1) {
        if (!1 === e.call(this[t], t, this[t])) return this;
      }return this;
    }, html: function html(e) {
      if (void 0 === e) return this[0] ? this[0].innerHTML : void 0;for (var t = 0; t < this.length; t += 1) {
        this[t].innerHTML = e;
      }return this;
    }, text: function text(e) {
      if (void 0 === e) return this[0] ? this[0].textContent.trim() : null;for (var t = 0; t < this.length; t += 1) {
        this[t].textContent = e;
      }return this;
    }, is: function is(e) {
      var t,
          a,
          i = this[0];if (!i || void 0 === e) return !1;if ("string" == typeof e) {
        if (i.matches) return i.matches(e);if (i.webkitMatchesSelector) return i.webkitMatchesSelector(e);if (i.msMatchesSelector) return i.msMatchesSelector(e);for (t = L(e), a = 0; a < t.length; a += 1) {
          if (t[a] === i) return !0;
        }return !1;
      }if (e === f) return i === f;if (e === J) return i === J;if (e.nodeType || e instanceof l) {
        for (t = e.nodeType ? [e] : e, a = 0; a < t.length; a += 1) {
          if (t[a] === i) return !0;
        }return !1;
      }return !1;
    }, index: function index() {
      var e,
          t = this[0];if (t) {
        for (e = 0; null !== (t = t.previousSibling);) {
          1 === t.nodeType && (e += 1);
        }return e;
      }
    }, eq: function eq(e) {
      if (void 0 === e) return this;var t,
          a = this.length;return new l(a - 1 < e ? [] : e < 0 ? (t = a + e) < 0 ? [] : [this[t]] : [this[e]]);
    }, append: function append() {
      for (var e, t = [], a = arguments.length; a--;) {
        t[a] = arguments[a];
      }for (var i = 0; i < t.length; i += 1) {
        e = t[i];for (var s = 0; s < this.length; s += 1) {
          if ("string" == typeof e) {
            var r = f.createElement("div");for (r.innerHTML = e; r.firstChild;) {
              this[s].appendChild(r.firstChild);
            }
          } else if (e instanceof l) for (var n = 0; n < e.length; n += 1) {
            this[s].appendChild(e[n]);
          } else this[s].appendChild(e);
        }
      }return this;
    }, prepend: function prepend(e) {
      var t, a;for (t = 0; t < this.length; t += 1) {
        if ("string" == typeof e) {
          var i = f.createElement("div");for (i.innerHTML = e, a = i.childNodes.length - 1; 0 <= a; a -= 1) {
            this[t].insertBefore(i.childNodes[a], this[t].childNodes[0]);
          }
        } else if (e instanceof l) for (a = 0; a < e.length; a += 1) {
          this[t].insertBefore(e[a], this[t].childNodes[0]);
        } else this[t].insertBefore(e, this[t].childNodes[0]);
      }return this;
    }, next: function next(e) {
      return 0 < this.length ? e ? this[0].nextElementSibling && L(this[0].nextElementSibling).is(e) ? new l([this[0].nextElementSibling]) : new l([]) : this[0].nextElementSibling ? new l([this[0].nextElementSibling]) : new l([]) : new l([]);
    }, nextAll: function nextAll(e) {
      var t = [],
          a = this[0];if (!a) return new l([]);for (; a.nextElementSibling;) {
        var i = a.nextElementSibling;e ? L(i).is(e) && t.push(i) : t.push(i), a = i;
      }return new l(t);
    }, prev: function prev(e) {
      if (0 < this.length) {
        var t = this[0];return e ? t.previousElementSibling && L(t.previousElementSibling).is(e) ? new l([t.previousElementSibling]) : new l([]) : t.previousElementSibling ? new l([t.previousElementSibling]) : new l([]);
      }return new l([]);
    }, prevAll: function prevAll(e) {
      var t = [],
          a = this[0];if (!a) return new l([]);for (; a.previousElementSibling;) {
        var i = a.previousElementSibling;e ? L(i).is(e) && t.push(i) : t.push(i), a = i;
      }return new l(t);
    }, parent: function parent(e) {
      for (var t = [], a = 0; a < this.length; a += 1) {
        null !== this[a].parentNode && (e ? L(this[a].parentNode).is(e) && t.push(this[a].parentNode) : t.push(this[a].parentNode));
      }return L(r(t));
    }, parents: function parents(e) {
      for (var t = [], a = 0; a < this.length; a += 1) {
        for (var i = this[a].parentNode; i;) {
          e ? L(i).is(e) && t.push(i) : t.push(i), i = i.parentNode;
        }
      }return L(r(t));
    }, closest: function closest(e) {
      var t = this;return void 0 === e ? new l([]) : (t.is(e) || (t = t.parents(e).eq(0)), t);
    }, find: function find(e) {
      for (var t = [], a = 0; a < this.length; a += 1) {
        for (var i = this[a].querySelectorAll(e), s = 0; s < i.length; s += 1) {
          t.push(i[s]);
        }
      }return new l(t);
    }, children: function children(e) {
      for (var t = [], a = 0; a < this.length; a += 1) {
        for (var i = this[a].childNodes, s = 0; s < i.length; s += 1) {
          e ? 1 === i[s].nodeType && L(i[s]).is(e) && t.push(i[s]) : 1 === i[s].nodeType && t.push(i[s]);
        }
      }return new l(r(t));
    }, remove: function remove() {
      for (var e = 0; e < this.length; e += 1) {
        this[e].parentNode && this[e].parentNode.removeChild(this[e]);
      }return this;
    }, add: function add() {
      for (var e = [], t = arguments.length; t--;) {
        e[t] = arguments[t];
      }var a, i;for (a = 0; a < e.length; a += 1) {
        var s = L(e[a]);for (i = 0; i < s.length; i += 1) {
          this[this.length] = s[i], this.length += 1;
        }
      }return this;
    }, styles: function styles() {
      return this[0] ? J.getComputedStyle(this[0], null) : {};
    } };Object.keys(t).forEach(function (e) {
    L.fn[e] = t[e];
  });var e,
      a,
      i,
      s,
      ee = { deleteProps: function deleteProps(e) {
      var t = e;Object.keys(t).forEach(function (e) {
        try {
          t[e] = null;
        } catch (e) {}try {
          delete t[e];
        } catch (e) {}
      });
    }, nextTick: function nextTick(e, t) {
      return void 0 === t && (t = 0), setTimeout(e, t);
    }, now: function now() {
      return Date.now();
    }, getTranslate: function getTranslate(e, t) {
      var a, i, s;void 0 === t && (t = "x");var r = J.getComputedStyle(e, null);return J.WebKitCSSMatrix ? (6 < (i = r.transform || r.webkitTransform).split(",").length && (i = i.split(", ").map(function (e) {
        return e.replace(",", ".");
      }).join(", ")), s = new J.WebKitCSSMatrix("none" === i ? "" : i)) : a = (s = r.MozTransform || r.OTransform || r.MsTransform || r.msTransform || r.transform || r.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,")).toString().split(","), "x" === t && (i = J.WebKitCSSMatrix ? s.m41 : 16 === a.length ? parseFloat(a[12]) : parseFloat(a[4])), "y" === t && (i = J.WebKitCSSMatrix ? s.m42 : 16 === a.length ? parseFloat(a[13]) : parseFloat(a[5])), i || 0;
    }, parseUrlQuery: function parseUrlQuery(e) {
      var t,
          a,
          i,
          s,
          r = {},
          n = e || J.location.href;if ("string" == typeof n && n.length) for (s = (a = (n = -1 < n.indexOf("?") ? n.replace(/\S*\?/, "") : "").split("&").filter(function (e) {
        return "" !== e;
      })).length, t = 0; t < s; t += 1) {
        i = a[t].replace(/#\S+/g, "").split("="), r[decodeURIComponent(i[0])] = void 0 === i[1] ? void 0 : decodeURIComponent(i[1]) || "";
      }return r;
    }, isObject: function isObject(e) {
      return "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && null !== e && e.constructor && e.constructor === Object;
    }, extend: function extend() {
      for (var e = [], t = arguments.length; t--;) {
        e[t] = arguments[t];
      }for (var a = Object(e[0]), i = 1; i < e.length; i += 1) {
        var s = e[i];if (null != s) for (var r = Object.keys(Object(s)), n = 0, o = r.length; n < o; n += 1) {
          var l = r[n],
              d = Object.getOwnPropertyDescriptor(s, l);void 0 !== d && d.enumerable && (ee.isObject(a[l]) && ee.isObject(s[l]) ? ee.extend(a[l], s[l]) : !ee.isObject(a[l]) && ee.isObject(s[l]) ? (a[l] = {}, ee.extend(a[l], s[l])) : a[l] = s[l]);
        }
      }return a;
    } },
      te = (i = f.createElement("div"), { touch: J.Modernizr && !0 === J.Modernizr.touch || !!(0 < J.navigator.maxTouchPoints || "ontouchstart" in J || J.DocumentTouch && f instanceof J.DocumentTouch), pointerEvents: !!(J.navigator.pointerEnabled || J.PointerEvent || "maxTouchPoints" in J.navigator && 0 < J.navigator.maxTouchPoints), prefixedPointerEvents: !!J.navigator.msPointerEnabled, transition: (a = i.style, "transition" in a || "webkitTransition" in a || "MozTransition" in a), transforms3d: J.Modernizr && !0 === J.Modernizr.csstransforms3d || (e = i.style, "webkitPerspective" in e || "MozPerspective" in e || "OPerspective" in e || "MsPerspective" in e || "perspective" in e), flexbox: function () {
      for (var e = i.style, t = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), a = 0; a < t.length; a += 1) {
        if (t[a] in e) return !0;
      }return !1;
    }(), observer: "MutationObserver" in J || "WebkitMutationObserver" in J, passiveListener: function () {
      var e = !1;try {
        var t = Object.defineProperty({}, "passive", { get: function get() {
            e = !0;
          } });J.addEventListener("testPassiveListener", null, t);
      } catch (e) {}return e;
    }(), gestures: "ongesturestart" in J }),
      I = { isIE: !!J.navigator.userAgent.match(/Trident/g) || !!J.navigator.userAgent.match(/MSIE/g), isEdge: !!J.navigator.userAgent.match(/Edge/g), isSafari: (s = J.navigator.userAgent.toLowerCase(), 0 <= s.indexOf("safari") && s.indexOf("chrome") < 0 && s.indexOf("android") < 0), isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(J.navigator.userAgent) },
      n = function n(e) {
    void 0 === e && (e = {});var t = this;t.params = e, t.eventsListeners = {}, t.params && t.params.on && Object.keys(t.params.on).forEach(function (e) {
      t.on(e, t.params.on[e]);
    });
  },
      o = { components: { configurable: !0 } };n.prototype.on = function (e, t, a) {
    var i = this;if ("function" != typeof t) return i;var s = a ? "unshift" : "push";return e.split(" ").forEach(function (e) {
      i.eventsListeners[e] || (i.eventsListeners[e] = []), i.eventsListeners[e][s](t);
    }), i;
  }, n.prototype.once = function (a, i, e) {
    var s = this;if ("function" != typeof i) return s;function r() {
      for (var e = [], t = arguments.length; t--;) {
        e[t] = arguments[t];
      }i.apply(s, e), s.off(a, r), r.f7proxy && delete r.f7proxy;
    }return r.f7proxy = i, s.on(a, r, e);
  }, n.prototype.off = function (e, i) {
    var s = this;return s.eventsListeners && e.split(" ").forEach(function (a) {
      void 0 === i ? s.eventsListeners[a] = [] : s.eventsListeners[a] && s.eventsListeners[a].length && s.eventsListeners[a].forEach(function (e, t) {
        (e === i || e.f7proxy && e.f7proxy === i) && s.eventsListeners[a].splice(t, 1);
      });
    }), s;
  }, n.prototype.emit = function () {
    for (var e = [], t = arguments.length; t--;) {
      e[t] = arguments[t];
    }var a,
        i,
        s,
        r = this;return r.eventsListeners && ("string" == typeof e[0] || Array.isArray(e[0]) ? (a = e[0], i = e.slice(1, e.length), s = r) : (a = e[0].events, i = e[0].data, s = e[0].context || r), (Array.isArray(a) ? a : a.split(" ")).forEach(function (e) {
      if (r.eventsListeners && r.eventsListeners[e]) {
        var t = [];r.eventsListeners[e].forEach(function (e) {
          t.push(e);
        }), t.forEach(function (e) {
          e.apply(s, i);
        });
      }
    })), r;
  }, n.prototype.useModulesParams = function (a) {
    var i = this;i.modules && Object.keys(i.modules).forEach(function (e) {
      var t = i.modules[e];t.params && ee.extend(a, t.params);
    });
  }, n.prototype.useModules = function (i) {
    void 0 === i && (i = {});var s = this;s.modules && Object.keys(s.modules).forEach(function (e) {
      var a = s.modules[e],
          t = i[e] || {};a.instance && Object.keys(a.instance).forEach(function (e) {
        var t = a.instance[e];s[e] = "function" == typeof t ? t.bind(s) : t;
      }), a.on && s.on && Object.keys(a.on).forEach(function (e) {
        s.on(e, a.on[e]);
      }), a.create && a.create.bind(s)(t);
    });
  }, o.components.set = function (e) {
    this.use && this.use(e);
  }, n.installModule = function (t) {
    for (var e = [], a = arguments.length - 1; 0 < a--;) {
      e[a] = arguments[a + 1];
    }var i = this;i.prototype.modules || (i.prototype.modules = {});var s = t.name || Object.keys(i.prototype.modules).length + "_" + ee.now();return (i.prototype.modules[s] = t).proto && Object.keys(t.proto).forEach(function (e) {
      i.prototype[e] = t.proto[e];
    }), t.static && Object.keys(t.static).forEach(function (e) {
      i[e] = t.static[e];
    }), t.install && t.install.apply(i, e), i;
  }, n.use = function (e) {
    for (var t = [], a = arguments.length - 1; 0 < a--;) {
      t[a] = arguments[a + 1];
    }var i = this;return Array.isArray(e) ? (e.forEach(function (e) {
      return i.installModule(e);
    }), i) : i.installModule.apply(i, [e].concat(t));
  }, Object.defineProperties(n, o);var d = { updateSize: function updateSize() {
      var e,
          t,
          a = this,
          i = a.$el;e = void 0 !== a.params.width ? a.params.width : i[0].clientWidth, t = void 0 !== a.params.height ? a.params.height : i[0].clientHeight, 0 === e && a.isHorizontal() || 0 === t && a.isVertical() || (e = e - parseInt(i.css("padding-left"), 10) - parseInt(i.css("padding-right"), 10), t = t - parseInt(i.css("padding-top"), 10) - parseInt(i.css("padding-bottom"), 10), ee.extend(a, { width: e, height: t, size: a.isHorizontal() ? e : t }));
    }, updateSlides: function updateSlides() {
      var e = this,
          t = e.params,
          a = e.$wrapperEl,
          i = e.size,
          s = e.rtlTranslate,
          r = e.wrongRTL,
          n = e.virtual && t.virtual.enabled,
          o = n ? e.virtual.slides.length : e.slides.length,
          l = a.children("." + e.params.slideClass),
          d = n ? e.virtual.slides.length : l.length,
          p = [],
          c = [],
          u = [],
          h = t.slidesOffsetBefore;"function" == typeof h && (h = t.slidesOffsetBefore.call(e));var v = t.slidesOffsetAfter;"function" == typeof v && (v = t.slidesOffsetAfter.call(e));var f = e.snapGrid.length,
          m = e.snapGrid.length,
          g = t.spaceBetween,
          b = -h,
          w = 0,
          y = 0;if (void 0 !== i) {
        var x, T;"string" == typeof g && 0 <= g.indexOf("%") && (g = parseFloat(g.replace("%", "")) / 100 * i), e.virtualSize = -g, s ? l.css({ marginLeft: "", marginTop: "" }) : l.css({ marginRight: "", marginBottom: "" }), 1 < t.slidesPerColumn && (x = Math.floor(d / t.slidesPerColumn) === d / e.params.slidesPerColumn ? d : Math.ceil(d / t.slidesPerColumn) * t.slidesPerColumn, "auto" !== t.slidesPerView && "row" === t.slidesPerColumnFill && (x = Math.max(x, t.slidesPerView * t.slidesPerColumn)));for (var E, S = t.slidesPerColumn, C = x / S, M = Math.floor(d / t.slidesPerColumn), z = 0; z < d; z += 1) {
          T = 0;var P = l.eq(z);if (1 < t.slidesPerColumn) {
            var k = void 0,
                $ = void 0,
                L = void 0;"column" === t.slidesPerColumnFill ? (L = z - ($ = Math.floor(z / S)) * S, (M < $ || $ === M && L === S - 1) && S <= (L += 1) && (L = 0, $ += 1), k = $ + L * x / S, P.css({ "-webkit-box-ordinal-group": k, "-moz-box-ordinal-group": k, "-ms-flex-order": k, "-webkit-order": k, order: k })) : $ = z - (L = Math.floor(z / C)) * C, P.css("margin-" + (e.isHorizontal() ? "top" : "left"), 0 !== L && t.spaceBetween && t.spaceBetween + "px").attr("data-swiper-column", $).attr("data-swiper-row", L);
          }if ("none" !== P.css("display")) {
            if ("auto" === t.slidesPerView) {
              var I = J.getComputedStyle(P[0], null),
                  D = P[0].style.transform,
                  O = P[0].style.webkitTransform;if (D && (P[0].style.transform = "none"), O && (P[0].style.webkitTransform = "none"), t.roundLengths) T = e.isHorizontal() ? P.outerWidth(!0) : P.outerHeight(!0);else if (e.isHorizontal()) {
                var A = parseFloat(I.getPropertyValue("width")),
                    H = parseFloat(I.getPropertyValue("padding-left")),
                    N = parseFloat(I.getPropertyValue("padding-right")),
                    G = parseFloat(I.getPropertyValue("margin-left")),
                    B = parseFloat(I.getPropertyValue("margin-right")),
                    X = I.getPropertyValue("box-sizing");T = X && "border-box" === X ? A + G + B : A + H + N + G + B;
              } else {
                var Y = parseFloat(I.getPropertyValue("height")),
                    V = parseFloat(I.getPropertyValue("padding-top")),
                    F = parseFloat(I.getPropertyValue("padding-bottom")),
                    R = parseFloat(I.getPropertyValue("margin-top")),
                    q = parseFloat(I.getPropertyValue("margin-bottom")),
                    W = I.getPropertyValue("box-sizing");T = W && "border-box" === W ? Y + R + q : Y + V + F + R + q;
              }D && (P[0].style.transform = D), O && (P[0].style.webkitTransform = O), t.roundLengths && (T = Math.floor(T));
            } else T = (i - (t.slidesPerView - 1) * g) / t.slidesPerView, t.roundLengths && (T = Math.floor(T)), l[z] && (e.isHorizontal() ? l[z].style.width = T + "px" : l[z].style.height = T + "px");l[z] && (l[z].swiperSlideSize = T), u.push(T), t.centeredSlides ? (b = b + T / 2 + w / 2 + g, 0 === w && 0 !== z && (b = b - i / 2 - g), 0 === z && (b = b - i / 2 - g), Math.abs(b) < .001 && (b = 0), t.roundLengths && (b = Math.floor(b)), y % t.slidesPerGroup == 0 && p.push(b), c.push(b)) : (t.roundLengths && (b = Math.floor(b)), y % t.slidesPerGroup == 0 && p.push(b), c.push(b), b = b + T + g), e.virtualSize += T + g, w = T, y += 1;
          }
        }if (e.virtualSize = Math.max(e.virtualSize, i) + v, s && r && ("slide" === t.effect || "coverflow" === t.effect) && a.css({ width: e.virtualSize + t.spaceBetween + "px" }), te.flexbox && !t.setWrapperSize || (e.isHorizontal() ? a.css({ width: e.virtualSize + t.spaceBetween + "px" }) : a.css({ height: e.virtualSize + t.spaceBetween + "px" })), 1 < t.slidesPerColumn && (e.virtualSize = (T + t.spaceBetween) * x, e.virtualSize = Math.ceil(e.virtualSize / t.slidesPerColumn) - t.spaceBetween, e.isHorizontal() ? a.css({ width: e.virtualSize + t.spaceBetween + "px" }) : a.css({ height: e.virtualSize + t.spaceBetween + "px" }), t.centeredSlides)) {
          E = [];for (var j = 0; j < p.length; j += 1) {
            var U = p[j];t.roundLengths && (U = Math.floor(U)), p[j] < e.virtualSize + p[0] && E.push(U);
          }p = E;
        }if (!t.centeredSlides) {
          E = [];for (var K = 0; K < p.length; K += 1) {
            var _ = p[K];t.roundLengths && (_ = Math.floor(_)), p[K] <= e.virtualSize - i && E.push(_);
          }p = E, 1 < Math.floor(e.virtualSize - i) - Math.floor(p[p.length - 1]) && p.push(e.virtualSize - i);
        }if (0 === p.length && (p = [0]), 0 !== t.spaceBetween && (e.isHorizontal() ? s ? l.css({ marginLeft: g + "px" }) : l.css({ marginRight: g + "px" }) : l.css({ marginBottom: g + "px" })), t.centerInsufficientSlides) {
          var Z = 0;if (u.forEach(function (e) {
            Z += e + (t.spaceBetween ? t.spaceBetween : 0);
          }), (Z -= t.spaceBetween) < i) {
            var Q = (i - Z) / 2;p.forEach(function (e, t) {
              p[t] = e - Q;
            }), c.forEach(function (e, t) {
              c[t] = e + Q;
            });
          }
        }ee.extend(e, { slides: l, snapGrid: p, slidesGrid: c, slidesSizesGrid: u }), d !== o && e.emit("slidesLengthChange"), p.length !== f && (e.params.watchOverflow && e.checkOverflow(), e.emit("snapGridLengthChange")), c.length !== m && e.emit("slidesGridLengthChange"), (t.watchSlidesProgress || t.watchSlidesVisibility) && e.updateSlidesOffset();
      }
    }, updateAutoHeight: function updateAutoHeight(e) {
      var t,
          a = this,
          i = [],
          s = 0;if ("number" == typeof e ? a.setTransition(e) : !0 === e && a.setTransition(a.params.speed), "auto" !== a.params.slidesPerView && 1 < a.params.slidesPerView) for (t = 0; t < Math.ceil(a.params.slidesPerView); t += 1) {
        var r = a.activeIndex + t;if (r > a.slides.length) break;i.push(a.slides.eq(r)[0]);
      } else i.push(a.slides.eq(a.activeIndex)[0]);for (t = 0; t < i.length; t += 1) {
        if (void 0 !== i[t]) {
          var n = i[t].offsetHeight;s = s < n ? n : s;
        }
      }s && a.$wrapperEl.css("height", s + "px");
    }, updateSlidesOffset: function updateSlidesOffset() {
      for (var e = this.slides, t = 0; t < e.length; t += 1) {
        e[t].swiperSlideOffset = this.isHorizontal() ? e[t].offsetLeft : e[t].offsetTop;
      }
    }, updateSlidesProgress: function updateSlidesProgress(e) {
      void 0 === e && (e = this && this.translate || 0);var t = this,
          a = t.params,
          i = t.slides,
          s = t.rtlTranslate;if (0 !== i.length) {
        void 0 === i[0].swiperSlideOffset && t.updateSlidesOffset();var r = -e;s && (r = e), i.removeClass(a.slideVisibleClass), t.visibleSlidesIndexes = [], t.visibleSlides = [];for (var n = 0; n < i.length; n += 1) {
          var o = i[n],
              l = (r + (a.centeredSlides ? t.minTranslate() : 0) - o.swiperSlideOffset) / (o.swiperSlideSize + a.spaceBetween);if (a.watchSlidesVisibility) {
            var d = -(r - o.swiperSlideOffset),
                p = d + t.slidesSizesGrid[n];(0 <= d && d < t.size || 0 < p && p <= t.size || d <= 0 && p >= t.size) && (t.visibleSlides.push(o), t.visibleSlidesIndexes.push(n), i.eq(n).addClass(a.slideVisibleClass));
          }o.progress = s ? -l : l;
        }t.visibleSlides = L(t.visibleSlides);
      }
    }, updateProgress: function updateProgress(e) {
      void 0 === e && (e = this && this.translate || 0);var t = this,
          a = t.params,
          i = t.maxTranslate() - t.minTranslate(),
          s = t.progress,
          r = t.isBeginning,
          n = t.isEnd,
          o = r,
          l = n;0 === i ? n = r = !(s = 0) : (r = (s = (e - t.minTranslate()) / i) <= 0, n = 1 <= s), ee.extend(t, { progress: s, isBeginning: r, isEnd: n }), (a.watchSlidesProgress || a.watchSlidesVisibility) && t.updateSlidesProgress(e), r && !o && t.emit("reachBeginning toEdge"), n && !l && t.emit("reachEnd toEdge"), (o && !r || l && !n) && t.emit("fromEdge"), t.emit("progress", s);
    }, updateSlidesClasses: function updateSlidesClasses() {
      var e,
          t = this,
          a = t.slides,
          i = t.params,
          s = t.$wrapperEl,
          r = t.activeIndex,
          n = t.realIndex,
          o = t.virtual && i.virtual.enabled;a.removeClass(i.slideActiveClass + " " + i.slideNextClass + " " + i.slidePrevClass + " " + i.slideDuplicateActiveClass + " " + i.slideDuplicateNextClass + " " + i.slideDuplicatePrevClass), (e = o ? t.$wrapperEl.find("." + i.slideClass + '[data-swiper-slide-index="' + r + '"]') : a.eq(r)).addClass(i.slideActiveClass), i.loop && (e.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + n + '"]').addClass(i.slideDuplicateActiveClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + n + '"]').addClass(i.slideDuplicateActiveClass));var l = e.nextAll("." + i.slideClass).eq(0).addClass(i.slideNextClass);i.loop && 0 === l.length && (l = a.eq(0)).addClass(i.slideNextClass);var d = e.prevAll("." + i.slideClass).eq(0).addClass(i.slidePrevClass);i.loop && 0 === d.length && (d = a.eq(-1)).addClass(i.slidePrevClass), i.loop && (l.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicateNextClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicateNextClass), d.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + d.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicatePrevClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + d.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicatePrevClass));
    }, updateActiveIndex: function updateActiveIndex(e) {
      var t,
          a = this,
          i = a.rtlTranslate ? a.translate : -a.translate,
          s = a.slidesGrid,
          r = a.snapGrid,
          n = a.params,
          o = a.activeIndex,
          l = a.realIndex,
          d = a.snapIndex,
          p = e;if (void 0 === p) {
        for (var c = 0; c < s.length; c += 1) {
          void 0 !== s[c + 1] ? i >= s[c] && i < s[c + 1] - (s[c + 1] - s[c]) / 2 ? p = c : i >= s[c] && i < s[c + 1] && (p = c + 1) : i >= s[c] && (p = c);
        }n.normalizeSlideIndex && (p < 0 || void 0 === p) && (p = 0);
      }if ((t = 0 <= r.indexOf(i) ? r.indexOf(i) : Math.floor(p / n.slidesPerGroup)) >= r.length && (t = r.length - 1), p !== o) {
        var u = parseInt(a.slides.eq(p).attr("data-swiper-slide-index") || p, 10);ee.extend(a, { snapIndex: t, realIndex: u, previousIndex: o, activeIndex: p }), a.emit("activeIndexChange"), a.emit("snapIndexChange"), l !== u && a.emit("realIndexChange"), a.emit("slideChange");
      } else t !== d && (a.snapIndex = t, a.emit("snapIndexChange"));
    }, updateClickedSlide: function updateClickedSlide(e) {
      var t = this,
          a = t.params,
          i = L(e.target).closest("." + a.slideClass)[0],
          s = !1;if (i) for (var r = 0; r < t.slides.length; r += 1) {
        t.slides[r] === i && (s = !0);
      }if (!i || !s) return t.clickedSlide = void 0, void (t.clickedIndex = void 0);t.clickedSlide = i, t.virtual && t.params.virtual.enabled ? t.clickedIndex = parseInt(L(i).attr("data-swiper-slide-index"), 10) : t.clickedIndex = L(i).index(), a.slideToClickedSlide && void 0 !== t.clickedIndex && t.clickedIndex !== t.activeIndex && t.slideToClickedSlide();
    } };var p = { getTranslate: function getTranslate(e) {
      void 0 === e && (e = this.isHorizontal() ? "x" : "y");var t = this.params,
          a = this.rtlTranslate,
          i = this.translate,
          s = this.$wrapperEl;if (t.virtualTranslate) return a ? -i : i;var r = ee.getTranslate(s[0], e);return a && (r = -r), r || 0;
    }, setTranslate: function setTranslate(e, t) {
      var a = this,
          i = a.rtlTranslate,
          s = a.params,
          r = a.$wrapperEl,
          n = a.progress,
          o = 0,
          l = 0;a.isHorizontal() ? o = i ? -e : e : l = e, s.roundLengths && (o = Math.floor(o), l = Math.floor(l)), s.virtualTranslate || (te.transforms3d ? r.transform("translate3d(" + o + "px, " + l + "px, 0px)") : r.transform("translate(" + o + "px, " + l + "px)")), a.previousTranslate = a.translate, a.translate = a.isHorizontal() ? o : l;var d = a.maxTranslate() - a.minTranslate();(0 === d ? 0 : (e - a.minTranslate()) / d) !== n && a.updateProgress(e), a.emit("setTranslate", a.translate, t);
    }, minTranslate: function minTranslate() {
      return -this.snapGrid[0];
    }, maxTranslate: function maxTranslate() {
      return -this.snapGrid[this.snapGrid.length - 1];
    } };var c = { setTransition: function setTransition(e, t) {
      this.$wrapperEl.transition(e), this.emit("setTransition", e, t);
    }, transitionStart: function transitionStart(e, t) {
      void 0 === e && (e = !0);var a = this,
          i = a.activeIndex,
          s = a.params,
          r = a.previousIndex;s.autoHeight && a.updateAutoHeight();var n = t;if (n || (n = r < i ? "next" : i < r ? "prev" : "reset"), a.emit("transitionStart"), e && i !== r) {
        if ("reset" === n) return void a.emit("slideResetTransitionStart");a.emit("slideChangeTransitionStart"), "next" === n ? a.emit("slideNextTransitionStart") : a.emit("slidePrevTransitionStart");
      }
    }, transitionEnd: function transitionEnd(e, t) {
      void 0 === e && (e = !0);var a = this,
          i = a.activeIndex,
          s = a.previousIndex;a.animating = !1, a.setTransition(0);var r = t;if (r || (r = s < i ? "next" : i < s ? "prev" : "reset"), a.emit("transitionEnd"), e && i !== s) {
        if ("reset" === r) return void a.emit("slideResetTransitionEnd");a.emit("slideChangeTransitionEnd"), "next" === r ? a.emit("slideNextTransitionEnd") : a.emit("slidePrevTransitionEnd");
      }
    } };var u = { slideTo: function slideTo(e, t, a, i) {
      void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === a && (a = !0);var s = this,
          r = e;r < 0 && (r = 0);var n = s.params,
          o = s.snapGrid,
          l = s.slidesGrid,
          d = s.previousIndex,
          p = s.activeIndex,
          c = s.rtlTranslate;if (s.animating && n.preventInteractionOnTransition) return !1;var u = Math.floor(r / n.slidesPerGroup);u >= o.length && (u = o.length - 1), (p || n.initialSlide || 0) === (d || 0) && a && s.emit("beforeSlideChangeStart");var h,
          v = -o[u];if (s.updateProgress(v), n.normalizeSlideIndex) for (var f = 0; f < l.length; f += 1) {
        -Math.floor(100 * v) >= Math.floor(100 * l[f]) && (r = f);
      }if (s.initialized && r !== p) {
        if (!s.allowSlideNext && v < s.translate && v < s.minTranslate()) return !1;if (!s.allowSlidePrev && v > s.translate && v > s.maxTranslate() && (p || 0) !== r) return !1;
      }return h = p < r ? "next" : r < p ? "prev" : "reset", c && -v === s.translate || !c && v === s.translate ? (s.updateActiveIndex(r), n.autoHeight && s.updateAutoHeight(), s.updateSlidesClasses(), "slide" !== n.effect && s.setTranslate(v), "reset" !== h && (s.transitionStart(a, h), s.transitionEnd(a, h)), !1) : (0 !== t && te.transition ? (s.setTransition(t), s.setTranslate(v), s.updateActiveIndex(r), s.updateSlidesClasses(), s.emit("beforeTransitionStart", t, i), s.transitionStart(a, h), s.animating || (s.animating = !0, s.onSlideToWrapperTransitionEnd || (s.onSlideToWrapperTransitionEnd = function (e) {
        s && !s.destroyed && e.target === this && (s.$wrapperEl[0].removeEventListener("transitionend", s.onSlideToWrapperTransitionEnd), s.$wrapperEl[0].removeEventListener("webkitTransitionEnd", s.onSlideToWrapperTransitionEnd), s.onSlideToWrapperTransitionEnd = null, delete s.onSlideToWrapperTransitionEnd, s.transitionEnd(a, h));
      }), s.$wrapperEl[0].addEventListener("transitionend", s.onSlideToWrapperTransitionEnd), s.$wrapperEl[0].addEventListener("webkitTransitionEnd", s.onSlideToWrapperTransitionEnd))) : (s.setTransition(0), s.setTranslate(v), s.updateActiveIndex(r), s.updateSlidesClasses(), s.emit("beforeTransitionStart", t, i), s.transitionStart(a, h), s.transitionEnd(a, h)), !0);
    }, slideToLoop: function slideToLoop(e, t, a, i) {
      void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === a && (a = !0);var s = e;return this.params.loop && (s += this.loopedSlides), this.slideTo(s, t, a, i);
    }, slideNext: function slideNext(e, t, a) {
      void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);var i = this,
          s = i.params,
          r = i.animating;return s.loop ? !r && (i.loopFix(), i._clientLeft = i.$wrapperEl[0].clientLeft, i.slideTo(i.activeIndex + s.slidesPerGroup, e, t, a)) : i.slideTo(i.activeIndex + s.slidesPerGroup, e, t, a);
    }, slidePrev: function slidePrev(e, t, a) {
      void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);var i = this,
          s = i.params,
          r = i.animating,
          n = i.snapGrid,
          o = i.slidesGrid,
          l = i.rtlTranslate;if (s.loop) {
        if (r) return !1;i.loopFix(), i._clientLeft = i.$wrapperEl[0].clientLeft;
      }function d(e) {
        return e < 0 ? -Math.floor(Math.abs(e)) : Math.floor(e);
      }var p,
          c = d(l ? i.translate : -i.translate),
          u = n.map(function (e) {
        return d(e);
      }),
          h = (o.map(function (e) {
        return d(e);
      }), n[u.indexOf(c)], n[u.indexOf(c) - 1]);return void 0 !== h && (p = o.indexOf(h)) < 0 && (p = i.activeIndex - 1), i.slideTo(p, e, t, a);
    }, slideReset: function slideReset(e, t, a) {
      return void 0 === e && (e = this.params.speed), void 0 === t && (t = !0), this.slideTo(this.activeIndex, e, t, a);
    }, slideToClosest: function slideToClosest(e, t, a) {
      void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);var i = this,
          s = i.activeIndex,
          r = Math.floor(s / i.params.slidesPerGroup);if (r < i.snapGrid.length - 1) {
        var n = i.rtlTranslate ? i.translate : -i.translate,
            o = i.snapGrid[r];(i.snapGrid[r + 1] - o) / 2 < n - o && (s = i.params.slidesPerGroup);
      }return i.slideTo(s, e, t, a);
    }, slideToClickedSlide: function slideToClickedSlide() {
      var e,
          t = this,
          a = t.params,
          i = t.$wrapperEl,
          s = "auto" === a.slidesPerView ? t.slidesPerViewDynamic() : a.slidesPerView,
          r = t.clickedIndex;if (a.loop) {
        if (t.animating) return;e = parseInt(L(t.clickedSlide).attr("data-swiper-slide-index"), 10), a.centeredSlides ? r < t.loopedSlides - s / 2 || r > t.slides.length - t.loopedSlides + s / 2 ? (t.loopFix(), r = i.children("." + a.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + a.slideDuplicateClass + ")").eq(0).index(), ee.nextTick(function () {
          t.slideTo(r);
        })) : t.slideTo(r) : r > t.slides.length - s ? (t.loopFix(), r = i.children("." + a.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + a.slideDuplicateClass + ")").eq(0).index(), ee.nextTick(function () {
          t.slideTo(r);
        })) : t.slideTo(r);
      } else t.slideTo(r);
    } };var h = { loopCreate: function loopCreate() {
      var i = this,
          e = i.params,
          t = i.$wrapperEl;t.children("." + e.slideClass + "." + e.slideDuplicateClass).remove();var s = t.children("." + e.slideClass);if (e.loopFillGroupWithBlank) {
        var a = e.slidesPerGroup - s.length % e.slidesPerGroup;if (a !== e.slidesPerGroup) {
          for (var r = 0; r < a; r += 1) {
            var n = L(f.createElement("div")).addClass(e.slideClass + " " + e.slideBlankClass);t.append(n);
          }s = t.children("." + e.slideClass);
        }
      }"auto" !== e.slidesPerView || e.loopedSlides || (e.loopedSlides = s.length), i.loopedSlides = parseInt(e.loopedSlides || e.slidesPerView, 10), i.loopedSlides += e.loopAdditionalSlides, i.loopedSlides > s.length && (i.loopedSlides = s.length);var o = [],
          l = [];s.each(function (e, t) {
        var a = L(t);e < i.loopedSlides && l.push(t), e < s.length && e >= s.length - i.loopedSlides && o.push(t), a.attr("data-swiper-slide-index", e);
      });for (var d = 0; d < l.length; d += 1) {
        t.append(L(l[d].cloneNode(!0)).addClass(e.slideDuplicateClass));
      }for (var p = o.length - 1; 0 <= p; p -= 1) {
        t.prepend(L(o[p].cloneNode(!0)).addClass(e.slideDuplicateClass));
      }
    }, loopFix: function loopFix() {
      var e,
          t = this,
          a = t.params,
          i = t.activeIndex,
          s = t.slides,
          r = t.loopedSlides,
          n = t.allowSlidePrev,
          o = t.allowSlideNext,
          l = t.snapGrid,
          d = t.rtlTranslate;t.allowSlidePrev = !0, t.allowSlideNext = !0;var p = -l[i] - t.getTranslate();i < r ? (e = s.length - 3 * r + i, e += r, t.slideTo(e, 0, !1, !0) && 0 !== p && t.setTranslate((d ? -t.translate : t.translate) - p)) : ("auto" === a.slidesPerView && 2 * r <= i || i >= s.length - r) && (e = -s.length + i + r, e += r, t.slideTo(e, 0, !1, !0) && 0 !== p && t.setTranslate((d ? -t.translate : t.translate) - p));t.allowSlidePrev = n, t.allowSlideNext = o;
    }, loopDestroy: function loopDestroy() {
      var e = this.$wrapperEl,
          t = this.params,
          a = this.slides;e.children("." + t.slideClass + "." + t.slideDuplicateClass + ",." + t.slideClass + "." + t.slideBlankClass).remove(), a.removeAttr("data-swiper-slide-index");
    } };var v = { setGrabCursor: function setGrabCursor(e) {
      if (!(te.touch || !this.params.simulateTouch || this.params.watchOverflow && this.isLocked)) {
        var t = this.el;t.style.cursor = "move", t.style.cursor = e ? "-webkit-grabbing" : "-webkit-grab", t.style.cursor = e ? "-moz-grabbin" : "-moz-grab", t.style.cursor = e ? "grabbing" : "grab";
      }
    }, unsetGrabCursor: function unsetGrabCursor() {
      te.touch || this.params.watchOverflow && this.isLocked || (this.el.style.cursor = "");
    } };var m = { appendSlide: function appendSlide(e) {
      var t = this,
          a = t.$wrapperEl,
          i = t.params;if (i.loop && t.loopDestroy(), "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && "length" in e) for (var s = 0; s < e.length; s += 1) {
        e[s] && a.append(e[s]);
      } else a.append(e);i.loop && t.loopCreate(), i.observer && te.observer || t.update();
    }, prependSlide: function prependSlide(e) {
      var t = this,
          a = t.params,
          i = t.$wrapperEl,
          s = t.activeIndex;a.loop && t.loopDestroy();var r = s + 1;if ("object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && "length" in e) {
        for (var n = 0; n < e.length; n += 1) {
          e[n] && i.prepend(e[n]);
        }r = s + e.length;
      } else i.prepend(e);a.loop && t.loopCreate(), a.observer && te.observer || t.update(), t.slideTo(r, 0, !1);
    }, addSlide: function addSlide(e, t) {
      var a = this,
          i = a.$wrapperEl,
          s = a.params,
          r = a.activeIndex;s.loop && (r -= a.loopedSlides, a.loopDestroy(), a.slides = i.children("." + s.slideClass));var n = a.slides.length;if (e <= 0) a.prependSlide(t);else if (n <= e) a.appendSlide(t);else {
        for (var o = e < r ? r + 1 : r, l = [], d = n - 1; e <= d; d -= 1) {
          var p = a.slides.eq(d);p.remove(), l.unshift(p);
        }if ("object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && "length" in t) {
          for (var c = 0; c < t.length; c += 1) {
            t[c] && i.append(t[c]);
          }o = e < r ? r + t.length : r;
        } else i.append(t);for (var u = 0; u < l.length; u += 1) {
          i.append(l[u]);
        }s.loop && a.loopCreate(), s.observer && te.observer || a.update(), s.loop ? a.slideTo(o + a.loopedSlides, 0, !1) : a.slideTo(o, 0, !1);
      }
    }, removeSlide: function removeSlide(e) {
      var t = this,
          a = t.params,
          i = t.$wrapperEl,
          s = t.activeIndex;a.loop && (s -= t.loopedSlides, t.loopDestroy(), t.slides = i.children("." + a.slideClass));var r,
          n = s;if ("object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && "length" in e) {
        for (var o = 0; o < e.length; o += 1) {
          r = e[o], t.slides[r] && t.slides.eq(r).remove(), r < n && (n -= 1);
        }n = Math.max(n, 0);
      } else r = e, t.slides[r] && t.slides.eq(r).remove(), r < n && (n -= 1), n = Math.max(n, 0);a.loop && t.loopCreate(), a.observer && te.observer || t.update(), a.loop ? t.slideTo(n + t.loopedSlides, 0, !1) : t.slideTo(n, 0, !1);
    }, removeAllSlides: function removeAllSlides() {
      for (var e = [], t = 0; t < this.slides.length; t += 1) {
        e.push(t);
      }this.removeSlide(e);
    } },
      g = function () {
    var e = J.navigator.userAgent,
        t = { ios: !1, android: !1, androidChrome: !1, desktop: !1, windows: !1, iphone: !1, ipod: !1, ipad: !1, cordova: J.cordova || J.phonegap, phonegap: J.cordova || J.phonegap },
        a = e.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),
        i = e.match(/(Android);?[\s\/]+([\d.]+)?/),
        s = e.match(/(iPad).*OS\s([\d_]+)/),
        r = e.match(/(iPod)(.*OS\s([\d_]+))?/),
        n = !s && e.match(/(iPhone\sOS|iOS)\s([\d_]+)/);if (a && (t.os = "windows", t.osVersion = a[2], t.windows = !0), i && !a && (t.os = "android", t.osVersion = i[2], t.android = !0, t.androidChrome = 0 <= e.toLowerCase().indexOf("chrome")), (s || n || r) && (t.os = "ios", t.ios = !0), n && !r && (t.osVersion = n[2].replace(/_/g, "."), t.iphone = !0), s && (t.osVersion = s[2].replace(/_/g, "."), t.ipad = !0), r && (t.osVersion = r[3] ? r[3].replace(/_/g, ".") : null, t.iphone = !0), t.ios && t.osVersion && 0 <= e.indexOf("Version/") && "10" === t.osVersion.split(".")[0] && (t.osVersion = e.toLowerCase().split("version/")[1].split(" ")[0]), t.desktop = !(t.os || t.android || t.webView), t.webView = (n || s || r) && e.match(/.*AppleWebKit(?!.*Safari)/i), t.os && "ios" === t.os) {
      var o = t.osVersion.split("."),
          l = f.querySelector('meta[name="viewport"]');t.minimalUi = !t.webView && (r || n) && (1 * o[0] == 7 ? 1 <= 1 * o[1] : 7 < 1 * o[0]) && l && 0 <= l.getAttribute("content").indexOf("minimal-ui");
    }return t.pixelRatio = J.devicePixelRatio || 1, t;
  }();function b() {
    var e = this,
        t = e.params,
        a = e.el;if (!a || 0 !== a.offsetWidth) {
      t.breakpoints && e.setBreakpoint();var i = e.allowSlideNext,
          s = e.allowSlidePrev,
          r = e.snapGrid;if (e.allowSlideNext = !0, e.allowSlidePrev = !0, e.updateSize(), e.updateSlides(), t.freeMode) {
        var n = Math.min(Math.max(e.translate, e.maxTranslate()), e.minTranslate());e.setTranslate(n), e.updateActiveIndex(), e.updateSlidesClasses(), t.autoHeight && e.updateAutoHeight();
      } else e.updateSlidesClasses(), ("auto" === t.slidesPerView || 1 < t.slidesPerView) && e.isEnd && !e.params.centeredSlides ? e.slideTo(e.slides.length - 1, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0);e.allowSlidePrev = s, e.allowSlideNext = i, e.params.watchOverflow && r !== e.snapGrid && e.checkOverflow();
    }
  }var w = { init: !0, direction: "horizontal", touchEventsTarget: "container", initialSlide: 0, speed: 300, preventInteractionOnTransition: !1, edgeSwipeDetection: !1, edgeSwipeThreshold: 20, freeMode: !1, freeModeMomentum: !0, freeModeMomentumRatio: 1, freeModeMomentumBounce: !0, freeModeMomentumBounceRatio: 1, freeModeMomentumVelocityRatio: 1, freeModeSticky: !1, freeModeMinimumVelocity: .02, autoHeight: !1, setWrapperSize: !1, virtualTranslate: !1, effect: "slide", breakpoints: void 0, breakpointsInverse: !1, spaceBetween: 0, slidesPerView: 1, slidesPerColumn: 1, slidesPerColumnFill: "column", slidesPerGroup: 1, centeredSlides: !1, slidesOffsetBefore: 0, slidesOffsetAfter: 0, normalizeSlideIndex: !0, centerInsufficientSlides: !1, watchOverflow: !1, roundLengths: !1, touchRatio: 1, touchAngle: 45, simulateTouch: !0, shortSwipes: !0, longSwipes: !0, longSwipesRatio: .5, longSwipesMs: 300, followFinger: !0, allowTouchMove: !0, threshold: 0, touchMoveStopPropagation: !0, touchStartPreventDefault: !0, touchStartForcePreventDefault: !1, touchReleaseOnEdges: !1, uniqueNavElements: !0, resistance: !0, resistanceRatio: .85, watchSlidesProgress: !1, watchSlidesVisibility: !1, grabCursor: !1, preventClicks: !0, preventClicksPropagation: !0, slideToClickedSlide: !1, preloadImages: !0, updateOnImagesReady: !0, loop: !1, loopAdditionalSlides: 0, loopedSlides: null, loopFillGroupWithBlank: !1, allowSlidePrev: !0, allowSlideNext: !0, swipeHandler: null, noSwiping: !0, noSwipingClass: "swiper-no-swiping", noSwipingSelector: null, passiveListeners: !0, containerModifierClass: "swiper-container-", slideClass: "swiper-slide", slideBlankClass: "swiper-slide-invisible-blank", slideActiveClass: "swiper-slide-active", slideDuplicateActiveClass: "swiper-slide-duplicate-active", slideVisibleClass: "swiper-slide-visible", slideDuplicateClass: "swiper-slide-duplicate", slideNextClass: "swiper-slide-next", slideDuplicateNextClass: "swiper-slide-duplicate-next", slidePrevClass: "swiper-slide-prev", slideDuplicatePrevClass: "swiper-slide-duplicate-prev", wrapperClass: "swiper-wrapper", runCallbacksOnInit: !0 },
      y = { update: d, translate: p, transition: c, slide: u, loop: h, grabCursor: v, manipulation: m, events: { attachEvents: function attachEvents() {
        var e = this,
            t = e.params,
            a = e.touchEvents,
            i = e.el,
            s = e.wrapperEl;e.onTouchStart = function (e) {
          var t = this,
              a = t.touchEventsData,
              i = t.params,
              s = t.touches;if (!t.animating || !i.preventInteractionOnTransition) {
            var r = e;if (r.originalEvent && (r = r.originalEvent), a.isTouchEvent = "touchstart" === r.type, (a.isTouchEvent || !("which" in r) || 3 !== r.which) && !(!a.isTouchEvent && "button" in r && 0 < r.button || a.isTouched && a.isMoved)) if (i.noSwiping && L(r.target).closest(i.noSwipingSelector ? i.noSwipingSelector : "." + i.noSwipingClass)[0]) t.allowClick = !0;else if (!i.swipeHandler || L(r).closest(i.swipeHandler)[0]) {
              s.currentX = "touchstart" === r.type ? r.targetTouches[0].pageX : r.pageX, s.currentY = "touchstart" === r.type ? r.targetTouches[0].pageY : r.pageY;var n = s.currentX,
                  o = s.currentY,
                  l = i.edgeSwipeDetection || i.iOSEdgeSwipeDetection,
                  d = i.edgeSwipeThreshold || i.iOSEdgeSwipeThreshold;if (!l || !(n <= d || n >= J.screen.width - d)) {
                if (ee.extend(a, { isTouched: !0, isMoved: !1, allowTouchCallbacks: !0, isScrolling: void 0, startMoving: void 0 }), s.startX = n, s.startY = o, a.touchStartTime = ee.now(), t.allowClick = !0, t.updateSize(), t.swipeDirection = void 0, 0 < i.threshold && (a.allowThresholdMove = !1), "touchstart" !== r.type) {
                  var p = !0;L(r.target).is(a.formElements) && (p = !1), f.activeElement && L(f.activeElement).is(a.formElements) && f.activeElement !== r.target && f.activeElement.blur();var c = p && t.allowTouchMove && i.touchStartPreventDefault;(i.touchStartForcePreventDefault || c) && r.preventDefault();
                }t.emit("touchStart", r);
              }
            }
          }
        }.bind(e), e.onTouchMove = function (e) {
          var t = this,
              a = t.touchEventsData,
              i = t.params,
              s = t.touches,
              r = t.rtlTranslate,
              n = e;if (n.originalEvent && (n = n.originalEvent), a.isTouched) {
            if (!a.isTouchEvent || "mousemove" !== n.type) {
              var o = "touchmove" === n.type ? n.targetTouches[0].pageX : n.pageX,
                  l = "touchmove" === n.type ? n.targetTouches[0].pageY : n.pageY;if (n.preventedByNestedSwiper) return s.startX = o, void (s.startY = l);if (!t.allowTouchMove) return t.allowClick = !1, void (a.isTouched && (ee.extend(s, { startX: o, startY: l, currentX: o, currentY: l }), a.touchStartTime = ee.now()));if (a.isTouchEvent && i.touchReleaseOnEdges && !i.loop) if (t.isVertical()) {
                if (l < s.startY && t.translate <= t.maxTranslate() || l > s.startY && t.translate >= t.minTranslate()) return a.isTouched = !1, void (a.isMoved = !1);
              } else if (o < s.startX && t.translate <= t.maxTranslate() || o > s.startX && t.translate >= t.minTranslate()) return;if (a.isTouchEvent && f.activeElement && n.target === f.activeElement && L(n.target).is(a.formElements)) return a.isMoved = !0, void (t.allowClick = !1);if (a.allowTouchCallbacks && t.emit("touchMove", n), !(n.targetTouches && 1 < n.targetTouches.length)) {
                s.currentX = o, s.currentY = l;var d,
                    p = s.currentX - s.startX,
                    c = s.currentY - s.startY;if (!(t.params.threshold && Math.sqrt(Math.pow(p, 2) + Math.pow(c, 2)) < t.params.threshold)) if (void 0 === a.isScrolling && (t.isHorizontal() && s.currentY === s.startY || t.isVertical() && s.currentX === s.startX ? a.isScrolling = !1 : 25 <= p * p + c * c && (d = 180 * Math.atan2(Math.abs(c), Math.abs(p)) / Math.PI, a.isScrolling = t.isHorizontal() ? d > i.touchAngle : 90 - d > i.touchAngle)), a.isScrolling && t.emit("touchMoveOpposite", n), void 0 === a.startMoving && (s.currentX === s.startX && s.currentY === s.startY || (a.startMoving = !0)), a.isScrolling) a.isTouched = !1;else if (a.startMoving) {
                  t.allowClick = !1, n.preventDefault(), i.touchMoveStopPropagation && !i.nested && n.stopPropagation(), a.isMoved || (i.loop && t.loopFix(), a.startTranslate = t.getTranslate(), t.setTransition(0), t.animating && t.$wrapperEl.trigger("webkitTransitionEnd transitionend"), a.allowMomentumBounce = !1, !i.grabCursor || !0 !== t.allowSlideNext && !0 !== t.allowSlidePrev || t.setGrabCursor(!0), t.emit("sliderFirstMove", n)), t.emit("sliderMove", n), a.isMoved = !0;var u = t.isHorizontal() ? p : c;s.diff = u, u *= i.touchRatio, r && (u = -u), t.swipeDirection = 0 < u ? "prev" : "next", a.currentTranslate = u + a.startTranslate;var h = !0,
                      v = i.resistanceRatio;if (i.touchReleaseOnEdges && (v = 0), 0 < u && a.currentTranslate > t.minTranslate() ? (h = !1, i.resistance && (a.currentTranslate = t.minTranslate() - 1 + Math.pow(-t.minTranslate() + a.startTranslate + u, v))) : u < 0 && a.currentTranslate < t.maxTranslate() && (h = !1, i.resistance && (a.currentTranslate = t.maxTranslate() + 1 - Math.pow(t.maxTranslate() - a.startTranslate - u, v))), h && (n.preventedByNestedSwiper = !0), !t.allowSlideNext && "next" === t.swipeDirection && a.currentTranslate < a.startTranslate && (a.currentTranslate = a.startTranslate), !t.allowSlidePrev && "prev" === t.swipeDirection && a.currentTranslate > a.startTranslate && (a.currentTranslate = a.startTranslate), 0 < i.threshold) {
                    if (!(Math.abs(u) > i.threshold || a.allowThresholdMove)) return void (a.currentTranslate = a.startTranslate);if (!a.allowThresholdMove) return a.allowThresholdMove = !0, s.startX = s.currentX, s.startY = s.currentY, a.currentTranslate = a.startTranslate, void (s.diff = t.isHorizontal() ? s.currentX - s.startX : s.currentY - s.startY);
                  }i.followFinger && ((i.freeMode || i.watchSlidesProgress || i.watchSlidesVisibility) && (t.updateActiveIndex(), t.updateSlidesClasses()), i.freeMode && (0 === a.velocities.length && a.velocities.push({ position: s[t.isHorizontal() ? "startX" : "startY"], time: a.touchStartTime }), a.velocities.push({ position: s[t.isHorizontal() ? "currentX" : "currentY"], time: ee.now() })), t.updateProgress(a.currentTranslate), t.setTranslate(a.currentTranslate));
                }
              }
            }
          } else a.startMoving && a.isScrolling && t.emit("touchMoveOpposite", n);
        }.bind(e), e.onTouchEnd = function (e) {
          var t = this,
              a = t.touchEventsData,
              i = t.params,
              s = t.touches,
              r = t.rtlTranslate,
              n = t.$wrapperEl,
              o = t.slidesGrid,
              l = t.snapGrid,
              d = e;if (d.originalEvent && (d = d.originalEvent), a.allowTouchCallbacks && t.emit("touchEnd", d), a.allowTouchCallbacks = !1, !a.isTouched) return a.isMoved && i.grabCursor && t.setGrabCursor(!1), a.isMoved = !1, void (a.startMoving = !1);i.grabCursor && a.isMoved && a.isTouched && (!0 === t.allowSlideNext || !0 === t.allowSlidePrev) && t.setGrabCursor(!1);var p,
              c = ee.now(),
              u = c - a.touchStartTime;if (t.allowClick && (t.updateClickedSlide(d), t.emit("tap", d), u < 300 && 300 < c - a.lastClickTime && (a.clickTimeout && clearTimeout(a.clickTimeout), a.clickTimeout = ee.nextTick(function () {
            t && !t.destroyed && t.emit("click", d);
          }, 300)), u < 300 && c - a.lastClickTime < 300 && (a.clickTimeout && clearTimeout(a.clickTimeout), t.emit("doubleTap", d))), a.lastClickTime = ee.now(), ee.nextTick(function () {
            t.destroyed || (t.allowClick = !0);
          }), !a.isTouched || !a.isMoved || !t.swipeDirection || 0 === s.diff || a.currentTranslate === a.startTranslate) return a.isTouched = !1, a.isMoved = !1, void (a.startMoving = !1);if (a.isTouched = !1, a.isMoved = !1, a.startMoving = !1, p = i.followFinger ? r ? t.translate : -t.translate : -a.currentTranslate, i.freeMode) {
            if (p < -t.minTranslate()) return void t.slideTo(t.activeIndex);if (p > -t.maxTranslate()) return void (t.slides.length < l.length ? t.slideTo(l.length - 1) : t.slideTo(t.slides.length - 1));if (i.freeModeMomentum) {
              if (1 < a.velocities.length) {
                var h = a.velocities.pop(),
                    v = a.velocities.pop(),
                    f = h.position - v.position,
                    m = h.time - v.time;t.velocity = f / m, t.velocity /= 2, Math.abs(t.velocity) < i.freeModeMinimumVelocity && (t.velocity = 0), (150 < m || 300 < ee.now() - h.time) && (t.velocity = 0);
              } else t.velocity = 0;t.velocity *= i.freeModeMomentumVelocityRatio, a.velocities.length = 0;var g = 1e3 * i.freeModeMomentumRatio,
                  b = t.velocity * g,
                  w = t.translate + b;r && (w = -w);var y,
                  x,
                  T = !1,
                  E = 20 * Math.abs(t.velocity) * i.freeModeMomentumBounceRatio;if (w < t.maxTranslate()) i.freeModeMomentumBounce ? (w + t.maxTranslate() < -E && (w = t.maxTranslate() - E), y = t.maxTranslate(), T = !0, a.allowMomentumBounce = !0) : w = t.maxTranslate(), i.loop && i.centeredSlides && (x = !0);else if (w > t.minTranslate()) i.freeModeMomentumBounce ? (w - t.minTranslate() > E && (w = t.minTranslate() + E), y = t.minTranslate(), T = !0, a.allowMomentumBounce = !0) : w = t.minTranslate(), i.loop && i.centeredSlides && (x = !0);else if (i.freeModeSticky) {
                for (var S, C = 0; C < l.length; C += 1) {
                  if (l[C] > -w) {
                    S = C;break;
                  }
                }w = -(w = Math.abs(l[S] - w) < Math.abs(l[S - 1] - w) || "next" === t.swipeDirection ? l[S] : l[S - 1]);
              }if (x && t.once("transitionEnd", function () {
                t.loopFix();
              }), 0 !== t.velocity) g = r ? Math.abs((-w - t.translate) / t.velocity) : Math.abs((w - t.translate) / t.velocity);else if (i.freeModeSticky) return void t.slideToClosest();i.freeModeMomentumBounce && T ? (t.updateProgress(y), t.setTransition(g), t.setTranslate(w), t.transitionStart(!0, t.swipeDirection), t.animating = !0, n.transitionEnd(function () {
                t && !t.destroyed && a.allowMomentumBounce && (t.emit("momentumBounce"), t.setTransition(i.speed), t.setTranslate(y), n.transitionEnd(function () {
                  t && !t.destroyed && t.transitionEnd();
                }));
              })) : t.velocity ? (t.updateProgress(w), t.setTransition(g), t.setTranslate(w), t.transitionStart(!0, t.swipeDirection), t.animating || (t.animating = !0, n.transitionEnd(function () {
                t && !t.destroyed && t.transitionEnd();
              }))) : t.updateProgress(w), t.updateActiveIndex(), t.updateSlidesClasses();
            } else if (i.freeModeSticky) return void t.slideToClosest();(!i.freeModeMomentum || u >= i.longSwipesMs) && (t.updateProgress(), t.updateActiveIndex(), t.updateSlidesClasses());
          } else {
            for (var M = 0, z = t.slidesSizesGrid[0], P = 0; P < o.length; P += i.slidesPerGroup) {
              void 0 !== o[P + i.slidesPerGroup] ? p >= o[P] && p < o[P + i.slidesPerGroup] && (z = o[(M = P) + i.slidesPerGroup] - o[P]) : p >= o[P] && (M = P, z = o[o.length - 1] - o[o.length - 2]);
            }var k = (p - o[M]) / z;if (u > i.longSwipesMs) {
              if (!i.longSwipes) return void t.slideTo(t.activeIndex);"next" === t.swipeDirection && (k >= i.longSwipesRatio ? t.slideTo(M + i.slidesPerGroup) : t.slideTo(M)), "prev" === t.swipeDirection && (k > 1 - i.longSwipesRatio ? t.slideTo(M + i.slidesPerGroup) : t.slideTo(M));
            } else {
              if (!i.shortSwipes) return void t.slideTo(t.activeIndex);"next" === t.swipeDirection && t.slideTo(M + i.slidesPerGroup), "prev" === t.swipeDirection && t.slideTo(M);
            }
          }
        }.bind(e), e.onClick = function (e) {
          this.allowClick || (this.params.preventClicks && e.preventDefault(), this.params.preventClicksPropagation && this.animating && (e.stopPropagation(), e.stopImmediatePropagation()));
        }.bind(e);var r = "container" === t.touchEventsTarget ? i : s,
            n = !!t.nested;if (te.touch || !te.pointerEvents && !te.prefixedPointerEvents) {
          if (te.touch) {
            var o = !("touchstart" !== a.start || !te.passiveListener || !t.passiveListeners) && { passive: !0, capture: !1 };r.addEventListener(a.start, e.onTouchStart, o), r.addEventListener(a.move, e.onTouchMove, te.passiveListener ? { passive: !1, capture: n } : n), r.addEventListener(a.end, e.onTouchEnd, o);
          }(t.simulateTouch && !g.ios && !g.android || t.simulateTouch && !te.touch && g.ios) && (r.addEventListener("mousedown", e.onTouchStart, !1), f.addEventListener("mousemove", e.onTouchMove, n), f.addEventListener("mouseup", e.onTouchEnd, !1));
        } else r.addEventListener(a.start, e.onTouchStart, !1), f.addEventListener(a.move, e.onTouchMove, n), f.addEventListener(a.end, e.onTouchEnd, !1);(t.preventClicks || t.preventClicksPropagation) && r.addEventListener("click", e.onClick, !0), e.on(g.ios || g.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", b, !0);
      }, detachEvents: function detachEvents() {
        var e = this,
            t = e.params,
            a = e.touchEvents,
            i = e.el,
            s = e.wrapperEl,
            r = "container" === t.touchEventsTarget ? i : s,
            n = !!t.nested;if (te.touch || !te.pointerEvents && !te.prefixedPointerEvents) {
          if (te.touch) {
            var o = !("onTouchStart" !== a.start || !te.passiveListener || !t.passiveListeners) && { passive: !0, capture: !1 };r.removeEventListener(a.start, e.onTouchStart, o), r.removeEventListener(a.move, e.onTouchMove, n), r.removeEventListener(a.end, e.onTouchEnd, o);
          }(t.simulateTouch && !g.ios && !g.android || t.simulateTouch && !te.touch && g.ios) && (r.removeEventListener("mousedown", e.onTouchStart, !1), f.removeEventListener("mousemove", e.onTouchMove, n), f.removeEventListener("mouseup", e.onTouchEnd, !1));
        } else r.removeEventListener(a.start, e.onTouchStart, !1), f.removeEventListener(a.move, e.onTouchMove, n), f.removeEventListener(a.end, e.onTouchEnd, !1);(t.preventClicks || t.preventClicksPropagation) && r.removeEventListener("click", e.onClick, !0), e.off(g.ios || g.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", b);
      } }, breakpoints: { setBreakpoint: function setBreakpoint() {
        var e = this,
            t = e.activeIndex,
            a = e.initialized,
            i = e.loopedSlides;void 0 === i && (i = 0);var s = e.params,
            r = s.breakpoints;if (r && (!r || 0 !== Object.keys(r).length)) {
          var n = e.getBreakpoint(r);if (n && e.currentBreakpoint !== n) {
            var o = n in r ? r[n] : void 0;o && ["slidesPerView", "spaceBetween", "slidesPerGroup"].forEach(function (e) {
              var t = o[e];void 0 !== t && (o[e] = "slidesPerView" !== e || "AUTO" !== t && "auto" !== t ? "slidesPerView" === e ? parseFloat(t) : parseInt(t, 10) : "auto");
            });var l = o || e.originalParams,
                d = l.direction && l.direction !== s.direction,
                p = s.loop && (l.slidesPerView !== s.slidesPerView || d);d && a && e.changeDirection(), ee.extend(e.params, l), ee.extend(e, { allowTouchMove: e.params.allowTouchMove, allowSlideNext: e.params.allowSlideNext, allowSlidePrev: e.params.allowSlidePrev }), e.currentBreakpoint = n, p && a && (e.loopDestroy(), e.loopCreate(), e.updateSlides(), e.slideTo(t - i + e.loopedSlides, 0, !1)), e.emit("breakpoint", l);
          }
        }
      }, getBreakpoint: function getBreakpoint(e) {
        if (e) {
          var t = !1,
              a = [];Object.keys(e).forEach(function (e) {
            a.push(e);
          }), a.sort(function (e, t) {
            return parseInt(e, 10) - parseInt(t, 10);
          });for (var i = 0; i < a.length; i += 1) {
            var s = a[i];this.params.breakpointsInverse ? s <= J.innerWidth && (t = s) : s >= J.innerWidth && !t && (t = s);
          }return t || "max";
        }
      } }, checkOverflow: { checkOverflow: function checkOverflow() {
        var e = this,
            t = e.isLocked;e.isLocked = 1 === e.snapGrid.length, e.allowSlideNext = !e.isLocked, e.allowSlidePrev = !e.isLocked, t !== e.isLocked && e.emit(e.isLocked ? "lock" : "unlock"), t && t !== e.isLocked && (e.isEnd = !1, e.navigation.update());
      } }, classes: { addClasses: function addClasses() {
        var t = this.classNames,
            a = this.params,
            e = this.rtl,
            i = this.$el,
            s = [];s.push("initialized"), s.push(a.direction), a.freeMode && s.push("free-mode"), te.flexbox || s.push("no-flexbox"), a.autoHeight && s.push("autoheight"), e && s.push("rtl"), 1 < a.slidesPerColumn && s.push("multirow"), g.android && s.push("android"), g.ios && s.push("ios"), (I.isIE || I.isEdge) && (te.pointerEvents || te.prefixedPointerEvents) && s.push("wp8-" + a.direction), s.forEach(function (e) {
          t.push(a.containerModifierClass + e);
        }), i.addClass(t.join(" "));
      }, removeClasses: function removeClasses() {
        var e = this.$el,
            t = this.classNames;e.removeClass(t.join(" "));
      } }, images: { loadImage: function loadImage(e, t, a, i, s, r) {
        var n;function o() {
          r && r();
        }e.complete && s ? o() : t ? ((n = new J.Image()).onload = o, n.onerror = o, i && (n.sizes = i), a && (n.srcset = a), t && (n.src = t)) : o();
      }, preloadImages: function preloadImages() {
        var e = this;function t() {
          null != e && e && !e.destroyed && (void 0 !== e.imagesLoaded && (e.imagesLoaded += 1), e.imagesLoaded === e.imagesToLoad.length && (e.params.updateOnImagesReady && e.update(), e.emit("imagesReady")));
        }e.imagesToLoad = e.$el.find("img");for (var a = 0; a < e.imagesToLoad.length; a += 1) {
          var i = e.imagesToLoad[a];e.loadImage(i, i.currentSrc || i.getAttribute("src"), i.srcset || i.getAttribute("srcset"), i.sizes || i.getAttribute("sizes"), !0, t);
        }
      } } },
      x = {},
      T = function (u) {
    function h() {
      for (var e, t, s, a = [], i = arguments.length; i--;) {
        a[i] = arguments[i];
      }1 === a.length && a[0].constructor && a[0].constructor === Object ? s = a[0] : (t = (e = a)[0], s = e[1]), s || (s = {}), s = ee.extend({}, s), t && !s.el && (s.el = t), u.call(this, s), Object.keys(y).forEach(function (t) {
        Object.keys(y[t]).forEach(function (e) {
          h.prototype[e] || (h.prototype[e] = y[t][e]);
        });
      });var r = this;void 0 === r.modules && (r.modules = {}), Object.keys(r.modules).forEach(function (e) {
        var t = r.modules[e];if (t.params) {
          var a = Object.keys(t.params)[0],
              i = t.params[a];if ("object" != (typeof i === "undefined" ? "undefined" : _typeof(i)) || null === i) return;if (!(a in s && "enabled" in i)) return;!0 === s[a] && (s[a] = { enabled: !0 }), "object" != _typeof(s[a]) || "enabled" in s[a] || (s[a].enabled = !0), s[a] || (s[a] = { enabled: !1 });
        }
      });var n = ee.extend({}, w);r.useModulesParams(n), r.params = ee.extend({}, n, x, s), r.originalParams = ee.extend({}, r.params), r.passedParams = ee.extend({}, s);var o = (r.$ = L)(r.params.el);if (t = o[0]) {
        if (1 < o.length) {
          var l = [];return o.each(function (e, t) {
            var a = ee.extend({}, s, { el: t });l.push(new h(a));
          }), l;
        }t.swiper = r, o.data("swiper", r);var d,
            p,
            c = o.children("." + r.params.wrapperClass);return ee.extend(r, { $el: o, el: t, $wrapperEl: c, wrapperEl: c[0], classNames: [], slides: L(), slidesGrid: [], snapGrid: [], slidesSizesGrid: [], isHorizontal: function isHorizontal() {
            return "horizontal" === r.params.direction;
          }, isVertical: function isVertical() {
            return "vertical" === r.params.direction;
          }, rtl: "rtl" === t.dir.toLowerCase() || "rtl" === o.css("direction"), rtlTranslate: "horizontal" === r.params.direction && ("rtl" === t.dir.toLowerCase() || "rtl" === o.css("direction")), wrongRTL: "-webkit-box" === c.css("display"), activeIndex: 0, realIndex: 0, isBeginning: !0, isEnd: !1, translate: 0, previousTranslate: 0, progress: 0, velocity: 0, animating: !1, allowSlideNext: r.params.allowSlideNext, allowSlidePrev: r.params.allowSlidePrev, touchEvents: (d = ["touchstart", "touchmove", "touchend"], p = ["mousedown", "mousemove", "mouseup"], te.pointerEvents ? p = ["pointerdown", "pointermove", "pointerup"] : te.prefixedPointerEvents && (p = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), r.touchEventsTouch = { start: d[0], move: d[1], end: d[2] }, r.touchEventsDesktop = { start: p[0], move: p[1], end: p[2] }, te.touch || !r.params.simulateTouch ? r.touchEventsTouch : r.touchEventsDesktop), touchEventsData: { isTouched: void 0, isMoved: void 0, allowTouchCallbacks: void 0, touchStartTime: void 0, isScrolling: void 0, currentTranslate: void 0, startTranslate: void 0, allowThresholdMove: void 0, formElements: "input, select, option, textarea, button, video", lastClickTime: ee.now(), clickTimeout: void 0, velocities: [], allowMomentumBounce: void 0, isTouchEvent: void 0, startMoving: void 0 }, allowClick: !0, allowTouchMove: r.params.allowTouchMove, touches: { startX: 0, startY: 0, currentX: 0, currentY: 0, diff: 0 }, imagesToLoad: [], imagesLoaded: 0 }), r.useModules(), r.params.init && r.init(), r;
      }
    }u && (h.__proto__ = u);var e = { extendedDefaults: { configurable: !0 }, defaults: { configurable: !0 }, Class: { configurable: !0 }, $: { configurable: !0 } };return ((h.prototype = Object.create(u && u.prototype)).constructor = h).prototype.slidesPerViewDynamic = function () {
      var e = this,
          t = e.params,
          a = e.slides,
          i = e.slidesGrid,
          s = e.size,
          r = e.activeIndex,
          n = 1;if (t.centeredSlides) {
        for (var o, l = a[r].swiperSlideSize, d = r + 1; d < a.length; d += 1) {
          a[d] && !o && (n += 1, s < (l += a[d].swiperSlideSize) && (o = !0));
        }for (var p = r - 1; 0 <= p; p -= 1) {
          a[p] && !o && (n += 1, s < (l += a[p].swiperSlideSize) && (o = !0));
        }
      } else for (var c = r + 1; c < a.length; c += 1) {
        i[c] - i[r] < s && (n += 1);
      }return n;
    }, h.prototype.update = function () {
      var a = this;if (a && !a.destroyed) {
        var e = a.snapGrid,
            t = a.params;t.breakpoints && a.setBreakpoint(), a.updateSize(), a.updateSlides(), a.updateProgress(), a.updateSlidesClasses(), a.params.freeMode ? (i(), a.params.autoHeight && a.updateAutoHeight()) : (("auto" === a.params.slidesPerView || 1 < a.params.slidesPerView) && a.isEnd && !a.params.centeredSlides ? a.slideTo(a.slides.length - 1, 0, !1, !0) : a.slideTo(a.activeIndex, 0, !1, !0)) || i(), t.watchOverflow && e !== a.snapGrid && a.checkOverflow(), a.emit("update");
      }function i() {
        var e = a.rtlTranslate ? -1 * a.translate : a.translate,
            t = Math.min(Math.max(e, a.maxTranslate()), a.minTranslate());a.setTranslate(t), a.updateActiveIndex(), a.updateSlidesClasses();
      }
    }, h.prototype.changeDirection = function (a, e) {
      void 0 === e && (e = !0);var t = this,
          i = t.params.direction;return a || (a = "horizontal" === i ? "vertical" : "horizontal"), a === i || "horizontal" !== a && "vertical" !== a || ("vertical" === i && (t.$el.removeClass(t.params.containerModifierClass + "vertical wp8-vertical").addClass("" + t.params.containerModifierClass + a), (I.isIE || I.isEdge) && (te.pointerEvents || te.prefixedPointerEvents) && t.$el.addClass(t.params.containerModifierClass + "wp8-" + a)), "horizontal" === i && (t.$el.removeClass(t.params.containerModifierClass + "horizontal wp8-horizontal").addClass("" + t.params.containerModifierClass + a), (I.isIE || I.isEdge) && (te.pointerEvents || te.prefixedPointerEvents) && t.$el.addClass(t.params.containerModifierClass + "wp8-" + a)), t.params.direction = a, t.slides.each(function (e, t) {
        "vertical" === a ? t.style.width = "" : t.style.height = "";
      }), t.emit("changeDirection"), e && t.update()), t;
    }, h.prototype.init = function () {
      var e = this;e.initialized || (e.emit("beforeInit"), e.params.breakpoints && e.setBreakpoint(), e.addClasses(), e.params.loop && e.loopCreate(), e.updateSize(), e.updateSlides(), e.params.watchOverflow && e.checkOverflow(), e.params.grabCursor && e.setGrabCursor(), e.params.preloadImages && e.preloadImages(), e.params.loop ? e.slideTo(e.params.initialSlide + e.loopedSlides, 0, e.params.runCallbacksOnInit) : e.slideTo(e.params.initialSlide, 0, e.params.runCallbacksOnInit), e.attachEvents(), e.initialized = !0, e.emit("init"));
    }, h.prototype.destroy = function (e, t) {
      void 0 === e && (e = !0), void 0 === t && (t = !0);var a = this,
          i = a.params,
          s = a.$el,
          r = a.$wrapperEl,
          n = a.slides;return void 0 === a.params || a.destroyed || (a.emit("beforeDestroy"), a.initialized = !1, a.detachEvents(), i.loop && a.loopDestroy(), t && (a.removeClasses(), s.removeAttr("style"), r.removeAttr("style"), n && n.length && n.removeClass([i.slideVisibleClass, i.slideActiveClass, i.slideNextClass, i.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")), a.emit("destroy"), Object.keys(a.eventsListeners).forEach(function (e) {
        a.off(e);
      }), !1 !== e && (a.$el[0].swiper = null, a.$el.data("swiper", null), ee.deleteProps(a)), a.destroyed = !0), null;
    }, h.extendDefaults = function (e) {
      ee.extend(x, e);
    }, e.extendedDefaults.get = function () {
      return x;
    }, e.defaults.get = function () {
      return w;
    }, e.Class.get = function () {
      return u;
    }, e.$.get = function () {
      return L;
    }, Object.defineProperties(h, e), h;
  }(n),
      E = { name: "device", proto: { device: g }, static: { device: g } },
      S = { name: "support", proto: { support: te }, static: { support: te } },
      C = { name: "browser", proto: { browser: I }, static: { browser: I } },
      M = { name: "resize", create: function create() {
      var e = this;ee.extend(e, { resize: { resizeHandler: function resizeHandler() {
            e && !e.destroyed && e.initialized && (e.emit("beforeResize"), e.emit("resize"));
          }, orientationChangeHandler: function orientationChangeHandler() {
            e && !e.destroyed && e.initialized && e.emit("orientationchange");
          } } });
    }, on: { init: function init() {
        J.addEventListener("resize", this.resize.resizeHandler), J.addEventListener("orientationchange", this.resize.orientationChangeHandler);
      }, destroy: function destroy() {
        J.removeEventListener("resize", this.resize.resizeHandler), J.removeEventListener("orientationchange", this.resize.orientationChangeHandler);
      } } },
      z = { func: J.MutationObserver || J.WebkitMutationObserver, attach: function attach(e, t) {
      void 0 === t && (t = {});var a = this,
          i = new z.func(function (e) {
        if (1 !== e.length) {
          var t = function t() {
            a.emit("observerUpdate", e[0]);
          };J.requestAnimationFrame ? J.requestAnimationFrame(t) : J.setTimeout(t, 0);
        } else a.emit("observerUpdate", e[0]);
      });i.observe(e, { attributes: void 0 === t.attributes || t.attributes, childList: void 0 === t.childList || t.childList, characterData: void 0 === t.characterData || t.characterData }), a.observer.observers.push(i);
    }, init: function init() {
      var e = this;if (te.observer && e.params.observer) {
        if (e.params.observeParents) for (var t = e.$el.parents(), a = 0; a < t.length; a += 1) {
          e.observer.attach(t[a]);
        }e.observer.attach(e.$el[0], { childList: e.params.observeSlideChildren }), e.observer.attach(e.$wrapperEl[0], { attributes: !1 });
      }
    }, destroy: function destroy() {
      this.observer.observers.forEach(function (e) {
        e.disconnect();
      }), this.observer.observers = [];
    } },
      P = { name: "observer", params: { observer: !1, observeParents: !1, observeSlideChildren: !1 }, create: function create() {
      ee.extend(this, { observer: { init: z.init.bind(this), attach: z.attach.bind(this), destroy: z.destroy.bind(this), observers: [] } });
    }, on: { init: function init() {
        this.observer.init();
      }, destroy: function destroy() {
        this.observer.destroy();
      } } },
      k = { update: function update(e) {
      var t = this,
          a = t.params,
          i = a.slidesPerView,
          s = a.slidesPerGroup,
          r = a.centeredSlides,
          n = t.params.virtual,
          o = n.addSlidesBefore,
          l = n.addSlidesAfter,
          d = t.virtual,
          p = d.from,
          c = d.to,
          u = d.slides,
          h = d.slidesGrid,
          v = d.renderSlide,
          f = d.offset;t.updateActiveIndex();var m,
          g,
          b,
          w = t.activeIndex || 0;m = t.rtlTranslate ? "right" : t.isHorizontal() ? "left" : "top", r ? (g = Math.floor(i / 2) + s + o, b = Math.floor(i / 2) + s + l) : (g = i + (s - 1) + o, b = s + l);var y = Math.max((w || 0) - b, 0),
          x = Math.min((w || 0) + g, u.length - 1),
          T = (t.slidesGrid[y] || 0) - (t.slidesGrid[0] || 0);function E() {
        t.updateSlides(), t.updateProgress(), t.updateSlidesClasses(), t.lazy && t.params.lazy.enabled && t.lazy.load();
      }if (ee.extend(t.virtual, { from: y, to: x, offset: T, slidesGrid: t.slidesGrid }), p === y && c === x && !e) return t.slidesGrid !== h && T !== f && t.slides.css(m, T + "px"), void t.updateProgress();if (t.params.virtual.renderExternal) return t.params.virtual.renderExternal.call(t, { offset: T, from: y, to: x, slides: function () {
          for (var e = [], t = y; t <= x; t += 1) {
            e.push(u[t]);
          }return e;
        }() }), void E();var S = [],
          C = [];if (e) t.$wrapperEl.find("." + t.params.slideClass).remove();else for (var M = p; M <= c; M += 1) {
        (M < y || x < M) && t.$wrapperEl.find("." + t.params.slideClass + '[data-swiper-slide-index="' + M + '"]').remove();
      }for (var z = 0; z < u.length; z += 1) {
        y <= z && z <= x && (void 0 === c || e ? C.push(z) : (c < z && C.push(z), z < p && S.push(z)));
      }C.forEach(function (e) {
        t.$wrapperEl.append(v(u[e], e));
      }), S.sort(function (e, t) {
        return t - e;
      }).forEach(function (e) {
        t.$wrapperEl.prepend(v(u[e], e));
      }), t.$wrapperEl.children(".swiper-slide").css(m, T + "px"), E();
    }, renderSlide: function renderSlide(e, t) {
      var a = this,
          i = a.params.virtual;if (i.cache && a.virtual.cache[t]) return a.virtual.cache[t];var s = i.renderSlide ? L(i.renderSlide.call(a, e, t)) : L('<div class="' + a.params.slideClass + '" data-swiper-slide-index="' + t + '">' + e + "</div>");return s.attr("data-swiper-slide-index") || s.attr("data-swiper-slide-index", t), i.cache && (a.virtual.cache[t] = s), s;
    }, appendSlide: function appendSlide(e) {
      if ("object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && "length" in e) for (var t = 0; t < e.length; t += 1) {
        e[t] && this.virtual.slides.push(e[t]);
      } else this.virtual.slides.push(e);this.virtual.update(!0);
    }, prependSlide: function prependSlide(e) {
      var t = this,
          a = t.activeIndex,
          i = a + 1,
          s = 1;if (Array.isArray(e)) {
        for (var r = 0; r < e.length; r += 1) {
          e[r] && t.virtual.slides.unshift(e[r]);
        }i = a + e.length, s = e.length;
      } else t.virtual.slides.unshift(e);if (t.params.virtual.cache) {
        var n = t.virtual.cache,
            o = {};Object.keys(n).forEach(function (e) {
          o[parseInt(e, 10) + s] = n[e];
        }), t.virtual.cache = o;
      }t.virtual.update(!0), t.slideTo(i, 0);
    }, removeSlide: function removeSlide(e) {
      var t = this;if (null != e) {
        var a = t.activeIndex;if (Array.isArray(e)) for (var i = e.length - 1; 0 <= i; i -= 1) {
          t.virtual.slides.splice(e[i], 1), t.params.virtual.cache && delete t.virtual.cache[e[i]], e[i] < a && (a -= 1), a = Math.max(a, 0);
        } else t.virtual.slides.splice(e, 1), t.params.virtual.cache && delete t.virtual.cache[e], e < a && (a -= 1), a = Math.max(a, 0);t.virtual.update(!0), t.slideTo(a, 0);
      }
    }, removeAllSlides: function removeAllSlides() {
      var e = this;e.virtual.slides = [], e.params.virtual.cache && (e.virtual.cache = {}), e.virtual.update(!0), e.slideTo(0, 0);
    } },
      $ = { name: "virtual", params: { virtual: { enabled: !1, slides: [], cache: !0, renderSlide: null, renderExternal: null, addSlidesBefore: 0, addSlidesAfter: 0 } }, create: function create() {
      var e = this;ee.extend(e, { virtual: { update: k.update.bind(e), appendSlide: k.appendSlide.bind(e), prependSlide: k.prependSlide.bind(e), removeSlide: k.removeSlide.bind(e), removeAllSlides: k.removeAllSlides.bind(e), renderSlide: k.renderSlide.bind(e), slides: e.params.virtual.slides, cache: {} } });
    }, on: { beforeInit: function beforeInit() {
        var e = this;if (e.params.virtual.enabled) {
          e.classNames.push(e.params.containerModifierClass + "virtual");var t = { watchSlidesProgress: !0 };ee.extend(e.params, t), ee.extend(e.originalParams, t), e.params.initialSlide || e.virtual.update();
        }
      }, setTranslate: function setTranslate() {
        this.params.virtual.enabled && this.virtual.update();
      } } },
      D = { handle: function handle(e) {
      var t = this,
          a = t.rtlTranslate,
          i = e;i.originalEvent && (i = i.originalEvent);var s = i.keyCode || i.charCode;if (!t.allowSlideNext && (t.isHorizontal() && 39 === s || t.isVertical() && 40 === s)) return !1;if (!t.allowSlidePrev && (t.isHorizontal() && 37 === s || t.isVertical() && 38 === s)) return !1;if (!(i.shiftKey || i.altKey || i.ctrlKey || i.metaKey || f.activeElement && f.activeElement.nodeName && ("input" === f.activeElement.nodeName.toLowerCase() || "textarea" === f.activeElement.nodeName.toLowerCase()))) {
        if (t.params.keyboard.onlyInViewport && (37 === s || 39 === s || 38 === s || 40 === s)) {
          var r = !1;if (0 < t.$el.parents("." + t.params.slideClass).length && 0 === t.$el.parents("." + t.params.slideActiveClass).length) return;var n = J.innerWidth,
              o = J.innerHeight,
              l = t.$el.offset();a && (l.left -= t.$el[0].scrollLeft);for (var d = [[l.left, l.top], [l.left + t.width, l.top], [l.left, l.top + t.height], [l.left + t.width, l.top + t.height]], p = 0; p < d.length; p += 1) {
            var c = d[p];0 <= c[0] && c[0] <= n && 0 <= c[1] && c[1] <= o && (r = !0);
          }if (!r) return;
        }t.isHorizontal() ? (37 !== s && 39 !== s || (i.preventDefault ? i.preventDefault() : i.returnValue = !1), (39 === s && !a || 37 === s && a) && t.slideNext(), (37 === s && !a || 39 === s && a) && t.slidePrev()) : (38 !== s && 40 !== s || (i.preventDefault ? i.preventDefault() : i.returnValue = !1), 40 === s && t.slideNext(), 38 === s && t.slidePrev()), t.emit("keyPress", s);
      }
    }, enable: function enable() {
      this.keyboard.enabled || (L(f).on("keydown", this.keyboard.handle), this.keyboard.enabled = !0);
    }, disable: function disable() {
      this.keyboard.enabled && (L(f).off("keydown", this.keyboard.handle), this.keyboard.enabled = !1);
    } },
      O = { name: "keyboard", params: { keyboard: { enabled: !1, onlyInViewport: !0 } }, create: function create() {
      ee.extend(this, { keyboard: { enabled: !1, enable: D.enable.bind(this), disable: D.disable.bind(this), handle: D.handle.bind(this) } });
    }, on: { init: function init() {
        this.params.keyboard.enabled && this.keyboard.enable();
      }, destroy: function destroy() {
        this.keyboard.enabled && this.keyboard.disable();
      } } };var A = { lastScrollTime: ee.now(), event: -1 < J.navigator.userAgent.indexOf("firefox") ? "DOMMouseScroll" : function () {
      var e = "onwheel",
          t = e in f;if (!t) {
        var a = f.createElement("div");a.setAttribute(e, "return;"), t = "function" == typeof a[e];
      }return !t && f.implementation && f.implementation.hasFeature && !0 !== f.implementation.hasFeature("", "") && (t = f.implementation.hasFeature("Events.wheel", "3.0")), t;
    }() ? "wheel" : "mousewheel", normalize: function normalize(e) {
      var t = 0,
          a = 0,
          i = 0,
          s = 0;return "detail" in e && (a = e.detail), "wheelDelta" in e && (a = -e.wheelDelta / 120), "wheelDeltaY" in e && (a = -e.wheelDeltaY / 120), "wheelDeltaX" in e && (t = -e.wheelDeltaX / 120), "axis" in e && e.axis === e.HORIZONTAL_AXIS && (t = a, a = 0), i = 10 * t, s = 10 * a, "deltaY" in e && (s = e.deltaY), "deltaX" in e && (i = e.deltaX), (i || s) && e.deltaMode && (1 === e.deltaMode ? (i *= 40, s *= 40) : (i *= 800, s *= 800)), i && !t && (t = i < 1 ? -1 : 1), s && !a && (a = s < 1 ? -1 : 1), { spinX: t, spinY: a, pixelX: i, pixelY: s };
    }, handleMouseEnter: function handleMouseEnter() {
      this.mouseEntered = !0;
    }, handleMouseLeave: function handleMouseLeave() {
      this.mouseEntered = !1;
    }, handle: function handle(e) {
      var t = e,
          a = this,
          i = a.params.mousewheel;if (!a.mouseEntered && !i.releaseOnEdges) return !0;t.originalEvent && (t = t.originalEvent);var s = 0,
          r = a.rtlTranslate ? -1 : 1,
          n = A.normalize(t);if (i.forceToAxis) {
        if (a.isHorizontal()) {
          if (!(Math.abs(n.pixelX) > Math.abs(n.pixelY))) return !0;s = n.pixelX * r;
        } else {
          if (!(Math.abs(n.pixelY) > Math.abs(n.pixelX))) return !0;s = n.pixelY;
        }
      } else s = Math.abs(n.pixelX) > Math.abs(n.pixelY) ? -n.pixelX * r : -n.pixelY;if (0 === s) return !0;if (i.invert && (s = -s), a.params.freeMode) {
        a.params.loop && a.loopFix();var o = a.getTranslate() + s * i.sensitivity,
            l = a.isBeginning,
            d = a.isEnd;if (o >= a.minTranslate() && (o = a.minTranslate()), o <= a.maxTranslate() && (o = a.maxTranslate()), a.setTransition(0), a.setTranslate(o), a.updateProgress(), a.updateActiveIndex(), a.updateSlidesClasses(), (!l && a.isBeginning || !d && a.isEnd) && a.updateSlidesClasses(), a.params.freeModeSticky && (clearTimeout(a.mousewheel.timeout), a.mousewheel.timeout = ee.nextTick(function () {
          a.slideToClosest();
        }, 300)), a.emit("scroll", t), a.params.autoplay && a.params.autoplayDisableOnInteraction && a.autoplay.stop(), o === a.minTranslate() || o === a.maxTranslate()) return !0;
      } else {
        if (60 < ee.now() - a.mousewheel.lastScrollTime) if (s < 0) {
          if (a.isEnd && !a.params.loop || a.animating) {
            if (i.releaseOnEdges) return !0;
          } else a.slideNext(), a.emit("scroll", t);
        } else if (a.isBeginning && !a.params.loop || a.animating) {
          if (i.releaseOnEdges) return !0;
        } else a.slidePrev(), a.emit("scroll", t);a.mousewheel.lastScrollTime = new J.Date().getTime();
      }return t.preventDefault ? t.preventDefault() : t.returnValue = !1, !1;
    }, enable: function enable() {
      var e = this;if (!A.event) return !1;if (e.mousewheel.enabled) return !1;var t = e.$el;return "container" !== e.params.mousewheel.eventsTarged && (t = L(e.params.mousewheel.eventsTarged)), t.on("mouseenter", e.mousewheel.handleMouseEnter), t.on("mouseleave", e.mousewheel.handleMouseLeave), t.on(A.event, e.mousewheel.handle), e.mousewheel.enabled = !0;
    }, disable: function disable() {
      var e = this;if (!A.event) return !1;if (!e.mousewheel.enabled) return !1;var t = e.$el;return "container" !== e.params.mousewheel.eventsTarged && (t = L(e.params.mousewheel.eventsTarged)), t.off(A.event, e.mousewheel.handle), !(e.mousewheel.enabled = !1);
    } },
      H = { update: function update() {
      var e = this,
          t = e.params.navigation;if (!e.params.loop) {
        var a = e.navigation,
            i = a.$nextEl,
            s = a.$prevEl;s && 0 < s.length && (e.isBeginning ? s.addClass(t.disabledClass) : s.removeClass(t.disabledClass), s[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](t.lockClass)), i && 0 < i.length && (e.isEnd ? i.addClass(t.disabledClass) : i.removeClass(t.disabledClass), i[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](t.lockClass));
      }
    }, onPrevClick: function onPrevClick(e) {
      e.preventDefault(), this.isBeginning && !this.params.loop || this.slidePrev();
    }, onNextClick: function onNextClick(e) {
      e.preventDefault(), this.isEnd && !this.params.loop || this.slideNext();
    }, init: function init() {
      var e,
          t,
          a = this,
          i = a.params.navigation;(i.nextEl || i.prevEl) && (i.nextEl && (e = L(i.nextEl), a.params.uniqueNavElements && "string" == typeof i.nextEl && 1 < e.length && 1 === a.$el.find(i.nextEl).length && (e = a.$el.find(i.nextEl))), i.prevEl && (t = L(i.prevEl), a.params.uniqueNavElements && "string" == typeof i.prevEl && 1 < t.length && 1 === a.$el.find(i.prevEl).length && (t = a.$el.find(i.prevEl))), e && 0 < e.length && e.on("click", a.navigation.onNextClick), t && 0 < t.length && t.on("click", a.navigation.onPrevClick), ee.extend(a.navigation, { $nextEl: e, nextEl: e && e[0], $prevEl: t, prevEl: t && t[0] }));
    }, destroy: function destroy() {
      var e = this,
          t = e.navigation,
          a = t.$nextEl,
          i = t.$prevEl;a && a.length && (a.off("click", e.navigation.onNextClick), a.removeClass(e.params.navigation.disabledClass)), i && i.length && (i.off("click", e.navigation.onPrevClick), i.removeClass(e.params.navigation.disabledClass));
    } },
      N = { update: function update() {
      var e = this,
          t = e.rtl,
          s = e.params.pagination;if (s.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
        var r,
            a = e.virtual && e.params.virtual.enabled ? e.virtual.slides.length : e.slides.length,
            i = e.pagination.$el,
            n = e.params.loop ? Math.ceil((a - 2 * e.loopedSlides) / e.params.slidesPerGroup) : e.snapGrid.length;if (e.params.loop ? ((r = Math.ceil((e.activeIndex - e.loopedSlides) / e.params.slidesPerGroup)) > a - 1 - 2 * e.loopedSlides && (r -= a - 2 * e.loopedSlides), n - 1 < r && (r -= n), r < 0 && "bullets" !== e.params.paginationType && (r = n + r)) : r = void 0 !== e.snapIndex ? e.snapIndex : e.activeIndex || 0, "bullets" === s.type && e.pagination.bullets && 0 < e.pagination.bullets.length) {
          var o,
              l,
              d,
              p = e.pagination.bullets;if (s.dynamicBullets && (e.pagination.bulletSize = p.eq(0)[e.isHorizontal() ? "outerWidth" : "outerHeight"](!0), i.css(e.isHorizontal() ? "width" : "height", e.pagination.bulletSize * (s.dynamicMainBullets + 4) + "px"), 1 < s.dynamicMainBullets && void 0 !== e.previousIndex && (e.pagination.dynamicBulletIndex += r - e.previousIndex, e.pagination.dynamicBulletIndex > s.dynamicMainBullets - 1 ? e.pagination.dynamicBulletIndex = s.dynamicMainBullets - 1 : e.pagination.dynamicBulletIndex < 0 && (e.pagination.dynamicBulletIndex = 0)), o = r - e.pagination.dynamicBulletIndex, d = ((l = o + (Math.min(p.length, s.dynamicMainBullets) - 1)) + o) / 2), p.removeClass(s.bulletActiveClass + " " + s.bulletActiveClass + "-next " + s.bulletActiveClass + "-next-next " + s.bulletActiveClass + "-prev " + s.bulletActiveClass + "-prev-prev " + s.bulletActiveClass + "-main"), 1 < i.length) p.each(function (e, t) {
            var a = L(t),
                i = a.index();i === r && a.addClass(s.bulletActiveClass), s.dynamicBullets && (o <= i && i <= l && a.addClass(s.bulletActiveClass + "-main"), i === o && a.prev().addClass(s.bulletActiveClass + "-prev").prev().addClass(s.bulletActiveClass + "-prev-prev"), i === l && a.next().addClass(s.bulletActiveClass + "-next").next().addClass(s.bulletActiveClass + "-next-next"));
          });else if (p.eq(r).addClass(s.bulletActiveClass), s.dynamicBullets) {
            for (var c = p.eq(o), u = p.eq(l), h = o; h <= l; h += 1) {
              p.eq(h).addClass(s.bulletActiveClass + "-main");
            }c.prev().addClass(s.bulletActiveClass + "-prev").prev().addClass(s.bulletActiveClass + "-prev-prev"), u.next().addClass(s.bulletActiveClass + "-next").next().addClass(s.bulletActiveClass + "-next-next");
          }if (s.dynamicBullets) {
            var v = Math.min(p.length, s.dynamicMainBullets + 4),
                f = (e.pagination.bulletSize * v - e.pagination.bulletSize) / 2 - d * e.pagination.bulletSize,
                m = t ? "right" : "left";p.css(e.isHorizontal() ? m : "top", f + "px");
          }
        }if ("fraction" === s.type && (i.find("." + s.currentClass).text(s.formatFractionCurrent(r + 1)), i.find("." + s.totalClass).text(s.formatFractionTotal(n))), "progressbar" === s.type) {
          var g;g = s.progressbarOpposite ? e.isHorizontal() ? "vertical" : "horizontal" : e.isHorizontal() ? "horizontal" : "vertical";var b = (r + 1) / n,
              w = 1,
              y = 1;"horizontal" === g ? w = b : y = b, i.find("." + s.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + w + ") scaleY(" + y + ")").transition(e.params.speed);
        }"custom" === s.type && s.renderCustom ? (i.html(s.renderCustom(e, r + 1, n)), e.emit("paginationRender", e, i[0])) : e.emit("paginationUpdate", e, i[0]), i[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](s.lockClass);
      }
    }, render: function render() {
      var e = this,
          t = e.params.pagination;if (t.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
        var a = e.virtual && e.params.virtual.enabled ? e.virtual.slides.length : e.slides.length,
            i = e.pagination.$el,
            s = "";if ("bullets" === t.type) {
          for (var r = e.params.loop ? Math.ceil((a - 2 * e.loopedSlides) / e.params.slidesPerGroup) : e.snapGrid.length, n = 0; n < r; n += 1) {
            t.renderBullet ? s += t.renderBullet.call(e, n, t.bulletClass) : s += "<" + t.bulletElement + ' class="' + t.bulletClass + '"></' + t.bulletElement + ">";
          }i.html(s), e.pagination.bullets = i.find("." + t.bulletClass);
        }"fraction" === t.type && (s = t.renderFraction ? t.renderFraction.call(e, t.currentClass, t.totalClass) : '<span class="' + t.currentClass + '"></span> / <span class="' + t.totalClass + '"></span>', i.html(s)), "progressbar" === t.type && (s = t.renderProgressbar ? t.renderProgressbar.call(e, t.progressbarFillClass) : '<span class="' + t.progressbarFillClass + '"></span>', i.html(s)), "custom" !== t.type && e.emit("paginationRender", e.pagination.$el[0]);
      }
    }, init: function init() {
      var a = this,
          e = a.params.pagination;if (e.el) {
        var t = L(e.el);0 !== t.length && (a.params.uniqueNavElements && "string" == typeof e.el && 1 < t.length && 1 === a.$el.find(e.el).length && (t = a.$el.find(e.el)), "bullets" === e.type && e.clickable && t.addClass(e.clickableClass), t.addClass(e.modifierClass + e.type), "bullets" === e.type && e.dynamicBullets && (t.addClass("" + e.modifierClass + e.type + "-dynamic"), a.pagination.dynamicBulletIndex = 0, e.dynamicMainBullets < 1 && (e.dynamicMainBullets = 1)), "progressbar" === e.type && e.progressbarOpposite && t.addClass(e.progressbarOppositeClass), e.clickable && t.on("click", "." + e.bulletClass, function (e) {
          e.preventDefault();var t = L(this).index() * a.params.slidesPerGroup;a.params.loop && (t += a.loopedSlides), a.slideTo(t);
        }), ee.extend(a.pagination, { $el: t, el: t[0] }));
      }
    }, destroy: function destroy() {
      var e = this,
          t = e.params.pagination;if (t.el && e.pagination.el && e.pagination.$el && 0 !== e.pagination.$el.length) {
        var a = e.pagination.$el;a.removeClass(t.hiddenClass), a.removeClass(t.modifierClass + t.type), e.pagination.bullets && e.pagination.bullets.removeClass(t.bulletActiveClass), t.clickable && a.off("click", "." + t.bulletClass);
      }
    } },
      G = { setTranslate: function setTranslate() {
      var e = this;if (e.params.scrollbar.el && e.scrollbar.el) {
        var t = e.scrollbar,
            a = e.rtlTranslate,
            i = e.progress,
            s = t.dragSize,
            r = t.trackSize,
            n = t.$dragEl,
            o = t.$el,
            l = e.params.scrollbar,
            d = s,
            p = (r - s) * i;a ? 0 < (p = -p) ? (d = s - p, p = 0) : r < -p + s && (d = r + p) : p < 0 ? (d = s + p, p = 0) : r < p + s && (d = r - p), e.isHorizontal() ? (te.transforms3d ? n.transform("translate3d(" + p + "px, 0, 0)") : n.transform("translateX(" + p + "px)"), n[0].style.width = d + "px") : (te.transforms3d ? n.transform("translate3d(0px, " + p + "px, 0)") : n.transform("translateY(" + p + "px)"), n[0].style.height = d + "px"), l.hide && (clearTimeout(e.scrollbar.timeout), o[0].style.opacity = 1, e.scrollbar.timeout = setTimeout(function () {
          o[0].style.opacity = 0, o.transition(400);
        }, 1e3));
      }
    }, setTransition: function setTransition(e) {
      this.params.scrollbar.el && this.scrollbar.el && this.scrollbar.$dragEl.transition(e);
    }, updateSize: function updateSize() {
      var e = this;if (e.params.scrollbar.el && e.scrollbar.el) {
        var t = e.scrollbar,
            a = t.$dragEl,
            i = t.$el;a[0].style.width = "", a[0].style.height = "";var s,
            r = e.isHorizontal() ? i[0].offsetWidth : i[0].offsetHeight,
            n = e.size / e.virtualSize,
            o = n * (r / e.size);s = "auto" === e.params.scrollbar.dragSize ? r * n : parseInt(e.params.scrollbar.dragSize, 10), e.isHorizontal() ? a[0].style.width = s + "px" : a[0].style.height = s + "px", i[0].style.display = 1 <= n ? "none" : "", e.params.scrollbar.hide && (i[0].style.opacity = 0), ee.extend(t, { trackSize: r, divider: n, moveDivider: o, dragSize: s }), t.$el[e.params.watchOverflow && e.isLocked ? "addClass" : "removeClass"](e.params.scrollbar.lockClass);
      }
    }, setDragPosition: function setDragPosition(e) {
      var t,
          a = this,
          i = a.scrollbar,
          s = a.rtlTranslate,
          r = i.$el,
          n = i.dragSize,
          o = i.trackSize;t = ((a.isHorizontal() ? "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX || e.clientX : "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY || e.clientY) - r.offset()[a.isHorizontal() ? "left" : "top"] - n / 2) / (o - n), t = Math.max(Math.min(t, 1), 0), s && (t = 1 - t);var l = a.minTranslate() + (a.maxTranslate() - a.minTranslate()) * t;a.updateProgress(l), a.setTranslate(l), a.updateActiveIndex(), a.updateSlidesClasses();
    }, onDragStart: function onDragStart(e) {
      var t = this,
          a = t.params.scrollbar,
          i = t.scrollbar,
          s = t.$wrapperEl,
          r = i.$el,
          n = i.$dragEl;t.scrollbar.isTouched = !0, e.preventDefault(), e.stopPropagation(), s.transition(100), n.transition(100), i.setDragPosition(e), clearTimeout(t.scrollbar.dragTimeout), r.transition(0), a.hide && r.css("opacity", 1), t.emit("scrollbarDragStart", e);
    }, onDragMove: function onDragMove(e) {
      var t = this.scrollbar,
          a = this.$wrapperEl,
          i = t.$el,
          s = t.$dragEl;this.scrollbar.isTouched && (e.preventDefault ? e.preventDefault() : e.returnValue = !1, t.setDragPosition(e), a.transition(0), i.transition(0), s.transition(0), this.emit("scrollbarDragMove", e));
    }, onDragEnd: function onDragEnd(e) {
      var t = this,
          a = t.params.scrollbar,
          i = t.scrollbar.$el;t.scrollbar.isTouched && (t.scrollbar.isTouched = !1, a.hide && (clearTimeout(t.scrollbar.dragTimeout), t.scrollbar.dragTimeout = ee.nextTick(function () {
        i.css("opacity", 0), i.transition(400);
      }, 1e3)), t.emit("scrollbarDragEnd", e), a.snapOnRelease && t.slideToClosest());
    }, enableDraggable: function enableDraggable() {
      var e = this;if (e.params.scrollbar.el) {
        var t = e.scrollbar,
            a = e.touchEventsTouch,
            i = e.touchEventsDesktop,
            s = e.params,
            r = t.$el[0],
            n = !(!te.passiveListener || !s.passiveListeners) && { passive: !1, capture: !1 },
            o = !(!te.passiveListener || !s.passiveListeners) && { passive: !0, capture: !1 };te.touch ? (r.addEventListener(a.start, e.scrollbar.onDragStart, n), r.addEventListener(a.move, e.scrollbar.onDragMove, n), r.addEventListener(a.end, e.scrollbar.onDragEnd, o)) : (r.addEventListener(i.start, e.scrollbar.onDragStart, n), f.addEventListener(i.move, e.scrollbar.onDragMove, n), f.addEventListener(i.end, e.scrollbar.onDragEnd, o));
      }
    }, disableDraggable: function disableDraggable() {
      var e = this;if (e.params.scrollbar.el) {
        var t = e.scrollbar,
            a = e.touchEventsTouch,
            i = e.touchEventsDesktop,
            s = e.params,
            r = t.$el[0],
            n = !(!te.passiveListener || !s.passiveListeners) && { passive: !1, capture: !1 },
            o = !(!te.passiveListener || !s.passiveListeners) && { passive: !0, capture: !1 };te.touch ? (r.removeEventListener(a.start, e.scrollbar.onDragStart, n), r.removeEventListener(a.move, e.scrollbar.onDragMove, n), r.removeEventListener(a.end, e.scrollbar.onDragEnd, o)) : (r.removeEventListener(i.start, e.scrollbar.onDragStart, n), f.removeEventListener(i.move, e.scrollbar.onDragMove, n), f.removeEventListener(i.end, e.scrollbar.onDragEnd, o));
      }
    }, init: function init() {
      var e = this;if (e.params.scrollbar.el) {
        var t = e.scrollbar,
            a = e.$el,
            i = e.params.scrollbar,
            s = L(i.el);e.params.uniqueNavElements && "string" == typeof i.el && 1 < s.length && 1 === a.find(i.el).length && (s = a.find(i.el));var r = s.find("." + e.params.scrollbar.dragClass);0 === r.length && (r = L('<div class="' + e.params.scrollbar.dragClass + '"></div>'), s.append(r)), ee.extend(t, { $el: s, el: s[0], $dragEl: r, dragEl: r[0] }), i.draggable && t.enableDraggable();
      }
    }, destroy: function destroy() {
      this.scrollbar.disableDraggable();
    } },
      B = { setTransform: function setTransform(e, t) {
      var a = this.rtl,
          i = L(e),
          s = a ? -1 : 1,
          r = i.attr("data-swiper-parallax") || "0",
          n = i.attr("data-swiper-parallax-x"),
          o = i.attr("data-swiper-parallax-y"),
          l = i.attr("data-swiper-parallax-scale"),
          d = i.attr("data-swiper-parallax-opacity");if (n || o ? (n = n || "0", o = o || "0") : this.isHorizontal() ? (n = r, o = "0") : (o = r, n = "0"), n = 0 <= n.indexOf("%") ? parseInt(n, 10) * t * s + "%" : n * t * s + "px", o = 0 <= o.indexOf("%") ? parseInt(o, 10) * t + "%" : o * t + "px", null != d) {
        var p = d - (d - 1) * (1 - Math.abs(t));i[0].style.opacity = p;
      }if (null == l) i.transform("translate3d(" + n + ", " + o + ", 0px)");else {
        var c = l - (l - 1) * (1 - Math.abs(t));i.transform("translate3d(" + n + ", " + o + ", 0px) scale(" + c + ")");
      }
    }, setTranslate: function setTranslate() {
      var i = this,
          e = i.$el,
          t = i.slides,
          s = i.progress,
          r = i.snapGrid;e.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function (e, t) {
        i.parallax.setTransform(t, s);
      }), t.each(function (e, t) {
        var a = t.progress;1 < i.params.slidesPerGroup && "auto" !== i.params.slidesPerView && (a += Math.ceil(e / 2) - s * (r.length - 1)), a = Math.min(Math.max(a, -1), 1), L(t).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function (e, t) {
          i.parallax.setTransform(t, a);
        });
      });
    }, setTransition: function setTransition(s) {
      void 0 === s && (s = this.params.speed);this.$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function (e, t) {
        var a = L(t),
            i = parseInt(a.attr("data-swiper-parallax-duration"), 10) || s;0 === s && (i = 0), a.transition(i);
      });
    } },
      X = { getDistanceBetweenTouches: function getDistanceBetweenTouches(e) {
      if (e.targetTouches.length < 2) return 1;var t = e.targetTouches[0].pageX,
          a = e.targetTouches[0].pageY,
          i = e.targetTouches[1].pageX,
          s = e.targetTouches[1].pageY;return Math.sqrt(Math.pow(i - t, 2) + Math.pow(s - a, 2));
    }, onGestureStart: function onGestureStart(e) {
      var t = this,
          a = t.params.zoom,
          i = t.zoom,
          s = i.gesture;if (i.fakeGestureTouched = !1, i.fakeGestureMoved = !1, !te.gestures) {
        if ("touchstart" !== e.type || "touchstart" === e.type && e.targetTouches.length < 2) return;i.fakeGestureTouched = !0, s.scaleStart = X.getDistanceBetweenTouches(e);
      }s.$slideEl && s.$slideEl.length || (s.$slideEl = L(e.target).closest(".swiper-slide"), 0 === s.$slideEl.length && (s.$slideEl = t.slides.eq(t.activeIndex)), s.$imageEl = s.$slideEl.find("img, svg, canvas"), s.$imageWrapEl = s.$imageEl.parent("." + a.containerClass), s.maxRatio = s.$imageWrapEl.attr("data-swiper-zoom") || a.maxRatio, 0 !== s.$imageWrapEl.length) ? (s.$imageEl.transition(0), t.zoom.isScaling = !0) : s.$imageEl = void 0;
    }, onGestureChange: function onGestureChange(e) {
      var t = this.params.zoom,
          a = this.zoom,
          i = a.gesture;if (!te.gestures) {
        if ("touchmove" !== e.type || "touchmove" === e.type && e.targetTouches.length < 2) return;a.fakeGestureMoved = !0, i.scaleMove = X.getDistanceBetweenTouches(e);
      }i.$imageEl && 0 !== i.$imageEl.length && (a.scale = te.gestures ? e.scale * a.currentScale : i.scaleMove / i.scaleStart * a.currentScale, a.scale > i.maxRatio && (a.scale = i.maxRatio - 1 + Math.pow(a.scale - i.maxRatio + 1, .5)), a.scale < t.minRatio && (a.scale = t.minRatio + 1 - Math.pow(t.minRatio - a.scale + 1, .5)), i.$imageEl.transform("translate3d(0,0,0) scale(" + a.scale + ")"));
    }, onGestureEnd: function onGestureEnd(e) {
      var t = this.params.zoom,
          a = this.zoom,
          i = a.gesture;if (!te.gestures) {
        if (!a.fakeGestureTouched || !a.fakeGestureMoved) return;if ("touchend" !== e.type || "touchend" === e.type && e.changedTouches.length < 2 && !g.android) return;a.fakeGestureTouched = !1, a.fakeGestureMoved = !1;
      }i.$imageEl && 0 !== i.$imageEl.length && (a.scale = Math.max(Math.min(a.scale, i.maxRatio), t.minRatio), i.$imageEl.transition(this.params.speed).transform("translate3d(0,0,0) scale(" + a.scale + ")"), a.currentScale = a.scale, a.isScaling = !1, 1 === a.scale && (i.$slideEl = void 0));
    }, onTouchStart: function onTouchStart(e) {
      var t = this.zoom,
          a = t.gesture,
          i = t.image;a.$imageEl && 0 !== a.$imageEl.length && (i.isTouched || (g.android && e.preventDefault(), i.isTouched = !0, i.touchesStart.x = "touchstart" === e.type ? e.targetTouches[0].pageX : e.pageX, i.touchesStart.y = "touchstart" === e.type ? e.targetTouches[0].pageY : e.pageY));
    }, onTouchMove: function onTouchMove(e) {
      var t = this,
          a = t.zoom,
          i = a.gesture,
          s = a.image,
          r = a.velocity;if (i.$imageEl && 0 !== i.$imageEl.length && (t.allowClick = !1, s.isTouched && i.$slideEl)) {
        s.isMoved || (s.width = i.$imageEl[0].offsetWidth, s.height = i.$imageEl[0].offsetHeight, s.startX = ee.getTranslate(i.$imageWrapEl[0], "x") || 0, s.startY = ee.getTranslate(i.$imageWrapEl[0], "y") || 0, i.slideWidth = i.$slideEl[0].offsetWidth, i.slideHeight = i.$slideEl[0].offsetHeight, i.$imageWrapEl.transition(0), t.rtl && (s.startX = -s.startX, s.startY = -s.startY));var n = s.width * a.scale,
            o = s.height * a.scale;if (!(n < i.slideWidth && o < i.slideHeight)) {
          if (s.minX = Math.min(i.slideWidth / 2 - n / 2, 0), s.maxX = -s.minX, s.minY = Math.min(i.slideHeight / 2 - o / 2, 0), s.maxY = -s.minY, s.touchesCurrent.x = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX, s.touchesCurrent.y = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, !s.isMoved && !a.isScaling) {
            if (t.isHorizontal() && (Math.floor(s.minX) === Math.floor(s.startX) && s.touchesCurrent.x < s.touchesStart.x || Math.floor(s.maxX) === Math.floor(s.startX) && s.touchesCurrent.x > s.touchesStart.x)) return void (s.isTouched = !1);if (!t.isHorizontal() && (Math.floor(s.minY) === Math.floor(s.startY) && s.touchesCurrent.y < s.touchesStart.y || Math.floor(s.maxY) === Math.floor(s.startY) && s.touchesCurrent.y > s.touchesStart.y)) return void (s.isTouched = !1);
          }e.preventDefault(), e.stopPropagation(), s.isMoved = !0, s.currentX = s.touchesCurrent.x - s.touchesStart.x + s.startX, s.currentY = s.touchesCurrent.y - s.touchesStart.y + s.startY, s.currentX < s.minX && (s.currentX = s.minX + 1 - Math.pow(s.minX - s.currentX + 1, .8)), s.currentX > s.maxX && (s.currentX = s.maxX - 1 + Math.pow(s.currentX - s.maxX + 1, .8)), s.currentY < s.minY && (s.currentY = s.minY + 1 - Math.pow(s.minY - s.currentY + 1, .8)), s.currentY > s.maxY && (s.currentY = s.maxY - 1 + Math.pow(s.currentY - s.maxY + 1, .8)), r.prevPositionX || (r.prevPositionX = s.touchesCurrent.x), r.prevPositionY || (r.prevPositionY = s.touchesCurrent.y), r.prevTime || (r.prevTime = Date.now()), r.x = (s.touchesCurrent.x - r.prevPositionX) / (Date.now() - r.prevTime) / 2, r.y = (s.touchesCurrent.y - r.prevPositionY) / (Date.now() - r.prevTime) / 2, Math.abs(s.touchesCurrent.x - r.prevPositionX) < 2 && (r.x = 0), Math.abs(s.touchesCurrent.y - r.prevPositionY) < 2 && (r.y = 0), r.prevPositionX = s.touchesCurrent.x, r.prevPositionY = s.touchesCurrent.y, r.prevTime = Date.now(), i.$imageWrapEl.transform("translate3d(" + s.currentX + "px, " + s.currentY + "px,0)");
        }
      }
    }, onTouchEnd: function onTouchEnd() {
      var e = this.zoom,
          t = e.gesture,
          a = e.image,
          i = e.velocity;if (t.$imageEl && 0 !== t.$imageEl.length) {
        if (!a.isTouched || !a.isMoved) return a.isTouched = !1, void (a.isMoved = !1);a.isTouched = !1, a.isMoved = !1;var s = 300,
            r = 300,
            n = i.x * s,
            o = a.currentX + n,
            l = i.y * r,
            d = a.currentY + l;0 !== i.x && (s = Math.abs((o - a.currentX) / i.x)), 0 !== i.y && (r = Math.abs((d - a.currentY) / i.y));var p = Math.max(s, r);a.currentX = o, a.currentY = d;var c = a.width * e.scale,
            u = a.height * e.scale;a.minX = Math.min(t.slideWidth / 2 - c / 2, 0), a.maxX = -a.minX, a.minY = Math.min(t.slideHeight / 2 - u / 2, 0), a.maxY = -a.minY, a.currentX = Math.max(Math.min(a.currentX, a.maxX), a.minX), a.currentY = Math.max(Math.min(a.currentY, a.maxY), a.minY), t.$imageWrapEl.transition(p).transform("translate3d(" + a.currentX + "px, " + a.currentY + "px,0)");
      }
    }, onTransitionEnd: function onTransitionEnd() {
      var e = this.zoom,
          t = e.gesture;t.$slideEl && this.previousIndex !== this.activeIndex && (t.$imageEl.transform("translate3d(0,0,0) scale(1)"), t.$imageWrapEl.transform("translate3d(0,0,0)"), e.scale = 1, e.currentScale = 1, t.$slideEl = void 0, t.$imageEl = void 0, t.$imageWrapEl = void 0);
    }, toggle: function toggle(e) {
      var t = this.zoom;t.scale && 1 !== t.scale ? t.out() : t.in(e);
    }, in: function _in(e) {
      var t,
          a,
          i,
          s,
          r,
          n,
          o,
          l,
          d,
          p,
          c,
          u,
          h,
          v,
          f,
          m,
          g = this,
          b = g.zoom,
          w = g.params.zoom,
          y = b.gesture,
          x = b.image;(y.$slideEl || (y.$slideEl = g.clickedSlide ? L(g.clickedSlide) : g.slides.eq(g.activeIndex), y.$imageEl = y.$slideEl.find("img, svg, canvas"), y.$imageWrapEl = y.$imageEl.parent("." + w.containerClass)), y.$imageEl && 0 !== y.$imageEl.length) && (y.$slideEl.addClass("" + w.zoomedSlideClass), void 0 === x.touchesStart.x && e ? (t = "touchend" === e.type ? e.changedTouches[0].pageX : e.pageX, a = "touchend" === e.type ? e.changedTouches[0].pageY : e.pageY) : (t = x.touchesStart.x, a = x.touchesStart.y), b.scale = y.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio, b.currentScale = y.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio, e ? (f = y.$slideEl[0].offsetWidth, m = y.$slideEl[0].offsetHeight, i = y.$slideEl.offset().left + f / 2 - t, s = y.$slideEl.offset().top + m / 2 - a, o = y.$imageEl[0].offsetWidth, l = y.$imageEl[0].offsetHeight, d = o * b.scale, p = l * b.scale, h = -(c = Math.min(f / 2 - d / 2, 0)), v = -(u = Math.min(m / 2 - p / 2, 0)), (r = i * b.scale) < c && (r = c), h < r && (r = h), (n = s * b.scale) < u && (n = u), v < n && (n = v)) : n = r = 0, y.$imageWrapEl.transition(300).transform("translate3d(" + r + "px, " + n + "px,0)"), y.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + b.scale + ")"));
    }, out: function out() {
      var e = this,
          t = e.zoom,
          a = e.params.zoom,
          i = t.gesture;i.$slideEl || (i.$slideEl = e.clickedSlide ? L(e.clickedSlide) : e.slides.eq(e.activeIndex), i.$imageEl = i.$slideEl.find("img, svg, canvas"), i.$imageWrapEl = i.$imageEl.parent("." + a.containerClass)), i.$imageEl && 0 !== i.$imageEl.length && (t.scale = 1, t.currentScale = 1, i.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"), i.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"), i.$slideEl.removeClass("" + a.zoomedSlideClass), i.$slideEl = void 0);
    }, enable: function enable() {
      var e = this,
          t = e.zoom;if (!t.enabled) {
        t.enabled = !0;var a = !("touchstart" !== e.touchEvents.start || !te.passiveListener || !e.params.passiveListeners) && { passive: !0, capture: !1 };te.gestures ? (e.$wrapperEl.on("gesturestart", ".swiper-slide", t.onGestureStart, a), e.$wrapperEl.on("gesturechange", ".swiper-slide", t.onGestureChange, a), e.$wrapperEl.on("gestureend", ".swiper-slide", t.onGestureEnd, a)) : "touchstart" === e.touchEvents.start && (e.$wrapperEl.on(e.touchEvents.start, ".swiper-slide", t.onGestureStart, a), e.$wrapperEl.on(e.touchEvents.move, ".swiper-slide", t.onGestureChange, a), e.$wrapperEl.on(e.touchEvents.end, ".swiper-slide", t.onGestureEnd, a)), e.$wrapperEl.on(e.touchEvents.move, "." + e.params.zoom.containerClass, t.onTouchMove);
      }
    }, disable: function disable() {
      var e = this,
          t = e.zoom;if (t.enabled) {
        e.zoom.enabled = !1;var a = !("touchstart" !== e.touchEvents.start || !te.passiveListener || !e.params.passiveListeners) && { passive: !0, capture: !1 };te.gestures ? (e.$wrapperEl.off("gesturestart", ".swiper-slide", t.onGestureStart, a), e.$wrapperEl.off("gesturechange", ".swiper-slide", t.onGestureChange, a), e.$wrapperEl.off("gestureend", ".swiper-slide", t.onGestureEnd, a)) : "touchstart" === e.touchEvents.start && (e.$wrapperEl.off(e.touchEvents.start, ".swiper-slide", t.onGestureStart, a), e.$wrapperEl.off(e.touchEvents.move, ".swiper-slide", t.onGestureChange, a), e.$wrapperEl.off(e.touchEvents.end, ".swiper-slide", t.onGestureEnd, a)), e.$wrapperEl.off(e.touchEvents.move, "." + e.params.zoom.containerClass, t.onTouchMove);
      }
    } },
      Y = { loadInSlide: function loadInSlide(e, l) {
      void 0 === l && (l = !0);var d = this,
          p = d.params.lazy;if (void 0 !== e && 0 !== d.slides.length) {
        var c = d.virtual && d.params.virtual.enabled ? d.$wrapperEl.children("." + d.params.slideClass + '[data-swiper-slide-index="' + e + '"]') : d.slides.eq(e),
            t = c.find("." + p.elementClass + ":not(." + p.loadedClass + "):not(." + p.loadingClass + ")");!c.hasClass(p.elementClass) || c.hasClass(p.loadedClass) || c.hasClass(p.loadingClass) || (t = t.add(c[0])), 0 !== t.length && t.each(function (e, t) {
          var i = L(t);i.addClass(p.loadingClass);var s = i.attr("data-background"),
              r = i.attr("data-src"),
              n = i.attr("data-srcset"),
              o = i.attr("data-sizes");d.loadImage(i[0], r || s, n, o, !1, function () {
            if (null != d && d && (!d || d.params) && !d.destroyed) {
              if (s ? (i.css("background-image", 'url("' + s + '")'), i.removeAttr("data-background")) : (n && (i.attr("srcset", n), i.removeAttr("data-srcset")), o && (i.attr("sizes", o), i.removeAttr("data-sizes")), r && (i.attr("src", r), i.removeAttr("data-src"))), i.addClass(p.loadedClass).removeClass(p.loadingClass), c.find("." + p.preloaderClass).remove(), d.params.loop && l) {
                var e = c.attr("data-swiper-slide-index");if (c.hasClass(d.params.slideDuplicateClass)) {
                  var t = d.$wrapperEl.children('[data-swiper-slide-index="' + e + '"]:not(.' + d.params.slideDuplicateClass + ")");d.lazy.loadInSlide(t.index(), !1);
                } else {
                  var a = d.$wrapperEl.children("." + d.params.slideDuplicateClass + '[data-swiper-slide-index="' + e + '"]');d.lazy.loadInSlide(a.index(), !1);
                }
              }d.emit("lazyImageReady", c[0], i[0]);
            }
          }), d.emit("lazyImageLoad", c[0], i[0]);
        });
      }
    }, load: function load() {
      var i = this,
          t = i.$wrapperEl,
          a = i.params,
          s = i.slides,
          e = i.activeIndex,
          r = i.virtual && a.virtual.enabled,
          n = a.lazy,
          o = a.slidesPerView;function l(e) {
        if (r) {
          if (t.children("." + a.slideClass + '[data-swiper-slide-index="' + e + '"]').length) return !0;
        } else if (s[e]) return !0;return !1;
      }function d(e) {
        return r ? L(e).attr("data-swiper-slide-index") : L(e).index();
      }if ("auto" === o && (o = 0), i.lazy.initialImageLoaded || (i.lazy.initialImageLoaded = !0), i.params.watchSlidesVisibility) t.children("." + a.slideVisibleClass).each(function (e, t) {
        var a = r ? L(t).attr("data-swiper-slide-index") : L(t).index();i.lazy.loadInSlide(a);
      });else if (1 < o) for (var p = e; p < e + o; p += 1) {
        l(p) && i.lazy.loadInSlide(p);
      } else i.lazy.loadInSlide(e);if (n.loadPrevNext) if (1 < o || n.loadPrevNextAmount && 1 < n.loadPrevNextAmount) {
        for (var c = n.loadPrevNextAmount, u = o, h = Math.min(e + u + Math.max(c, u), s.length), v = Math.max(e - Math.max(u, c), 0), f = e + o; f < h; f += 1) {
          l(f) && i.lazy.loadInSlide(f);
        }for (var m = v; m < e; m += 1) {
          l(m) && i.lazy.loadInSlide(m);
        }
      } else {
        var g = t.children("." + a.slideNextClass);0 < g.length && i.lazy.loadInSlide(d(g));var b = t.children("." + a.slidePrevClass);0 < b.length && i.lazy.loadInSlide(d(b));
      }
    } },
      V = { LinearSpline: function LinearSpline(e, t) {
      var a,
          i,
          s,
          r,
          n,
          o = function o(e, t) {
        for (i = -1, a = e.length; 1 < a - i;) {
          e[s = a + i >> 1] <= t ? i = s : a = s;
        }return a;
      };return this.x = e, this.y = t, this.lastIndex = e.length - 1, this.interpolate = function (e) {
        return e ? (n = o(this.x, e), r = n - 1, (e - this.x[r]) * (this.y[n] - this.y[r]) / (this.x[n] - this.x[r]) + this.y[r]) : 0;
      }, this;
    }, getInterpolateFunction: function getInterpolateFunction(e) {
      var t = this;t.controller.spline || (t.controller.spline = t.params.loop ? new V.LinearSpline(t.slidesGrid, e.slidesGrid) : new V.LinearSpline(t.snapGrid, e.snapGrid));
    }, setTranslate: function setTranslate(e, t) {
      var a,
          i,
          s = this,
          r = s.controller.control;function n(e) {
        var t = s.rtlTranslate ? -s.translate : s.translate;"slide" === s.params.controller.by && (s.controller.getInterpolateFunction(e), i = -s.controller.spline.interpolate(-t)), i && "container" !== s.params.controller.by || (a = (e.maxTranslate() - e.minTranslate()) / (s.maxTranslate() - s.minTranslate()), i = (t - s.minTranslate()) * a + e.minTranslate()), s.params.controller.inverse && (i = e.maxTranslate() - i), e.updateProgress(i), e.setTranslate(i, s), e.updateActiveIndex(), e.updateSlidesClasses();
      }if (Array.isArray(r)) for (var o = 0; o < r.length; o += 1) {
        r[o] !== t && r[o] instanceof T && n(r[o]);
      } else r instanceof T && t !== r && n(r);
    }, setTransition: function setTransition(t, e) {
      var a,
          i = this,
          s = i.controller.control;function r(e) {
        e.setTransition(t, i), 0 !== t && (e.transitionStart(), e.params.autoHeight && ee.nextTick(function () {
          e.updateAutoHeight();
        }), e.$wrapperEl.transitionEnd(function () {
          s && (e.params.loop && "slide" === i.params.controller.by && e.loopFix(), e.transitionEnd());
        }));
      }if (Array.isArray(s)) for (a = 0; a < s.length; a += 1) {
        s[a] !== e && s[a] instanceof T && r(s[a]);
      } else s instanceof T && e !== s && r(s);
    } },
      F = { makeElFocusable: function makeElFocusable(e) {
      return e.attr("tabIndex", "0"), e;
    }, addElRole: function addElRole(e, t) {
      return e.attr("role", t), e;
    }, addElLabel: function addElLabel(e, t) {
      return e.attr("aria-label", t), e;
    }, disableEl: function disableEl(e) {
      return e.attr("aria-disabled", !0), e;
    }, enableEl: function enableEl(e) {
      return e.attr("aria-disabled", !1), e;
    }, onEnterKey: function onEnterKey(e) {
      var t = this,
          a = t.params.a11y;if (13 === e.keyCode) {
        var i = L(e.target);t.navigation && t.navigation.$nextEl && i.is(t.navigation.$nextEl) && (t.isEnd && !t.params.loop || t.slideNext(), t.isEnd ? t.a11y.notify(a.lastSlideMessage) : t.a11y.notify(a.nextSlideMessage)), t.navigation && t.navigation.$prevEl && i.is(t.navigation.$prevEl) && (t.isBeginning && !t.params.loop || t.slidePrev(), t.isBeginning ? t.a11y.notify(a.firstSlideMessage) : t.a11y.notify(a.prevSlideMessage)), t.pagination && i.is("." + t.params.pagination.bulletClass) && i[0].click();
      }
    }, notify: function notify(e) {
      var t = this.a11y.liveRegion;0 !== t.length && (t.html(""), t.html(e));
    }, updateNavigation: function updateNavigation() {
      var e = this;if (!e.params.loop) {
        var t = e.navigation,
            a = t.$nextEl,
            i = t.$prevEl;i && 0 < i.length && (e.isBeginning ? e.a11y.disableEl(i) : e.a11y.enableEl(i)), a && 0 < a.length && (e.isEnd ? e.a11y.disableEl(a) : e.a11y.enableEl(a));
      }
    }, updatePagination: function updatePagination() {
      var i = this,
          s = i.params.a11y;i.pagination && i.params.pagination.clickable && i.pagination.bullets && i.pagination.bullets.length && i.pagination.bullets.each(function (e, t) {
        var a = L(t);i.a11y.makeElFocusable(a), i.a11y.addElRole(a, "button"), i.a11y.addElLabel(a, s.paginationBulletMessage.replace(/{{index}}/, a.index() + 1));
      });
    }, init: function init() {
      var e = this;e.$el.append(e.a11y.liveRegion);var t,
          a,
          i = e.params.a11y;e.navigation && e.navigation.$nextEl && (t = e.navigation.$nextEl), e.navigation && e.navigation.$prevEl && (a = e.navigation.$prevEl), t && (e.a11y.makeElFocusable(t), e.a11y.addElRole(t, "button"), e.a11y.addElLabel(t, i.nextSlideMessage), t.on("keydown", e.a11y.onEnterKey)), a && (e.a11y.makeElFocusable(a), e.a11y.addElRole(a, "button"), e.a11y.addElLabel(a, i.prevSlideMessage), a.on("keydown", e.a11y.onEnterKey)), e.pagination && e.params.pagination.clickable && e.pagination.bullets && e.pagination.bullets.length && e.pagination.$el.on("keydown", "." + e.params.pagination.bulletClass, e.a11y.onEnterKey);
    }, destroy: function destroy() {
      var e,
          t,
          a = this;a.a11y.liveRegion && 0 < a.a11y.liveRegion.length && a.a11y.liveRegion.remove(), a.navigation && a.navigation.$nextEl && (e = a.navigation.$nextEl), a.navigation && a.navigation.$prevEl && (t = a.navigation.$prevEl), e && e.off("keydown", a.a11y.onEnterKey), t && t.off("keydown", a.a11y.onEnterKey), a.pagination && a.params.pagination.clickable && a.pagination.bullets && a.pagination.bullets.length && a.pagination.$el.off("keydown", "." + a.params.pagination.bulletClass, a.a11y.onEnterKey);
    } },
      R = { init: function init() {
      var e = this;if (e.params.history) {
        if (!J.history || !J.history.pushState) return e.params.history.enabled = !1, void (e.params.hashNavigation.enabled = !0);var t = e.history;t.initialized = !0, t.paths = R.getPathValues(), (t.paths.key || t.paths.value) && (t.scrollToSlide(0, t.paths.value, e.params.runCallbacksOnInit), e.params.history.replaceState || J.addEventListener("popstate", e.history.setHistoryPopState));
      }
    }, destroy: function destroy() {
      this.params.history.replaceState || J.removeEventListener("popstate", this.history.setHistoryPopState);
    }, setHistoryPopState: function setHistoryPopState() {
      this.history.paths = R.getPathValues(), this.history.scrollToSlide(this.params.speed, this.history.paths.value, !1);
    }, getPathValues: function getPathValues() {
      var e = J.location.pathname.slice(1).split("/").filter(function (e) {
        return "" !== e;
      }),
          t = e.length;return { key: e[t - 2], value: e[t - 1] };
    }, setHistory: function setHistory(e, t) {
      if (this.history.initialized && this.params.history.enabled) {
        var a = this.slides.eq(t),
            i = R.slugify(a.attr("data-history"));J.location.pathname.includes(e) || (i = e + "/" + i);var s = J.history.state;s && s.value === i || (this.params.history.replaceState ? J.history.replaceState({ value: i }, null, i) : J.history.pushState({ value: i }, null, i));
      }
    }, slugify: function slugify(e) {
      return e.toString().replace(/\s+/g, "-").replace(/[^\w-]+/g, "").replace(/--+/g, "-").replace(/^-+/, "").replace(/-+$/, "");
    }, scrollToSlide: function scrollToSlide(e, t, a) {
      var i = this;if (t) for (var s = 0, r = i.slides.length; s < r; s += 1) {
        var n = i.slides.eq(s);if (R.slugify(n.attr("data-history")) === t && !n.hasClass(i.params.slideDuplicateClass)) {
          var o = n.index();i.slideTo(o, e, a);
        }
      } else i.slideTo(0, e, a);
    } },
      q = { onHashCange: function onHashCange() {
      var e = this,
          t = f.location.hash.replace("#", "");if (t !== e.slides.eq(e.activeIndex).attr("data-hash")) {
        var a = e.$wrapperEl.children("." + e.params.slideClass + '[data-hash="' + t + '"]').index();if (void 0 === a) return;e.slideTo(a);
      }
    }, setHash: function setHash() {
      var e = this;if (e.hashNavigation.initialized && e.params.hashNavigation.enabled) if (e.params.hashNavigation.replaceState && J.history && J.history.replaceState) J.history.replaceState(null, null, "#" + e.slides.eq(e.activeIndex).attr("data-hash") || "");else {
        var t = e.slides.eq(e.activeIndex),
            a = t.attr("data-hash") || t.attr("data-history");f.location.hash = a || "";
      }
    }, init: function init() {
      var e = this;if (!(!e.params.hashNavigation.enabled || e.params.history && e.params.history.enabled)) {
        e.hashNavigation.initialized = !0;var t = f.location.hash.replace("#", "");if (t) for (var a = 0, i = e.slides.length; a < i; a += 1) {
          var s = e.slides.eq(a);if ((s.attr("data-hash") || s.attr("data-history")) === t && !s.hasClass(e.params.slideDuplicateClass)) {
            var r = s.index();e.slideTo(r, 0, e.params.runCallbacksOnInit, !0);
          }
        }e.params.hashNavigation.watchState && L(J).on("hashchange", e.hashNavigation.onHashCange);
      }
    }, destroy: function destroy() {
      this.params.hashNavigation.watchState && L(J).off("hashchange", this.hashNavigation.onHashCange);
    } },
      W = { run: function run() {
      var e = this,
          t = e.slides.eq(e.activeIndex),
          a = e.params.autoplay.delay;t.attr("data-swiper-autoplay") && (a = t.attr("data-swiper-autoplay") || e.params.autoplay.delay), e.autoplay.timeout = ee.nextTick(function () {
        e.params.autoplay.reverseDirection ? e.params.loop ? (e.loopFix(), e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.isBeginning ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(e.slides.length - 1, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.params.loop ? (e.loopFix(), e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")) : e.isEnd ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(0, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slideNext(e.params.speed, !0, !0), e.emit("autoplay"));
      }, a);
    }, start: function start() {
      var e = this;return void 0 === e.autoplay.timeout && !e.autoplay.running && (e.autoplay.running = !0, e.emit("autoplayStart"), e.autoplay.run(), !0);
    }, stop: function stop() {
      var e = this;return !!e.autoplay.running && void 0 !== e.autoplay.timeout && (e.autoplay.timeout && (clearTimeout(e.autoplay.timeout), e.autoplay.timeout = void 0), e.autoplay.running = !1, e.emit("autoplayStop"), !0);
    }, pause: function pause(e) {
      var t = this;t.autoplay.running && (t.autoplay.paused || (t.autoplay.timeout && clearTimeout(t.autoplay.timeout), t.autoplay.paused = !0, 0 !== e && t.params.autoplay.waitForTransition ? (t.$wrapperEl[0].addEventListener("transitionend", t.autoplay.onTransitionEnd), t.$wrapperEl[0].addEventListener("webkitTransitionEnd", t.autoplay.onTransitionEnd)) : (t.autoplay.paused = !1, t.autoplay.run())));
    } },
      j = { setTranslate: function setTranslate() {
      for (var e = this, t = e.slides, a = 0; a < t.length; a += 1) {
        var i = e.slides.eq(a),
            s = -i[0].swiperSlideOffset;e.params.virtualTranslate || (s -= e.translate);var r = 0;e.isHorizontal() || (r = s, s = 0);var n = e.params.fadeEffect.crossFade ? Math.max(1 - Math.abs(i[0].progress), 0) : 1 + Math.min(Math.max(i[0].progress, -1), 0);i.css({ opacity: n }).transform("translate3d(" + s + "px, " + r + "px, 0px)");
      }
    }, setTransition: function setTransition(e) {
      var a = this,
          t = a.slides,
          i = a.$wrapperEl;if (t.transition(e), a.params.virtualTranslate && 0 !== e) {
        var s = !1;t.transitionEnd(function () {
          if (!s && a && !a.destroyed) {
            s = !0, a.animating = !1;for (var e = ["webkitTransitionEnd", "transitionend"], t = 0; t < e.length; t += 1) {
              i.trigger(e[t]);
            }
          }
        });
      }
    } },
      U = { setTranslate: function setTranslate() {
      var e,
          t = this,
          a = t.$el,
          i = t.$wrapperEl,
          s = t.slides,
          r = t.width,
          n = t.height,
          o = t.rtlTranslate,
          l = t.size,
          d = t.params.cubeEffect,
          p = t.isHorizontal(),
          c = t.virtual && t.params.virtual.enabled,
          u = 0;d.shadow && (p ? (0 === (e = i.find(".swiper-cube-shadow")).length && (e = L('<div class="swiper-cube-shadow"></div>'), i.append(e)), e.css({ height: r + "px" })) : 0 === (e = a.find(".swiper-cube-shadow")).length && (e = L('<div class="swiper-cube-shadow"></div>'), a.append(e)));for (var h = 0; h < s.length; h += 1) {
        var v = s.eq(h),
            f = h;c && (f = parseInt(v.attr("data-swiper-slide-index"), 10));var m = 90 * f,
            g = Math.floor(m / 360);o && (m = -m, g = Math.floor(-m / 360));var b = Math.max(Math.min(v[0].progress, 1), -1),
            w = 0,
            y = 0,
            x = 0;f % 4 == 0 ? (w = 4 * -g * l, x = 0) : (f - 1) % 4 == 0 ? (w = 0, x = 4 * -g * l) : (f - 2) % 4 == 0 ? (w = l + 4 * g * l, x = l) : (f - 3) % 4 == 0 && (w = -l, x = 3 * l + 4 * l * g), o && (w = -w), p || (y = w, w = 0);var T = "rotateX(" + (p ? 0 : -m) + "deg) rotateY(" + (p ? m : 0) + "deg) translate3d(" + w + "px, " + y + "px, " + x + "px)";if (b <= 1 && -1 < b && (u = 90 * f + 90 * b, o && (u = 90 * -f - 90 * b)), v.transform(T), d.slideShadows) {
          var E = p ? v.find(".swiper-slide-shadow-left") : v.find(".swiper-slide-shadow-top"),
              S = p ? v.find(".swiper-slide-shadow-right") : v.find(".swiper-slide-shadow-bottom");0 === E.length && (E = L('<div class="swiper-slide-shadow-' + (p ? "left" : "top") + '"></div>'), v.append(E)), 0 === S.length && (S = L('<div class="swiper-slide-shadow-' + (p ? "right" : "bottom") + '"></div>'), v.append(S)), E.length && (E[0].style.opacity = Math.max(-b, 0)), S.length && (S[0].style.opacity = Math.max(b, 0));
        }
      }if (i.css({ "-webkit-transform-origin": "50% 50% -" + l / 2 + "px", "-moz-transform-origin": "50% 50% -" + l / 2 + "px", "-ms-transform-origin": "50% 50% -" + l / 2 + "px", "transform-origin": "50% 50% -" + l / 2 + "px" }), d.shadow) if (p) e.transform("translate3d(0px, " + (r / 2 + d.shadowOffset) + "px, " + -r / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + d.shadowScale + ")");else {
        var C = Math.abs(u) - 90 * Math.floor(Math.abs(u) / 90),
            M = 1.5 - (Math.sin(2 * C * Math.PI / 360) / 2 + Math.cos(2 * C * Math.PI / 360) / 2),
            z = d.shadowScale,
            P = d.shadowScale / M,
            k = d.shadowOffset;e.transform("scale3d(" + z + ", 1, " + P + ") translate3d(0px, " + (n / 2 + k) + "px, " + -n / 2 / P + "px) rotateX(-90deg)");
      }var $ = I.isSafari || I.isUiWebView ? -l / 2 : 0;i.transform("translate3d(0px,0," + $ + "px) rotateX(" + (t.isHorizontal() ? 0 : u) + "deg) rotateY(" + (t.isHorizontal() ? -u : 0) + "deg)");
    }, setTransition: function setTransition(e) {
      var t = this.$el;this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), this.params.cubeEffect.shadow && !this.isHorizontal() && t.find(".swiper-cube-shadow").transition(e);
    } },
      K = { setTranslate: function setTranslate() {
      for (var e = this, t = e.slides, a = e.rtlTranslate, i = 0; i < t.length; i += 1) {
        var s = t.eq(i),
            r = s[0].progress;e.params.flipEffect.limitRotation && (r = Math.max(Math.min(s[0].progress, 1), -1));var n = -180 * r,
            o = 0,
            l = -s[0].swiperSlideOffset,
            d = 0;if (e.isHorizontal() ? a && (n = -n) : (d = l, o = -n, n = l = 0), s[0].style.zIndex = -Math.abs(Math.round(r)) + t.length, e.params.flipEffect.slideShadows) {
          var p = e.isHorizontal() ? s.find(".swiper-slide-shadow-left") : s.find(".swiper-slide-shadow-top"),
              c = e.isHorizontal() ? s.find(".swiper-slide-shadow-right") : s.find(".swiper-slide-shadow-bottom");0 === p.length && (p = L('<div class="swiper-slide-shadow-' + (e.isHorizontal() ? "left" : "top") + '"></div>'), s.append(p)), 0 === c.length && (c = L('<div class="swiper-slide-shadow-' + (e.isHorizontal() ? "right" : "bottom") + '"></div>'), s.append(c)), p.length && (p[0].style.opacity = Math.max(-r, 0)), c.length && (c[0].style.opacity = Math.max(r, 0));
        }s.transform("translate3d(" + l + "px, " + d + "px, 0px) rotateX(" + o + "deg) rotateY(" + n + "deg)");
      }
    }, setTransition: function setTransition(e) {
      var a = this,
          t = a.slides,
          i = a.activeIndex,
          s = a.$wrapperEl;if (t.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), a.params.virtualTranslate && 0 !== e) {
        var r = !1;t.eq(i).transitionEnd(function () {
          if (!r && a && !a.destroyed) {
            r = !0, a.animating = !1;for (var e = ["webkitTransitionEnd", "transitionend"], t = 0; t < e.length; t += 1) {
              s.trigger(e[t]);
            }
          }
        });
      }
    } },
      _ = { setTranslate: function setTranslate() {
      for (var e = this, t = e.width, a = e.height, i = e.slides, s = e.$wrapperEl, r = e.slidesSizesGrid, n = e.params.coverflowEffect, o = e.isHorizontal(), l = e.translate, d = o ? t / 2 - l : a / 2 - l, p = o ? n.rotate : -n.rotate, c = n.depth, u = 0, h = i.length; u < h; u += 1) {
        var v = i.eq(u),
            f = r[u],
            m = (d - v[0].swiperSlideOffset - f / 2) / f * n.modifier,
            g = o ? p * m : 0,
            b = o ? 0 : p * m,
            w = -c * Math.abs(m),
            y = o ? 0 : n.stretch * m,
            x = o ? n.stretch * m : 0;Math.abs(x) < .001 && (x = 0), Math.abs(y) < .001 && (y = 0), Math.abs(w) < .001 && (w = 0), Math.abs(g) < .001 && (g = 0), Math.abs(b) < .001 && (b = 0);var T = "translate3d(" + x + "px," + y + "px," + w + "px)  rotateX(" + b + "deg) rotateY(" + g + "deg)";if (v.transform(T), v[0].style.zIndex = 1 - Math.abs(Math.round(m)), n.slideShadows) {
          var E = o ? v.find(".swiper-slide-shadow-left") : v.find(".swiper-slide-shadow-top"),
              S = o ? v.find(".swiper-slide-shadow-right") : v.find(".swiper-slide-shadow-bottom");0 === E.length && (E = L('<div class="swiper-slide-shadow-' + (o ? "left" : "top") + '"></div>'), v.append(E)), 0 === S.length && (S = L('<div class="swiper-slide-shadow-' + (o ? "right" : "bottom") + '"></div>'), v.append(S)), E.length && (E[0].style.opacity = 0 < m ? m : 0), S.length && (S[0].style.opacity = 0 < -m ? -m : 0);
        }
      }(te.pointerEvents || te.prefixedPointerEvents) && (s[0].style.perspectiveOrigin = d + "px 50%");
    }, setTransition: function setTransition(e) {
      this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e);
    } },
      Z = { init: function init() {
      var e = this,
          t = e.params.thumbs,
          a = e.constructor;t.swiper instanceof a ? (e.thumbs.swiper = t.swiper, ee.extend(e.thumbs.swiper.originalParams, { watchSlidesProgress: !0, slideToClickedSlide: !1 }), ee.extend(e.thumbs.swiper.params, { watchSlidesProgress: !0, slideToClickedSlide: !1 })) : ee.isObject(t.swiper) && (e.thumbs.swiper = new a(ee.extend({}, t.swiper, { watchSlidesVisibility: !0, watchSlidesProgress: !0, slideToClickedSlide: !1 })), e.thumbs.swiperCreated = !0), e.thumbs.swiper.$el.addClass(e.params.thumbs.thumbsContainerClass), e.thumbs.swiper.on("tap", e.thumbs.onThumbClick);
    }, onThumbClick: function onThumbClick() {
      var e = this,
          t = e.thumbs.swiper;if (t) {
        var a = t.clickedIndex,
            i = t.clickedSlide;if (!(i && L(i).hasClass(e.params.thumbs.slideThumbActiveClass) || null == a)) {
          var s;if (s = t.params.loop ? parseInt(L(t.clickedSlide).attr("data-swiper-slide-index"), 10) : a, e.params.loop) {
            var r = e.activeIndex;e.slides.eq(r).hasClass(e.params.slideDuplicateClass) && (e.loopFix(), e._clientLeft = e.$wrapperEl[0].clientLeft, r = e.activeIndex);var n = e.slides.eq(r).prevAll('[data-swiper-slide-index="' + s + '"]').eq(0).index(),
                o = e.slides.eq(r).nextAll('[data-swiper-slide-index="' + s + '"]').eq(0).index();s = void 0 === n ? o : void 0 === o ? n : o - r < r - n ? o : n;
          }e.slideTo(s);
        }
      }
    }, update: function update(e) {
      var t = this,
          a = t.thumbs.swiper;if (a) {
        var i = "auto" === a.params.slidesPerView ? a.slidesPerViewDynamic() : a.params.slidesPerView;if (t.realIndex !== a.realIndex) {
          var s,
              r = a.activeIndex;if (a.params.loop) {
            a.slides.eq(r).hasClass(a.params.slideDuplicateClass) && (a.loopFix(), a._clientLeft = a.$wrapperEl[0].clientLeft, r = a.activeIndex);var n = a.slides.eq(r).prevAll('[data-swiper-slide-index="' + t.realIndex + '"]').eq(0).index(),
                o = a.slides.eq(r).nextAll('[data-swiper-slide-index="' + t.realIndex + '"]').eq(0).index();s = void 0 === n ? o : void 0 === o ? n : o - r == r - n ? r : o - r < r - n ? o : n;
          } else s = t.realIndex;a.visibleSlidesIndexes.indexOf(s) < 0 && (a.params.centeredSlides ? s = r < s ? s - Math.floor(i / 2) + 1 : s + Math.floor(i / 2) - 1 : r < s && (s = s - i + 1), a.slideTo(s, e ? 0 : void 0));
        }var l = 1,
            d = t.params.thumbs.slideThumbActiveClass;if (1 < t.params.slidesPerView && !t.params.centeredSlides && (l = t.params.slidesPerView), a.slides.removeClass(d), a.params.loop) for (var p = 0; p < l; p += 1) {
          a.$wrapperEl.children('[data-swiper-slide-index="' + (t.realIndex + p) + '"]').addClass(d);
        } else for (var c = 0; c < l; c += 1) {
          a.slides.eq(t.realIndex + c).addClass(d);
        }
      }
    } },
      Q = [E, S, C, M, P, $, O, { name: "mousewheel", params: { mousewheel: { enabled: !1, releaseOnEdges: !1, invert: !1, forceToAxis: !1, sensitivity: 1, eventsTarged: "container" } }, create: function create() {
      var e = this;ee.extend(e, { mousewheel: { enabled: !1, enable: A.enable.bind(e), disable: A.disable.bind(e), handle: A.handle.bind(e), handleMouseEnter: A.handleMouseEnter.bind(e), handleMouseLeave: A.handleMouseLeave.bind(e), lastScrollTime: ee.now() } });
    }, on: { init: function init() {
        this.params.mousewheel.enabled && this.mousewheel.enable();
      }, destroy: function destroy() {
        this.mousewheel.enabled && this.mousewheel.disable();
      } } }, { name: "navigation", params: { navigation: { nextEl: null, prevEl: null, hideOnClick: !1, disabledClass: "swiper-button-disabled", hiddenClass: "swiper-button-hidden", lockClass: "swiper-button-lock" } }, create: function create() {
      var e = this;ee.extend(e, { navigation: { init: H.init.bind(e), update: H.update.bind(e), destroy: H.destroy.bind(e), onNextClick: H.onNextClick.bind(e), onPrevClick: H.onPrevClick.bind(e) } });
    }, on: { init: function init() {
        this.navigation.init(), this.navigation.update();
      }, toEdge: function toEdge() {
        this.navigation.update();
      }, fromEdge: function fromEdge() {
        this.navigation.update();
      }, destroy: function destroy() {
        this.navigation.destroy();
      }, click: function click(e) {
        var t,
            a = this,
            i = a.navigation,
            s = i.$nextEl,
            r = i.$prevEl;!a.params.navigation.hideOnClick || L(e.target).is(r) || L(e.target).is(s) || (s ? t = s.hasClass(a.params.navigation.hiddenClass) : r && (t = r.hasClass(a.params.navigation.hiddenClass)), !0 === t ? a.emit("navigationShow", a) : a.emit("navigationHide", a), s && s.toggleClass(a.params.navigation.hiddenClass), r && r.toggleClass(a.params.navigation.hiddenClass));
      } } }, { name: "pagination", params: { pagination: { el: null, bulletElement: "span", clickable: !1, hideOnClick: !1, renderBullet: null, renderProgressbar: null, renderFraction: null, renderCustom: null, progressbarOpposite: !1, type: "bullets", dynamicBullets: !1, dynamicMainBullets: 1, formatFractionCurrent: function formatFractionCurrent(e) {
          return e;
        }, formatFractionTotal: function formatFractionTotal(e) {
          return e;
        }, bulletClass: "swiper-pagination-bullet", bulletActiveClass: "swiper-pagination-bullet-active", modifierClass: "swiper-pagination-", currentClass: "swiper-pagination-current", totalClass: "swiper-pagination-total", hiddenClass: "swiper-pagination-hidden", progressbarFillClass: "swiper-pagination-progressbar-fill", progressbarOppositeClass: "swiper-pagination-progressbar-opposite", clickableClass: "swiper-pagination-clickable", lockClass: "swiper-pagination-lock" } }, create: function create() {
      var e = this;ee.extend(e, { pagination: { init: N.init.bind(e), render: N.render.bind(e), update: N.update.bind(e), destroy: N.destroy.bind(e), dynamicBulletIndex: 0 } });
    }, on: { init: function init() {
        this.pagination.init(), this.pagination.render(), this.pagination.update();
      }, activeIndexChange: function activeIndexChange() {
        this.params.loop ? this.pagination.update() : void 0 === this.snapIndex && this.pagination.update();
      }, snapIndexChange: function snapIndexChange() {
        this.params.loop || this.pagination.update();
      }, slidesLengthChange: function slidesLengthChange() {
        this.params.loop && (this.pagination.render(), this.pagination.update());
      }, snapGridLengthChange: function snapGridLengthChange() {
        this.params.loop || (this.pagination.render(), this.pagination.update());
      }, destroy: function destroy() {
        this.pagination.destroy();
      }, click: function click(e) {
        var t = this;t.params.pagination.el && t.params.pagination.hideOnClick && 0 < t.pagination.$el.length && !L(e.target).hasClass(t.params.pagination.bulletClass) && (!0 === t.pagination.$el.hasClass(t.params.pagination.hiddenClass) ? t.emit("paginationShow", t) : t.emit("paginationHide", t), t.pagination.$el.toggleClass(t.params.pagination.hiddenClass));
      } } }, { name: "scrollbar", params: { scrollbar: { el: null, dragSize: "auto", hide: !1, draggable: !1, snapOnRelease: !0, lockClass: "swiper-scrollbar-lock", dragClass: "swiper-scrollbar-drag" } }, create: function create() {
      var e = this;ee.extend(e, { scrollbar: { init: G.init.bind(e), destroy: G.destroy.bind(e), updateSize: G.updateSize.bind(e), setTranslate: G.setTranslate.bind(e), setTransition: G.setTransition.bind(e), enableDraggable: G.enableDraggable.bind(e), disableDraggable: G.disableDraggable.bind(e), setDragPosition: G.setDragPosition.bind(e), onDragStart: G.onDragStart.bind(e), onDragMove: G.onDragMove.bind(e), onDragEnd: G.onDragEnd.bind(e), isTouched: !1, timeout: null, dragTimeout: null } });
    }, on: { init: function init() {
        this.scrollbar.init(), this.scrollbar.updateSize(), this.scrollbar.setTranslate();
      }, update: function update() {
        this.scrollbar.updateSize();
      }, resize: function resize() {
        this.scrollbar.updateSize();
      }, observerUpdate: function observerUpdate() {
        this.scrollbar.updateSize();
      }, setTranslate: function setTranslate() {
        this.scrollbar.setTranslate();
      }, setTransition: function setTransition(e) {
        this.scrollbar.setTransition(e);
      }, destroy: function destroy() {
        this.scrollbar.destroy();
      } } }, { name: "parallax", params: { parallax: { enabled: !1 } }, create: function create() {
      ee.extend(this, { parallax: { setTransform: B.setTransform.bind(this), setTranslate: B.setTranslate.bind(this), setTransition: B.setTransition.bind(this) } });
    }, on: { beforeInit: function beforeInit() {
        this.params.parallax.enabled && (this.params.watchSlidesProgress = !0, this.originalParams.watchSlidesProgress = !0);
      }, init: function init() {
        this.params.parallax.enabled && this.parallax.setTranslate();
      }, setTranslate: function setTranslate() {
        this.params.parallax.enabled && this.parallax.setTranslate();
      }, setTransition: function setTransition(e) {
        this.params.parallax.enabled && this.parallax.setTransition(e);
      } } }, { name: "zoom", params: { zoom: { enabled: !1, maxRatio: 3, minRatio: 1, toggle: !0, containerClass: "swiper-zoom-container", zoomedSlideClass: "swiper-slide-zoomed" } }, create: function create() {
      var i = this,
          t = { enabled: !1, scale: 1, currentScale: 1, isScaling: !1, gesture: { $slideEl: void 0, slideWidth: void 0, slideHeight: void 0, $imageEl: void 0, $imageWrapEl: void 0, maxRatio: 3 }, image: { isTouched: void 0, isMoved: void 0, currentX: void 0, currentY: void 0, minX: void 0, minY: void 0, maxX: void 0, maxY: void 0, width: void 0, height: void 0, startX: void 0, startY: void 0, touchesStart: {}, touchesCurrent: {} }, velocity: { x: void 0, y: void 0, prevPositionX: void 0, prevPositionY: void 0, prevTime: void 0 } };"onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function (e) {
        t[e] = X[e].bind(i);
      }), ee.extend(i, { zoom: t });var s = 1;Object.defineProperty(i.zoom, "scale", { get: function get() {
          return s;
        }, set: function set(e) {
          if (s !== e) {
            var t = i.zoom.gesture.$imageEl ? i.zoom.gesture.$imageEl[0] : void 0,
                a = i.zoom.gesture.$slideEl ? i.zoom.gesture.$slideEl[0] : void 0;i.emit("zoomChange", e, t, a);
          }s = e;
        } });
    }, on: { init: function init() {
        this.params.zoom.enabled && this.zoom.enable();
      }, destroy: function destroy() {
        this.zoom.disable();
      }, touchStart: function touchStart(e) {
        this.zoom.enabled && this.zoom.onTouchStart(e);
      }, touchEnd: function touchEnd(e) {
        this.zoom.enabled && this.zoom.onTouchEnd(e);
      }, doubleTap: function doubleTap(e) {
        this.params.zoom.enabled && this.zoom.enabled && this.params.zoom.toggle && this.zoom.toggle(e);
      }, transitionEnd: function transitionEnd() {
        this.zoom.enabled && this.params.zoom.enabled && this.zoom.onTransitionEnd();
      } } }, { name: "lazy", params: { lazy: { enabled: !1, loadPrevNext: !1, loadPrevNextAmount: 1, loadOnTransitionStart: !1, elementClass: "swiper-lazy", loadingClass: "swiper-lazy-loading", loadedClass: "swiper-lazy-loaded", preloaderClass: "swiper-lazy-preloader" } }, create: function create() {
      ee.extend(this, { lazy: { initialImageLoaded: !1, load: Y.load.bind(this), loadInSlide: Y.loadInSlide.bind(this) } });
    }, on: { beforeInit: function beforeInit() {
        this.params.lazy.enabled && this.params.preloadImages && (this.params.preloadImages = !1);
      }, init: function init() {
        this.params.lazy.enabled && !this.params.loop && 0 === this.params.initialSlide && this.lazy.load();
      }, scroll: function scroll() {
        this.params.freeMode && !this.params.freeModeSticky && this.lazy.load();
      }, resize: function resize() {
        this.params.lazy.enabled && this.lazy.load();
      }, scrollbarDragMove: function scrollbarDragMove() {
        this.params.lazy.enabled && this.lazy.load();
      }, transitionStart: function transitionStart() {
        var e = this;e.params.lazy.enabled && (e.params.lazy.loadOnTransitionStart || !e.params.lazy.loadOnTransitionStart && !e.lazy.initialImageLoaded) && e.lazy.load();
      }, transitionEnd: function transitionEnd() {
        this.params.lazy.enabled && !this.params.lazy.loadOnTransitionStart && this.lazy.load();
      } } }, { name: "controller", params: { controller: { control: void 0, inverse: !1, by: "slide" } }, create: function create() {
      var e = this;ee.extend(e, { controller: { control: e.params.controller.control, getInterpolateFunction: V.getInterpolateFunction.bind(e), setTranslate: V.setTranslate.bind(e), setTransition: V.setTransition.bind(e) } });
    }, on: { update: function update() {
        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline);
      }, resize: function resize() {
        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline);
      }, observerUpdate: function observerUpdate() {
        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline);
      }, setTranslate: function setTranslate(e, t) {
        this.controller.control && this.controller.setTranslate(e, t);
      }, setTransition: function setTransition(e, t) {
        this.controller.control && this.controller.setTransition(e, t);
      } } }, { name: "a11y", params: { a11y: { enabled: !0, notificationClass: "swiper-notification", prevSlideMessage: "Previous slide", nextSlideMessage: "Next slide", firstSlideMessage: "This is the first slide", lastSlideMessage: "This is the last slide", paginationBulletMessage: "Go to slide {{index}}" } }, create: function create() {
      var t = this;ee.extend(t, { a11y: { liveRegion: L('<span class="' + t.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>') } }), Object.keys(F).forEach(function (e) {
        t.a11y[e] = F[e].bind(t);
      });
    }, on: { init: function init() {
        this.params.a11y.enabled && (this.a11y.init(), this.a11y.updateNavigation());
      }, toEdge: function toEdge() {
        this.params.a11y.enabled && this.a11y.updateNavigation();
      }, fromEdge: function fromEdge() {
        this.params.a11y.enabled && this.a11y.updateNavigation();
      }, paginationUpdate: function paginationUpdate() {
        this.params.a11y.enabled && this.a11y.updatePagination();
      }, destroy: function destroy() {
        this.params.a11y.enabled && this.a11y.destroy();
      } } }, { name: "history", params: { history: { enabled: !1, replaceState: !1, key: "slides" } }, create: function create() {
      var e = this;ee.extend(e, { history: { init: R.init.bind(e), setHistory: R.setHistory.bind(e), setHistoryPopState: R.setHistoryPopState.bind(e), scrollToSlide: R.scrollToSlide.bind(e), destroy: R.destroy.bind(e) } });
    }, on: { init: function init() {
        this.params.history.enabled && this.history.init();
      }, destroy: function destroy() {
        this.params.history.enabled && this.history.destroy();
      }, transitionEnd: function transitionEnd() {
        this.history.initialized && this.history.setHistory(this.params.history.key, this.activeIndex);
      } } }, { name: "hash-navigation", params: { hashNavigation: { enabled: !1, replaceState: !1, watchState: !1 } }, create: function create() {
      var e = this;ee.extend(e, { hashNavigation: { initialized: !1, init: q.init.bind(e), destroy: q.destroy.bind(e), setHash: q.setHash.bind(e), onHashCange: q.onHashCange.bind(e) } });
    }, on: { init: function init() {
        this.params.hashNavigation.enabled && this.hashNavigation.init();
      }, destroy: function destroy() {
        this.params.hashNavigation.enabled && this.hashNavigation.destroy();
      }, transitionEnd: function transitionEnd() {
        this.hashNavigation.initialized && this.hashNavigation.setHash();
      } } }, { name: "autoplay", params: { autoplay: { enabled: !1, delay: 3e3, waitForTransition: !0, disableOnInteraction: !0, stopOnLastSlide: !1, reverseDirection: !1 } }, create: function create() {
      var t = this;ee.extend(t, { autoplay: { running: !1, paused: !1, run: W.run.bind(t), start: W.start.bind(t), stop: W.stop.bind(t), pause: W.pause.bind(t), onTransitionEnd: function onTransitionEnd(e) {
            t && !t.destroyed && t.$wrapperEl && e.target === this && (t.$wrapperEl[0].removeEventListener("transitionend", t.autoplay.onTransitionEnd), t.$wrapperEl[0].removeEventListener("webkitTransitionEnd", t.autoplay.onTransitionEnd), t.autoplay.paused = !1, t.autoplay.running ? t.autoplay.run() : t.autoplay.stop());
          } } });
    }, on: { init: function init() {
        this.params.autoplay.enabled && this.autoplay.start();
      }, beforeTransitionStart: function beforeTransitionStart(e, t) {
        this.autoplay.running && (t || !this.params.autoplay.disableOnInteraction ? this.autoplay.pause(e) : this.autoplay.stop());
      }, sliderFirstMove: function sliderFirstMove() {
        this.autoplay.running && (this.params.autoplay.disableOnInteraction ? this.autoplay.stop() : this.autoplay.pause());
      }, destroy: function destroy() {
        this.autoplay.running && this.autoplay.stop();
      } } }, { name: "effect-fade", params: { fadeEffect: { crossFade: !1 } }, create: function create() {
      ee.extend(this, { fadeEffect: { setTranslate: j.setTranslate.bind(this), setTransition: j.setTransition.bind(this) } });
    }, on: { beforeInit: function beforeInit() {
        var e = this;if ("fade" === e.params.effect) {
          e.classNames.push(e.params.containerModifierClass + "fade");var t = { slidesPerView: 1, slidesPerColumn: 1, slidesPerGroup: 1, watchSlidesProgress: !0, spaceBetween: 0, virtualTranslate: !0 };ee.extend(e.params, t), ee.extend(e.originalParams, t);
        }
      }, setTranslate: function setTranslate() {
        "fade" === this.params.effect && this.fadeEffect.setTranslate();
      }, setTransition: function setTransition(e) {
        "fade" === this.params.effect && this.fadeEffect.setTransition(e);
      } } }, { name: "effect-cube", params: { cubeEffect: { slideShadows: !0, shadow: !0, shadowOffset: 20, shadowScale: .94 } }, create: function create() {
      ee.extend(this, { cubeEffect: { setTranslate: U.setTranslate.bind(this), setTransition: U.setTransition.bind(this) } });
    }, on: { beforeInit: function beforeInit() {
        var e = this;if ("cube" === e.params.effect) {
          e.classNames.push(e.params.containerModifierClass + "cube"), e.classNames.push(e.params.containerModifierClass + "3d");var t = { slidesPerView: 1, slidesPerColumn: 1, slidesPerGroup: 1, watchSlidesProgress: !0, resistanceRatio: 0, spaceBetween: 0, centeredSlides: !1, virtualTranslate: !0 };ee.extend(e.params, t), ee.extend(e.originalParams, t);
        }
      }, setTranslate: function setTranslate() {
        "cube" === this.params.effect && this.cubeEffect.setTranslate();
      }, setTransition: function setTransition(e) {
        "cube" === this.params.effect && this.cubeEffect.setTransition(e);
      } } }, { name: "effect-flip", params: { flipEffect: { slideShadows: !0, limitRotation: !0 } }, create: function create() {
      ee.extend(this, { flipEffect: { setTranslate: K.setTranslate.bind(this), setTransition: K.setTransition.bind(this) } });
    }, on: { beforeInit: function beforeInit() {
        var e = this;if ("flip" === e.params.effect) {
          e.classNames.push(e.params.containerModifierClass + "flip"), e.classNames.push(e.params.containerModifierClass + "3d");var t = { slidesPerView: 1, slidesPerColumn: 1, slidesPerGroup: 1, watchSlidesProgress: !0, spaceBetween: 0, virtualTranslate: !0 };ee.extend(e.params, t), ee.extend(e.originalParams, t);
        }
      }, setTranslate: function setTranslate() {
        "flip" === this.params.effect && this.flipEffect.setTranslate();
      }, setTransition: function setTransition(e) {
        "flip" === this.params.effect && this.flipEffect.setTransition(e);
      } } }, { name: "effect-coverflow", params: { coverflowEffect: { rotate: 50, stretch: 0, depth: 100, modifier: 1, slideShadows: !0 } }, create: function create() {
      ee.extend(this, { coverflowEffect: { setTranslate: _.setTranslate.bind(this), setTransition: _.setTransition.bind(this) } });
    }, on: { beforeInit: function beforeInit() {
        var e = this;"coverflow" === e.params.effect && (e.classNames.push(e.params.containerModifierClass + "coverflow"), e.classNames.push(e.params.containerModifierClass + "3d"), e.params.watchSlidesProgress = !0, e.originalParams.watchSlidesProgress = !0);
      }, setTranslate: function setTranslate() {
        "coverflow" === this.params.effect && this.coverflowEffect.setTranslate();
      }, setTransition: function setTransition(e) {
        "coverflow" === this.params.effect && this.coverflowEffect.setTransition(e);
      } } }, { name: "thumbs", params: { thumbs: { swiper: null, slideThumbActiveClass: "swiper-slide-thumb-active", thumbsContainerClass: "swiper-container-thumbs" } }, create: function create() {
      ee.extend(this, { thumbs: { swiper: null, init: Z.init.bind(this), update: Z.update.bind(this), onThumbClick: Z.onThumbClick.bind(this) } });
    }, on: { beforeInit: function beforeInit() {
        var e = this.params.thumbs;e && e.swiper && (this.thumbs.init(), this.thumbs.update(!0));
      }, slideChange: function slideChange() {
        this.thumbs.swiper && this.thumbs.update();
      }, update: function update() {
        this.thumbs.swiper && this.thumbs.update();
      }, resize: function resize() {
        this.thumbs.swiper && this.thumbs.update();
      }, observerUpdate: function observerUpdate() {
        this.thumbs.swiper && this.thumbs.update();
      }, setTransition: function setTransition(e) {
        var t = this.thumbs.swiper;t && t.setTransition(e);
      }, beforeDestroy: function beforeDestroy() {
        var e = this.thumbs.swiper;e && this.thumbs.swiperCreated && e && e.destroy();
      } } }];return void 0 === T.use && (T.use = T.Class.use, T.installModule = T.Class.installModule), T.use(Q), T;
});
//# sourceMappingURL=swiper.min.js.map
;

var swiper = new Swiper('.slider-offers', {
  slidesPerView: 4,
  slidesPerColumn: 2,
  spaceBetween: 30,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  },
  breakpoints: {
    640: {
      slidesPerView: 1,
      slidesPerColumn: 1,
      spaceBetween: 10
    },
    960: {
      slidesPerView: 2,
      slidesPerColumn: 2,
      spaceBetween: 30
    },
    1440: {
      slidesPerView: 3,
      slidesPerColumn: 2,
      spaceBetween: 30
    }
  }
});

var swiper = new Swiper('.slider-other-packages', {
  slidesPerView: 4,
  spaceBetween: 30,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  },
  breakpoints: {
    640: {
      slidesPerView: 1,
      spaceBetween: 10
    },
    960: {
      slidesPerView: 2,
      spaceBetween: 30
    },
    1440: {
      slidesPerView: 3,
      spaceBetween: 30
    }
  }
});;
var navbarDropdown = UIkit.drop(".uk-navbar-dropdown-nav", {
  mode: "click"
});;