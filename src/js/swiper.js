@@include('../../node_modules/swiper/dist/js/swiper.min.js');

var swiper = new Swiper('.slider-offers', {
  slidesPerView: 4,
  slidesPerColumn: 2,
  spaceBetween: 30,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    640: {
      slidesPerView: 1,
      slidesPerColumn: 1,
      spaceBetween: 10
    },
    960: {
      slidesPerView: 2,
      slidesPerColumn: 2,
      spaceBetween: 30
    },
    1440: {
      slidesPerView: 3,
      slidesPerColumn: 2,
      spaceBetween: 30
    }
  }
});

var swiper = new Swiper('.slider-other-packages', {
  slidesPerView: 4,
  spaceBetween: 30,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    640: {
      slidesPerView: 1,
      spaceBetween: 10
    },
    960: {
      slidesPerView: 2,
      spaceBetween: 30
    },
    1440: {
      slidesPerView: 3,
      spaceBetween: 30
    }
  }
});